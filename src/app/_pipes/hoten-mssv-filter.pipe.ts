import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hotenMssvFilter'
})
export class HotenMssvFilterPipe implements PipeTransform {

  /*
      * items là một mảng cần filter
      * search là từ khóa được nhập từ bàn phím
    */
  transform(items: any[], search): any {
    /*
      * Nếu không có từ khóa nào thì trả về danh sách toàn bộ
      * Nếu có trả về thông tin của item đó
     */
    if (search) {
      search = search.toLowerCase();
    }
    return search ? items.filter(item => {
      if (item.mssv.toLowerCase().indexOf(search) !== -1) {
        return item;
      } else if (item.hoten.toLowerCase().indexOf(search) !== -1) {
        return item;
      } else {
        return;
      }
    }) : items;
  }


}
