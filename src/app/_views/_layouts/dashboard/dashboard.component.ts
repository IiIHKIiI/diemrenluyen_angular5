import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  isAdmin: boolean;

  constructor(
    private user_Service: UserService,
  ) { }

  ngOnInit() {
    this.isAdmin = false;
    this.getInfoUserNow();
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 3) {
          this.isAdmin = true;
        }
      }
    );
  }


}


