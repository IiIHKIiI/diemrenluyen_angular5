import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../_services/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  infoUser: any;
  infoSV_CB: any;
  isCheckAdmin: boolean;
  loaiuser: any;
  constructor(
    private user_Service: UserService,
    private route: Router
  ) { }

  ngOnInit() {
    this.setDefaultData();
    this.getInfoUserNow();
  }

  setDefaultData() {
    this.infoSV_CB = {
      'ten': '',
      'ma': ''
    };
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 3) {
          this.isCheckAdmin = true;
          this.infoUser = responseData.user.username;
          this.loaiuser = responseData.user.loaiuser;
        } else if (responseData.user.loaiuser === 2) {
          this.isCheckAdmin = false;
          this.infoSV_CB.ten = responseData.canbo.hotencanbo;
          this.infoSV_CB.ma = responseData.canbo.macanbo;
          this.loaiuser = responseData.user.loaiuser;
        } else {
          this.isCheckAdmin = false;
          this.infoSV_CB.ten = responseData.sv.hoten;
          this.infoSV_CB.ma = responseData.sv.mssv;
          this.loaiuser = responseData.user.loaiuser;
        }
      }
    );
  }

  onLogOut() {
    this.user_Service.logout();
    this.route.navigateByUrl('/dangnhap');
  }

  returnHome() {
    if (this.loaiuser === 1) {
      this.route.navigateByUrl('/trangchu/sinhvien');
    } else if (this.loaiuser === 2) {
      this.route.navigateByUrl('/trangchu/canbo');
    } else if (this.loaiuser === 3) {
      this.route.navigateByUrl('/trangchu/quanly');
    }

  }
}
