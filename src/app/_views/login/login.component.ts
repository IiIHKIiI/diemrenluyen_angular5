import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as JWT from 'jwt-decode';
import { UserService } from '../../_services/user.service';
import { AlertService } from '../../_services/alert.service';
import { ToastrService } from 'ngx-toastr';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  hide = true;
  constructor(
    private route: Router,
    private uService: UserService,
    private alert: AlertService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    this.uService.logout();
    this.uService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe(
      tokenData => {
        // decode the token to get its payload
        const tokenPayload = JWT(this.uService.getToken());
        const userRole = JSON.parse(tokenPayload['user'].loaiuser);
        const trangthai = JSON.parse(tokenPayload['user'].trangthai);
        if (trangthai === 1) {
          switch (userRole) {
            case 1:
              this.route.navigateByUrl('/trangchu/sinhvien');
              break;
            case 2:
              this.route.navigateByUrl('/trangchu/canbo');
              break;
            case 3:
              this.route.navigateByUrl('/trangchu/quanly');
              break;
            default:
              this.route.navigateByUrl('/dangnhap');
              break;
          }
        } else {
          // tslint:disable-next-line:max-line-length
          this.toastr.warning('Tài khoản của bạn đã bị khóa. Vui lòng liên hệ với phòng công tác sinh viên để mở lại tài khoản', 'ĐĂNG NHẬP THẤT BẠI !!!');
        }
      },
      err => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.alert.error('Tài khoản hoặc mật khẩu không đúng!. Vui lòng kiểm tra lại...');
          }
        }
      }
    );
  }
}
