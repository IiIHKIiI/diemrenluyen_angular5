
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './_layouts/header/header.component';
import { FooterComponent } from './_layouts/footer/footer.component';
import { MaterialModule } from '../material.module';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './_layouts/dashboard/dashboard.component';
import { BacktotopComponent } from './_layouts/backtotop/backtotop.component';
import { GoTopButtonModule } from 'ng2-go-top-button';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KhoaFilterPipe } from '../_pipes/khoa-filter.pipe';
import { BomonFilterPipe } from '../_pipes/bomon-filter.pipe';
import { NganhFilterPipe } from '../_pipes/nganh-filter.pipe';
import { LopFilterPipe } from '../_pipes/lop-filter.pipe';
import { HotencanboMacanboFilterPipe } from '../_pipes/hotencanbo-macanbo-filter.pipe';
import { HotenMssvFilterPipe } from '../_pipes/hoten-mssv-filter.pipe';
import { ManamhocMahockyPipe } from '../_pipes/manamhoc-mahocky-filter.pipe';
import { ConfirmDialogComponent } from './_layouts/confirm-dialog/confirm-dialog.component';
import { ThemphanquyenDialogComponent } from './adminpage/qlTaiKhoan_Module/themphanquyen-dialog/themphanquyen-dialog.component';
// tslint:disable-next-line:max-line-length
import { XemchitietsinhvienDialogComponent } from './adminpage/qlSinhVien_Module/xemchitietsinhvien-dialog/xemchitietsinhvien-dialog.component';
import { SuatieuchidanhgiadialogComponent } from './adminpage/qlTieuChiDanhGia_Module/suatieuchidanhgiadialog/suatieuchidanhgiadialog.component';
// tslint:disable-next-line:max-line-length
import { CapnhatthoigianketthucdialogComponent } from './adminpage/qlTieuChiDanhGia_Module/capnhatthoigianketthucdialog/capnhatthoigianketthucdialog.component';
import { NumberArrPipe } from '../_pipes/number-arr.pipe';
import { ThongbaoComponent } from './mainpage/main_shared_component/thongbao/thongbao.component';
// tslint:disable-next-line:max-line-length
import { DanhsachsinhvienDanhgiaComponent } from './mainpage/main_shared_component/danhsachsinhvien-danhgia/danhsachsinhvien-danhgia.component';
import { DanhsachsinhvienCanboDanhgiaComponent } from './mainpage/main_shared_component/danhsachsinhvien-canbo-danhgia/danhsachsinhvien-canbo-danhgia.component';
import { ThemminhchungDialogComponent } from './mainpage/main_shared_component/themminhchung-dialog/themminhchung-dialog.component';
import { FileUploadModule } from 'primeng/fileupload';
import { XemlichsudanhgiadialogComponent } from './mainpage/main_shared_component/xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { SafeHtmlPipe } from '../_pipes/safe-html.pipe';
import { LichsudanhgiaComponent } from './mainpage/main_shared_component/lichsudanhgia/lichsudanhgia.component';
import { DoimatkhaudialogComponent } from './mainpage/main_shared_component/doimatkhaudialog/doimatkhaudialog.component';
// tslint:disable-next-line:max-line-length
import { ImportDssinhVienDialogComponent } from './adminpage/qlSinhVien_Module/import-dssinh-vien-dialog/import-dssinh-vien-dialog.component';
import { XemthongkediemdanhgiadialogComponent } from './mainpage/CanBoPage/xemthongkediemdanhgiadialog/xemthongkediemdanhgiadialog.component';
import { FusionChartsModule } from 'angular4-fusioncharts';
import { ImportDscanBoDialogComponent } from './adminpage/qlCanBo_Module/import-dscan-bo-dialog/import-dscan-bo-dialog.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        GoTopButtonModule,
        FileUploadModule,
        FusionChartsModule,
        NgxPaginationModule
    ],
    declarations: [
        /* Filter */
        KhoaFilterPipe,
        BomonFilterPipe,
        NganhFilterPipe,
        LopFilterPipe,
        HotenMssvFilterPipe,
        HotencanboMacanboFilterPipe,
        ManamhocMahockyPipe,
        NumberArrPipe,
        SafeHtmlPipe,

        DashboardComponent,
        HeaderComponent,
        FooterComponent,
        ThongbaoComponent,
        BacktotopComponent,
        LoginComponent,
        ConfirmDialogComponent,
        XemchitietsinhvienDialogComponent,
        ThemphanquyenDialogComponent,
        SuatieuchidanhgiadialogComponent,
        CapnhatthoigianketthucdialogComponent,
        DanhsachsinhvienDanhgiaComponent,
        DanhsachsinhvienCanboDanhgiaComponent,
        ThemminhchungDialogComponent,
        XemlichsudanhgiadialogComponent,
        LichsudanhgiaComponent,
        DoimatkhaudialogComponent,
        ImportDssinhVienDialogComponent,
        ImportDscanBoDialogComponent,
        XemthongkediemdanhgiadialogComponent,

    ],
    exports: [
        /* Filter */
        KhoaFilterPipe,
        BomonFilterPipe,
        NganhFilterPipe,
        LopFilterPipe,
        HotenMssvFilterPipe,
        HotencanboMacanboFilterPipe,
        ManamhocMahockyPipe,
        NumberArrPipe,
        SafeHtmlPipe,

        DashboardComponent,
        HeaderComponent,
        FooterComponent,
        ThongbaoComponent,
        GoTopButtonModule,
        BacktotopComponent,
        LoginComponent,
        ConfirmDialogComponent,
        XemchitietsinhvienDialogComponent,
        ThemphanquyenDialogComponent,
        SuatieuchidanhgiadialogComponent,
        CapnhatthoigianketthucdialogComponent,
        DanhsachsinhvienDanhgiaComponent,
        DanhsachsinhvienCanboDanhgiaComponent,
        ThemminhchungDialogComponent,
        XemlichsudanhgiadialogComponent,
        LichsudanhgiaComponent,
        DoimatkhaudialogComponent,
        ImportDssinhVienDialogComponent,
        ImportDscanBoDialogComponent,
        XemthongkediemdanhgiadialogComponent,
    ]
})
export class SharedComponentModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedComponentModule,

        };
    }
}
