import { Component, OnInit, Inject } from '@angular/core';
import { ChungService } from '../../../../_services/chung.service';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { UserService } from '../../../../_services/user.service';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { saveAs as importedSaveAs } from 'file-saver';


@Component({
  selector: 'app-xemlichsudanhgiadialog',
  templateUrl: './xemlichsudanhgiadialog.component.html',
  // styleUrls: ['./xemlichsudanhgiadialog.component.scss']
})
export class XemlichsudanhgiadialogComponent implements OnInit {

  mahocky: string;
  manamhoc: string;

  DanhSachTieuChi: any;

  ChiTietDanhGiaSV: any;
  ChiTietDanhGiaBCS: any;
  ChiTietDanhGiaCVHT: any;
  ChiTietDanhGiaHDKhoa: any;
  ChiTietDanhGiaHDTruong: any;

  constructor(
    private dialog: MatDialog,
    private ctdg_Service: ChitietdanhgiaService,
    private tgdg_Service: ThoigiandanhgiaService,
    private tcdg_Service: TieuchidanhgiaService,
    private user_Service: UserService,
    public dialogRef: MatDialogRef<XemlichsudanhgiadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  ngOnInit() {
    this.ChiTietDanhGiaSV = [];
    this.ChiTietDanhGiaBCS = [];
    this.ChiTietDanhGiaCVHT = [];
    this.ChiTietDanhGiaHDKhoa = [];
    this.ChiTietDanhGiaHDTruong = [];

    this.getThoiGianDanhGia();
    this.mahocky = this.data.bddanhgia.mahocky;
    this.manamhoc = this.data.bddanhgia.manamhoc;
  }


  getThoiGianDanhGia() {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(this.data.bddanhgia.id_hocky).subscribe(
      responseData => {
        this.getTieuChiDanhGia(responseData.thongtin.id_tieuchidanhgia);
        this.getInfoDanhGia();
      }
    );
  }

  getTieuChiDanhGia(id_tieuchidanhgia: string) {
    this.tcdg_Service.getTieuChiDanhGia(id_tieuchidanhgia).subscribe(
      responseData => {
        this.DanhSachTieuChi = responseData.tieuchi;
      }
    );
  }

  downloadFileMinhChung(ctdg: any ) {
    const info = {
      'ctdg': ctdg,
      'id_sv': this.data.bddanhgia.id_sv
    };

    this.ctdg_Service.getFileMinhChung(info).subscribe();
  }

  getInfoDanhGia() {
    this.ctdg_Service.getInfoDanhGiaByIdBangDiemDanhGia(this.data.bddanhgia.id).subscribe(
      responseData => {
        this.ChiTietDanhGiaSV = responseData.ctdg_sv;
        this.ChiTietDanhGiaBCS = responseData.ctdg_bcs;
        this.ChiTietDanhGiaCVHT = responseData.ctdg_cvht;
        this.ChiTietDanhGiaHDKhoa = responseData.ctdg_hdkhoa;
        this.ChiTietDanhGiaHDTruong = responseData.ctdg_hdtruong;
      }
    );
  }

  exportFile(type: string) {
    const info = {
      ext: type,
      id_hocky: this.data.bddanhgia.id_hocky,
      id_bangdiemdanhgia: this.data.bddanhgia.id
    };

    this.ctdg_Service.exportKetQuaDanhGia(info).subscribe(
      resposeData => {
        importedSaveAs(resposeData);
      }
    );
  }

}

