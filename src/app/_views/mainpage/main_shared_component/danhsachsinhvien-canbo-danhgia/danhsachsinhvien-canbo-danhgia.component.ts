import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../_services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { ChungService } from '../../../../_services/chung.service';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { LopService } from '../../../../_services/lop.service';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { ToastrService } from 'ngx-toastr';
import { XemlichsudanhgiadialogComponent } from '../xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { XemthongkediemdanhgiadialogComponent } from '../../CanBoPage/xemthongkediemdanhgiadialog/xemthongkediemdanhgiadialog.component';
import { saveAs as importedSaveAs } from 'file-saver';
import { KyluatService } from '../../../../_services/kyluat.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-danhsachsinhvien-canbo-danhgia',
  templateUrl: './danhsachsinhvien-canbo-danhgia.component.html',
  // styleUrls: ['./danhsachsinhvien-canbo-danhgia.component.scss']
})
export class DanhsachsinhvienCanboDanhgiaComponent implements OnInit {

  lop: string;
  id_thoigiandanhgia: string;

  DanhSachSinhVien = [];
  DanhSachKyLuat = [];
  id = '';
  id_hocky = '';
  quyentk: string;
  id_user: string;
  p = 1;
  total = 0;
  constructor(
    private kyluat_Service: KyluatService,
    private ctdg_Service: ChitietdanhgiaService,
    private activatedRoute: ActivatedRoute,
    private lop_Service: LopService,
    private sv_Service: SinhvienService,
    private user_Service: UserService,
    private route: Router,
    private tgdg_Service: ThoigiandanhgiaService,
    private chung_Service: ChungService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this.lop = '';
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getHKHienTai();
    this.getAllDanhSachKyLuat(this.id);
  }

  backBtn() {
    this.location.back();
  }

  getAllDanhSachKyLuat(id_lop: string) {
    return this.kyluat_Service.getAllDanhSachKyLuat(id_lop).subscribe(
      responseData => {
        this.DanhSachKyLuat = responseData.danhsach;
      }
    );
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();

    this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.id_hocky = responseData.hk.id;
        this.getThoiGianDanhGia(responseData.hk.id);
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.id_thoigiandanhgia = responseData.thongtin.id;
        this.getInfoUserNow();
      }
    );
  }

  getInfoUserNow() {
    this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 2) {
          this.getTenLop(this.id);
          this.id_user = responseData.user.id;
          this.getDanhSachSinhVien_DanhGia(this.id, this.id_thoigiandanhgia);
          responseData.quyenhan.forEach(element => {
            if (element.id_nhomquyen === 3) {
              this.quyentk = element.id_nhomquyen;
            } else if (element.id_nhomquyen === 4) {
              this.quyentk = element.id_nhomquyen;
            } else if (element.id_nhomquyen === 5) {
              this.quyentk = element.id_nhomquyen;
            }
          });
        }
      }
    );
  }

  getDanhSachSinhVien_DanhGia(id_lop: string, id_thoigiandanhgia: string) {
    this.sv_Service.postDanhSachSinhVien_DanhGia(id_lop, id_thoigiandanhgia).subscribe(
      responseData => {
        this.DanhSachSinhVien = responseData.danhsach;
        this.total = this.DanhSachSinhVien.length;
      }
    );
  }

  getTenLop(id_lop: string) {
    this.lop_Service.getInfoLop(id_lop).subscribe(
      responseData => {
        this.lop = responseData.lop.tenlop;
      }
    );
  }

  openXemChiTietDanhGia(bddanhgia: any) {
    const dialogRef = this.dialog.open(XemlichsudanhgiadialogComponent, {
      data: { bddanhgia: bddanhgia }
    });
  }

  openDialog(id_sv: string, loaiuserduyet: string, loaiuserdanhgia: string, hoten: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: 'Xác nhận duyệt nhanh kết quả đánh giá rèn luyện của ', name: 'sinh viên ' + hoten }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.duyetNhanh(id_sv, loaiuserduyet, loaiuserdanhgia);
      }
    });
  }

  duyetNhanh(id_sv: string, loaiuserduyet: string, loaiuserdanhgia: string) {
    const infoDuyet = {
      id_sv: id_sv,
      id_user: this.id_user,
      id_thoigiandanhgia: this.id_thoigiandanhgia,
      loaiuserdanhgia: loaiuserdanhgia,
      loaiuserduyet: loaiuserduyet
    };

    this.ctdg_Service.postDuyetNhanh(infoDuyet).subscribe(
      responseData => {
        if (responseData.error) {
          this.toastr.error('THÊM KHÔNG THÀNH CÔNG !!!');
        } else {
          this.toastr.success(responseData.message, 'ĐÁNH GIÁ THÀNH CÔNG !!!');
          this.getInfoUserNow();
        }
      },
      err => {
        if (err) {
          console.log(err.error);
          this.toastr.error(err.error, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );

  }

  openDialogChart() {
    const dialogRef = this.dialog.open(XemthongkediemdanhgiadialogComponent, {
      data: { id_lop: this.id, tenlop: this.lop }
    });
  }

  openDialogExport() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: 'Xác nhận xuất bảng điểm đánh giá của lớp ' + this.lop + '?', name: null }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.exportBangDiemDanhGia();
      }
    });
  }

  downloadFile(id_sv: string) {
    this.kyluat_Service.getFileKyLuat(id_sv).subscribe();
  }

  exportBangDiemDanhGia() {
    const info = {
      id_hocky: this.id_hocky,
      id_lop: this.id
    };

    this.ctdg_Service.exportBangDiemDanhGia(info).subscribe(
      resposeData => {
        importedSaveAs(resposeData);
      }
    );
  }
}
