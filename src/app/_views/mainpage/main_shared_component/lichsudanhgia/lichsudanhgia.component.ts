import { Component, OnInit } from '@angular/core';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { UserService } from '../../../../_services/user.service';
import { MatDialog } from '@angular/material';
import { XemlichsudanhgiadialogComponent } from '../../main_shared_component/xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-lichsudanhgia',
  templateUrl: './lichsudanhgia.component.html',
  styleUrls: ['./lichsudanhgia.component.scss']
})
export class LichsudanhgiaComponent implements OnInit {

  LichSuDanhGia: any;
  id_sv: string;
  xeploai: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private ctdg_Service: ChitietdanhgiaService,
    private user_Service: UserService,
    private location: Location
  ) { }

  ngOnInit() {
    this.LichSuDanhGia = [];
    this.id_sv = this.activatedRoute.snapshot.paramMap.get('id_sv');
    if (this.id_sv === null) {
      this.getInfoUserNow();
    } else {
      this.getLichSuDanhGia(this.id_sv);
    }
  }

  backBtn() {
    this.location.back();
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 1) {
          this.getLichSuDanhGia(responseData.sv.id);
        }
      }
    );
  }

  getLichSuDanhGia(id_sv: string) {
    this.ctdg_Service.getLichSuDanhGiaByIdSV(id_sv).subscribe(
      responseData => {
        this.LichSuDanhGia = responseData.danhsach;
      }
    );
  }

  openXemChiTietDanhGia(bddanhgia: any) {
    const dialogRef = this.dialog.open(XemlichsudanhgiadialogComponent, {
      data: { bddanhgia: bddanhgia }
    });
  }

}
