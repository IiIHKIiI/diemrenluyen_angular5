import { Component, OnInit, Inject, forwardRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-themminhchung-dialog',
  templateUrl: './themminhchung-dialog.component.html',
  // styleUrls: ['./themminhchung-dialog.component.scss']
})
export class ThemminhchungDialogComponent {
  fileToUpload: any;
  constructor(
    public dialogRef: MatDialogRef<ThemminhchungDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
  }

  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
      this.dialogRef.close(this.fileToUpload);
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }

}
