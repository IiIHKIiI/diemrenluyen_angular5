import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../_services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { ChungService } from '../../../../_services/chung.service';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { LopService } from '../../../../_services/lop.service';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { XemlichsudanhgiadialogComponent } from '../xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { KyluatService } from '../../../../_services/kyluat.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-danhsachsinhvien-danhgia',
  templateUrl: './danhsachsinhvien-danhgia.component.html',
  // styleUrls: ['./danhsachsinhvien-danhgia.component.scss']
})
export class DanhsachsinhvienDanhgiaComponent implements OnInit {

  lop: string;
  id_thoigiandanhgia: string;

  DanhSachSinhVien = [];
  DanhSachKyLuat = [];
  id = '';
  id_user: string;
  id_sv: string;
  id_bangdiemdanhgia: string;
  p = 1;
  total = 0;
  constructor(
    private kyluat_Service: KyluatService,
    private ctdg_Service: ChitietdanhgiaService,
    private activatedRoute: ActivatedRoute,
    private lop_Service: LopService,
    private sv_Service: SinhvienService,
    private user_Service: UserService,
    private route: Router,
    private tgdg_Service: ThoigiandanhgiaService,
    private chung_Service: ChungService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location
  ) { }

  ngOnInit() {
    this.lop = '';
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getHKHienTai();
    this.getAllDanhSachKyLuat(this.id);
  }

  backBtn() {
    this.location.back();
  }

  getAllDanhSachKyLuat(id_lop: string) {
    return this.kyluat_Service.getAllDanhSachKyLuat(id_lop).subscribe(
      responseData => {
        this.DanhSachKyLuat = responseData.danhsach;
      }
    );
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();

    return this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.getThoiGianDanhGia(responseData.hk.id);
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.id_thoigiandanhgia = responseData.thongtin.id;
        if (this.id === '' || this.id === null) {
          this.getInfoUserNow();
        } else {
          this.getDanhSachSinhVien_DanhGia(this.id, this.id_thoigiandanhgia);
        }
      }
    );
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 1) {
          this.getTenLop(responseData.sv.id_lop);
          this.id_user = responseData.user.id;
          this.getDanhSachSinhVien_DanhGia(responseData.sv.id_lop, this.id_thoigiandanhgia);
        }
      }
    );
  }

  getDanhSachSinhVien_DanhGia(id_lop: string, id_thoigiandanhgia: string) {
    this.sv_Service.postDanhSachSinhVien_DanhGia(id_lop, id_thoigiandanhgia).subscribe(
      responseData => {
        this.DanhSachSinhVien = responseData.danhsach;
        this.total = this.DanhSachSinhVien.length;
      }
    );
  }

  getTenLop(id_lop: string) {
    this.lop_Service.getInfoLop(id_lop).subscribe(
      responseData => {
        this.lop = responseData.lop.tenlop;
      }
    );
  }

  openDialog(id_sv: string, loaiuserdanhgia: string, hoten: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: 'Xác nhận duyệt nhanh kết quả đánh giá rèn luyện của sinh viên ', name: hoten }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.duyetNhanh(id_sv, loaiuserdanhgia);
      }
    });
  }

  duyetNhanh(id_sv: string, loaiuserdanhgia: string) {
    const infoDuyet = {
      id_sv: id_sv,
      id_user: this.id_user,
      id_thoigiandanhgia: this.id_thoigiandanhgia,
      loaiuserdanhgia: '1',
      loaiuserduyet: loaiuserdanhgia
    };

    this.ctdg_Service.postDuyetNhanh(infoDuyet).subscribe(
      responseData => {
        if (responseData.error) {
          this.toastr.error('THÊM KHÔNG THÀNH CÔNG !!!');
        } else {
          this.toastr.success(responseData.message, 'ĐÁNH GIÁ THÀNH CÔNG !!!');
          this.getInfoUserNow();
        }
      },
      err => {
        if (err) {
          console.log(err.error);
          this.toastr.error(err.error, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );

  }

  openXemChiTietDanhGia(bddanhgia: any) {
    const dialogRef = this.dialog.open(XemlichsudanhgiadialogComponent, {
      height: '700px',
      data: { bddanhgia: bddanhgia }
    });
  }

  downloadFile(id_sv: string) {
    this.kyluat_Service.getFileKyLuat(id_sv).subscribe();
  }
}
