import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder } from '@angular/forms';
import { UserService } from '../../../../_services/user.service';
import { ToastrService } from 'ngx-toastr';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) { return; }
  const pwd = c.parent.get('matkhaumoi');
  const cpwd = c.parent.get('xacnhanmatkhau');

  if (!pwd || !cpwd) { return; }
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}


@Component({
  selector: 'app-doimatkhaudialog',
  templateUrl: './doimatkhaudialog.component.html',
  // styleUrls: ['./doimatkhaudialog.component.scss']
})
export class DoimatkhaudialogComponent implements OnInit {
  hidePwd = true;
  hideCPwd = true;
  doiMatKhau_Form: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<DoimatkhaudialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private user_Service: UserService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder
  ) { }

  get cpwd() {
    return this.doiMatKhau_Form.get('xacnhanmatkhau');
  }

  ngOnInit() {
    this.setForm();
  }

  setForm() {
    // this.doiMatKhau_Form = new FormGroup({
    //   matkhaumoi: new FormControl('', Validators.required),
    //   xacnhanmatkhau: new FormControl('', Validators.required),
    // }, this.customValidatorPasswordRepeat);

    this.doiMatKhau_Form = this.formBuilder.group({
      matkhaumoi: [null, Validators.required],
      xacnhanmatkhau: [null, [Validators.required, passwordConfirming]]
    });
  }

  customValidatorPasswordRepeat(group: FormGroup): any {
    return group.get('matkhaumoi').value === group.get('xacnhanmatkhau').value ? null : { 'khongtrung': true };
  }


  onSubmit() {
    this.user_Service.doiMatKhau(this.doiMatKhau_Form.value.xacnhanmatkhau, this.data.id_user).subscribe(
      responseData => {
        this.dialogRef.close('success');
      },
      err => {
        if (err) {
          this.dialogRef.close(err);
        }
      }
    );
  }
}
