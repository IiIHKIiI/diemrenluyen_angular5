import { Component, OnInit } from '@angular/core';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { ThongbaoService } from '../../../../_services/thongbao.service';
import { ChungService } from '../../../../_services/chung.service';

@Component({
  selector: 'app-thongbao',
  templateUrl: './thongbao.component.html',
  // styleUrls: ['./thongbao.component.scss']
})
export class ThongbaoComponent implements OnInit {

  DanhSachThongBao: any;
  thoigiandanhgia: any;
  mahocky: string;
  manamhoc: string;
  constructor(
    private tgdg_Service: ThoigiandanhgiaService,
    private tb_Service: ThongbaoService,
    private chung_Service: ChungService
  ) { }

  ngOnInit() {
    this.thoigiandanhgia = null;
    this.getHKHienTai();
    this.getThongBaoHienThi();
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();

    return this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.mahocky = responseData.hk.mahocky;
        this.manamhoc = responseData.hk.manamhoc;
        this.getThoiGianDanhGia(responseData.hk.id);
      }
    );
  }

  getThongBaoHienThi() {
    this.tb_Service.getDanhSachThongBaoHienThi().subscribe(
      responseData => {
        this.DanhSachThongBao = responseData.danhsach;
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.thoigiandanhgia = responseData.thongtin;
      }
    );
  }

}
