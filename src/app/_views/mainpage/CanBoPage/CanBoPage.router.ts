import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../../_layouts/dashboard/dashboard.component';
import { CanBoHomeComponent } from './can-bo-home/can-bo-home.component';
import { ChitietDanhgiaCvhtComponent } from './chitiet-danhgia-cvht/chitiet-danhgia-cvht.component';
import { ChitietDanhgiaHdkhoaComponent } from './chitiet-danhgia-hdkhoa/chitiet-danhgia-hdkhoa.component';
import { ChitietDanhgiaHdtruongComponent } from './chitiet-danhgia-hdtruong/chitiet-danhgia-hdtruong.component';
import { DanhsachsinhvienDanhgiaComponent } from '../main_shared_component/danhsachsinhvien-danhgia/danhsachsinhvien-danhgia.component';
// tslint:disable-next-line:max-line-length
import { DanhsachsinhvienCanboDanhgiaComponent } from '../main_shared_component/danhsachsinhvien-canbo-danhgia/danhsachsinhvien-canbo-danhgia.component';
import { DanhsachkhoaTruongduyetComponent } from './danhsachkhoa-truongduyet/danhsachkhoa-truongduyet.component';
import { XemlichsudanhgiadialogComponent } from '../main_shared_component/xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { LichsudanhgiaComponent } from '../main_shared_component/lichsudanhgia/lichsudanhgia.component';
import { SuaDanhgiaCvhtComponent } from './sua-danhgia-cvht/sua-danhgia-cvht.component';
import { SuaDanhgiaHdkhoaComponent } from './sua-danhgia-hdkhoa/sua-danhgia-hdkhoa.component';
import { SuaDanhgiaHdtruongComponent } from './sua-danhgia-hdtruong/sua-danhgia-hdtruong.component';
import { ThongkeDanhgiaComponent } from './thongke-danhgia/thongke-danhgia.component';

const cbRoutes: Routes = [
    /* Route trang chủ */
    {
        path: '',
        component: DashboardComponent,
        children: [
            { path: '', component: CanBoHomeComponent },
            { path: 'cvht/danhsachsinhvien/:id', component: DanhsachsinhvienCanboDanhgiaComponent },
            { path: 'hdkhoa/danhsachsinhvien/:id', component: DanhsachsinhvienCanboDanhgiaComponent },
            { path: 'hdtruong/danhsachsinhvien/:id', component: DanhsachsinhvienCanboDanhgiaComponent },
            { path: 'donvikhac/danhsachsinhvien/:id', component: DanhsachsinhvienCanboDanhgiaComponent },

            { path: 'cvht/danhgia/:id_sv', component: ChitietDanhgiaCvhtComponent },
            { path: 'cvht/chinhsuadanhgia/:id_sv', component: SuaDanhgiaCvhtComponent },

            { path: 'hoidongkhoa/danhgia/:id_sv', component: ChitietDanhgiaHdkhoaComponent },
            { path: 'hoidongkhoa/chinhsuadanhgia/:id_sv', component: SuaDanhgiaHdkhoaComponent },

            { path: 'hoidongtruong/danhsachlop', component: DanhsachkhoaTruongduyetComponent },
            { path: 'hoidongtruong/danhgia/:id_sv', component: ChitietDanhgiaHdtruongComponent },
            { path: 'hoidongtruong/chinhsuadanhgia/:id_sv', component: SuaDanhgiaHdtruongComponent },

            { path: 'donvikhac/danhsachlop', component: DanhsachkhoaTruongduyetComponent },

            { path: 'lichsudanhgia/:id_sv', component: LichsudanhgiaComponent },

            { path: 'thongke', component: ThongkeDanhgiaComponent },

        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(cbRoutes)],
    exports: [RouterModule]
})

export class CanBoPageRouters { }

