import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { LopService } from '../../../../_services/lop.service';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { saveAs as importedSaveAs } from 'file-saver';
@Component({
  selector: 'app-thongke-danhgia',
  templateUrl: './thongke-danhgia.component.html',
  // styleUrls: ['./thongke-danhgia.component.scss']
})
export class ThongkeDanhgiaComponent implements OnInit {
  id: string;
  width = 1000;
  height = 450;
  type = 'column2d';
  dataFormat = 'json';
  dataSource: any;
  loaichucvudanhgia: any;

  width2 = 600;
  height2 = 400;
  type2 = 'pie3d';
  dataSource2: any;

  DanhSachKhoa = []; // Lưu danh sách các khoa
  DanhSachNganh = []; // Lưu danh sách ngành
  DanhSachLop = []; // Lưu danh sách lớp
  DanhSachHocKy = [];

  dskhoa_control = new FormControl('', Validators.required);
  dsnganh_control = new FormControl('', Validators.required);
  dslop_control = new FormControl('', Validators.required);
  dshk_control = new FormControl('', Validators.required);

  isXemThongKe: boolean;
  constructor(
    private ctdg_Service: ChitietdanhgiaService,
    private khoa_Service: KhoaService,
    private nganhService: NganhService,
    private lop_Service: LopService,
    private nh_hk_Service: NamhochockyService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this.isXemThongKe = false;
    this.loaichucvudanhgia = 1;
    this.getDanhSachKhoa();
    this.getHocKy_TGDG();
    this.dataSource = {
      'chart': {
        'theme': 'fint',
        'xAxisName': 'Mã Số Sinh Viên',
        'yAxisName': 'Tổng điểm đánh giá'
      },
      'data': []
    };

    this.dataSource2 = {
      'chart': {
        'caption': 'Thống Kê Xếp Loại Kết Quả Điểm Rèn Luyện',
        'startingangle': '0',
        'showlabels': '0',
        'showlegend': '1',
        'enablemultislicing': '0',
        'slicingdistance': '15',
        'showpercentvalues': '1',
        'showpercentintooltip': '0',
        'plottooltext': 'Xếp Loại : $label Số Lượng : $datavalue',
        'theme': 'ocean'
      },
      'data': [

      ]
    };

  }

  backBtn() {
    this.location.back();
  }

  reset() {
    this.dsnganh_control = new FormControl('', Validators.required);
    this.dslop_control = new FormControl('', Validators.required);
  }

  /* Lấy danh sách các khoa trong trường */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(
      responseData => {
        this.DanhSachKhoa = responseData.danhsach;
      },
      error => {
        console.log(error.status);
      }
    );
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganhService.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  /* Lấy danh sách các lớp thuộc ngành đươc chọn */
  getDanhSachLop(id_nganh: number): void {
    this.lop_Service.getDanhSachLop_Nganh(id_nganh).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  getHocKy_TGDG() {
    this.nh_hk_Service.getHocKy_TGDG().subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.hk;
      }
    );
  }

  exportBangDiem() {
    if (this.dshk_control.value !== '' && this.dslop_control.value !== '') {
      const info = {
        id_hocky: this.dshk_control.value,
        id_lop: this.dslop_control.value
      };

      this.ctdg_Service.exportBangDiemDanhGia(info).subscribe(
        resposeData => {
          importedSaveAs(resposeData);
        }
      );
    }
  }

  xemThongKe() {
    this.isXemThongKe = true;
    if (this.dslop_control.value && this.dslop_control.value) {
      const info = {
        id_lop: this.dslop_control.value,
        id_hocky: this.dshk_control.value,
        loaichucvudanhgia: this.loaichucvudanhgia
      };

      let count_XS = 0;
      let count_G = 0;
      let count_K = 0;
      let count_TB = 0;
      let count_Y = 0;
      let count_Kem = 0;
      let count_KDG = 0;

      this.ctdg_Service.postThongKeDiemDanhGiaTheoLop_HocKy(info)
        .subscribe(
          responseData => {
            const arr = [];
            this.dataSource.data = responseData.data;
            this.dataSource.data.forEach(element => {
              if (element.value >= 90 && element.value <= 100) {
                count_XS += 1;
              } else if (element.value >= 80 && element.value <= 90) {
                count_G += 1;
              } else if (element.value >= 65 && element.value <= 80) {
                count_K += 1;
              } else if (element.value >= 50 && element.value <= 65) {
                count_TB += 1;
              } else if (element.value >= 35 && element.value <= 50) {
                count_Y += 1;
              } else if (element.value < 35) {
                count_Kem += 1;
              } else if (!element.value || element.value === '') {
                count_KDG += 1;
              }
            });

            arr.push({ label: 'Xuất Sắc', value: count_XS });
            arr.push({ label: 'Giỏi', value: count_G });
            arr.push({ label: 'Khá', value: count_K });
            arr.push({ label: 'Trung Bình', value: count_TB });
            arr.push({ label: 'Yếu', value: count_Y });
            arr.push({ label: 'Kém', value: count_Kem });
            arr.push({ label: 'Chưa đánh giá', value: count_KDG });

            this.dataSource2.data = arr;
          }
        );
    }
  }

}
