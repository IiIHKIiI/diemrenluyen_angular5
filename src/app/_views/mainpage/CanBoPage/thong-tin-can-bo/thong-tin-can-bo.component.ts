import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../_services/user.service';
import { DoimatkhaudialogComponent } from '../../main_shared_component/doimatkhaudialog/doimatkhaudialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-thong-tin-can-bo',
  templateUrl: './thong-tin-can-bo.component.html',
  // styleUrls: ['./thong-tin-can-bo.component.scss']
})
export class ThongTinCanBoComponent implements OnInit {
  infoCB: any;
  chucvu: any;
  quyentk: any;
  constructor(
    private user_Service: UserService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.chucvu = null;
    this.quyentk = null;
    this.infoCB = {};
    this.getInfoUserNow();
  }

  getInfoUserNow() {
    this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 2) {
          this.infoCB = responseData.canbo;
          if (responseData.chucvu !== null) {
            this.chucvu = responseData.chucvu;
            this.quyentk = responseData.tennhomquyen;
          }
        }
      }
    );
  }

  changePasswordDialog() {
    const dialogRef = this.dialog.open(DoimatkhaudialogComponent, {
      width: '600px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
      }
    });
  }

}
