import { Component, OnInit, Inject } from '@angular/core';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-xemthongkediemdanhgiadialog',
  templateUrl: './xemthongkediemdanhgiadialog.component.html',
  // styleUrls: ['./xemthongkediemdanhgiadialog.component.scss']
})
export class XemthongkediemdanhgiadialogComponent implements OnInit {
  tenlop = '';
  width = 1000;
  height = 450;
  type = 'column2d';
  dataFormat = 'json';
  dataSource: any;
  loaichucvudanhgia: any;
  constructor(
    private ctdg_Service: ChitietdanhgiaService,
    public dialogRef: MatDialogRef<XemthongkediemdanhgiadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit() {
    this.loaichucvudanhgia = 1;
    this.tenlop = this.data.tenlop;
    this.dataSource = {
      'chart': {
        'theme': 'fint',
        'xAxisName': 'Mã Số Sinh Viên',
        'yAxisName': 'Tổng điểm đánh giá'
      },
      'data': []
    };
    this.getData();
  }

  getData() {
    const info = {
      id_lop: this.data.id_lop,
      loaichucvudanhgia: this.loaichucvudanhgia
    };

    this.ctdg_Service.postThongKeDiemDanhGiaTheoHocKy(info).subscribe(
      responseData => {
        this.dataSource.data = responseData.data;
      });
  }

}
