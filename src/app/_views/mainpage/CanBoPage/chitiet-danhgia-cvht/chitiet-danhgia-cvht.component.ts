import { Component, OnInit } from '@angular/core';
import { ChungService } from '../../../../_services/chung.service';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { UserService } from '../../../../_services/user.service';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { ThemminhchungDialogComponent } from '../../main_shared_component/themminhchung-dialog/themminhchung-dialog.component';

@Component({
  selector: 'app-chitiet-danhgia-cvht',
  templateUrl: './chitiet-danhgia-cvht.component.html',
  // styleUrls: ['./chitiet-danhgia-cvht.component.scss']
})
export class ChitietDanhgiaCvhtComponent implements OnInit {

  mahocky: string;
  manamhoc: string;

  DanhSachTieuChi: any;
  oldtc = {};

  danhgia = {};

  dsminhchung = {};
  name_minhchung = [];

  id_lop: string;
  id_user: string;
  id_sv: string;
  id_bangdiemdanhgia: string;
  id_thoigiandanhgia: string;

  ChiTietDanhGiaSV = [];
  ChiTietDanhGiaBCS = [];
  constructor(
    private sv_Service: SinhvienService,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private ctdg_Service: ChitietdanhgiaService,
    private tgdg_Service: ThoigiandanhgiaService,
    private tcdg_Service: TieuchidanhgiaService,
    private chung_Service: ChungService,
    private user_Service: UserService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.id_sv = this.activatedRoute.snapshot.paramMap.get('id_sv');
    this.getHKHienTai();
    this.getIDLop(this.id_sv);
  }

  backBtn() {
    this.location.back();
  }

  getIDLop(id_sv: string) {
    this.sv_Service.getIDLop(id_sv).subscribe(
      responseData => {
        this.id_lop = responseData.id_lop;
        console.log(this.id_lop);
      }
    );
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();

    this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.mahocky = responseData.hk.mahocky;
        this.manamhoc = responseData.hk.manamhoc;
        this.getThoiGianDanhGia(responseData.hk.id);
        this.getInfoUserNow();
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.id_thoigiandanhgia = responseData.thongtin.id;
        this.getTieuChiDanhGia(responseData.thongtin.id_tieuchidanhgia);
        this.getInfoDanhGia(this.id_thoigiandanhgia, this.id_sv);
        this.getIDBangDiem(this.id_thoigiandanhgia, this.id_sv);
      }
    );
  }

  getTieuChiDanhGia(id_tieuchidanhgia: string) {
    this.tcdg_Service.getTieuChiDanhGia(id_tieuchidanhgia).subscribe(
      responseData => {
        this.DanhSachTieuChi = responseData.tieuchi;
      }
    );
  }

  getInfoUserNow() {
    this.user_Service.getInfoUser().subscribe(
      responseData => {
        this.id_user = responseData.user.id;
      }
    );
  }

  getInfoDanhGia(id_thoigiandanhgia: string, id_sv: string) {
    this.ctdg_Service.getInfoDanhGia(id_thoigiandanhgia, id_sv, '1').subscribe(
      responseData => {
        this.ChiTietDanhGiaSV = responseData.ctdg;
      }
    );

    this.ctdg_Service.getInfoDanhGia(id_thoigiandanhgia, id_sv, '2').subscribe(
      responseData => {
        this.ChiTietDanhGiaBCS = responseData.ctdg;
      }
    );
  }

  getIDBangDiem(id_thoigiandanhgia: string, id_sv: string) {
    this.chung_Service.getBangDiemDanhGiaByIDThoiGian_SV(id_thoigiandanhgia, id_sv).subscribe(
      responseData => {
        this.id_bangdiemdanhgia = responseData.bangdiem.id;
      }
    );
  }

  objectToArray(obj: Object) {
    const tmp = [];
    // tslint:disable-next-line:forin
    for (const key in obj) {
      tmp.push([key, obj[key]]);
    }
    return tmp;
  }

  openDialogMinhhChung(tieuchi: any) {
    const dialogRef = this.dialog.open(ThemminhchungDialogComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.dsminhchung[tieuchi.id] = result;
        this.name_minhchung = this.objectToArray(this.dsminhchung);
      }
    });

  }

  deleteFile(fileminhchung: any) {
    delete this.dsminhchung[fileminhchung[0]];
    this.name_minhchung.forEach((element, index) => {
      if (element[0] === fileminhchung[0]) {
        this.name_minhchung.splice(index, 1);
      }
    });
  }

  ganGiaTri(tieuchi: any) {
    this.danhgia[tieuchi.id] = tieuchi.diemtoida.toString();
    const tcc = tieuchi.id_tieuchicha;

    if (this.oldtc['' + tcc]) {

      delete this.danhgia[this.oldtc['' + tcc]];
    }
    this.oldtc['' + tcc] = tieuchi.id;
  }

  tinhTongDiem(diemdanhgia: any): any {
    let tongdiem = 0;
    // tslint:disable-next-line:forin
    for (const key in diemdanhgia) {
      let diem = 0;
      if (diemdanhgia[key] !== '') {
        // tslint:disable-next-line:radix
        diem = parseInt(diemdanhgia[key]);
      } else {
        diem = 0;
      }
      tongdiem += diem;
    }
    if (tongdiem > 100) {
      tongdiem = 100;
    }
    return tongdiem;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: { message: 'Xác nhận đánh giá ?', name: 'tổng điểm đánh giá: ' + this.tinhTongDiem(this.danhgia) }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {
    const input: FormData = new FormData();
    input.append('id_sv', this.id_sv);
    input.append('loaiuserdanhgia', '3');
    input.append('id_user', this.id_user);
    input.append('id_bangdiemdanhgia', this.id_bangdiemdanhgia);
    input.append('tongdiem', this.tinhTongDiem(this.danhgia));
    input.append('ctdanhgia', JSON.stringify(this.danhgia));
    // tslint:disable-next-line:forin
    for (const key in this.dsminhchung) {
      input.append('dsminhchung[' + key + ']', this.dsminhchung[key]);
    }
    return input;
  }

  onSubmit() {

    const formModel = this.prepareSave();

    return this.ctdg_Service.postChiTietDanhGia(formModel).subscribe(
      responseData => {
        if (responseData.error) {
          this.toastr.error('THÊM KHÔNG THÀNH CÔNG !!!');
        } else {
          this.toastr.success(responseData.message, 'ĐÁNH GIÁ THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/canbo/cvht/danhsachsinhvien/' + this.id_lop);
        }
      },
      err => {
        if (err) {
          console.log(err.error);
          this.toastr.error(err.error, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }


}

