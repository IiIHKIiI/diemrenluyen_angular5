import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CanBoHomeComponent } from './can-bo-home/can-bo-home.component';
import { CanBoPageRouters } from './CanBoPage.router';
import { MaterialModule } from '../../../material.module';
import { SharedComponentModule } from '../../SharedComponent.module';
import { ChitietDanhgiaCvhtComponent } from './chitiet-danhgia-cvht/chitiet-danhgia-cvht.component';
import { ChitietDanhgiaHdkhoaComponent } from './chitiet-danhgia-hdkhoa/chitiet-danhgia-hdkhoa.component';
import { ChitietDanhgiaHdtruongComponent } from './chitiet-danhgia-hdtruong/chitiet-danhgia-hdtruong.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThongTinCanBoComponent } from './thong-tin-can-bo/thong-tin-can-bo.component';
import { DanhsachkhoaTruongduyetComponent } from './danhsachkhoa-truongduyet/danhsachkhoa-truongduyet.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { LichsudanhgiaComponent } from '../main_shared_component/lichsudanhgia/lichsudanhgia.component';
import { SuaDanhgiaCvhtComponent } from './sua-danhgia-cvht/sua-danhgia-cvht.component';
import { SuaDanhgiaHdkhoaComponent } from './sua-danhgia-hdkhoa/sua-danhgia-hdkhoa.component';
import { SuaDanhgiaHdtruongComponent } from './sua-danhgia-hdtruong/sua-danhgia-hdtruong.component';
import { ThongkeDanhgiaComponent } from './thongke-danhgia/thongke-danhgia.component';
import { FusionChartsModule } from 'angular4-fusioncharts';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CanBoPageRouters,
    SharedComponentModule,
    NgxPaginationModule,
    FusionChartsModule
  ],
  declarations: [
    CanBoHomeComponent,
    ThongTinCanBoComponent,
    ChitietDanhgiaCvhtComponent,
    ChitietDanhgiaHdkhoaComponent,
    ChitietDanhgiaHdtruongComponent,
    DanhsachkhoaTruongduyetComponent,
    SuaDanhgiaCvhtComponent,
    SuaDanhgiaHdkhoaComponent,
    SuaDanhgiaHdtruongComponent,
    ThongkeDanhgiaComponent,
  ],
  exports: [
    CanBoHomeComponent,
    ThongTinCanBoComponent,
    ChitietDanhgiaCvhtComponent,
    ChitietDanhgiaHdkhoaComponent,
    ChitietDanhgiaHdtruongComponent,
    DanhsachkhoaTruongduyetComponent,
    SuaDanhgiaCvhtComponent,
    SuaDanhgiaHdkhoaComponent,
    SuaDanhgiaHdtruongComponent,
    ThongkeDanhgiaComponent,

  ]
})
export class CanBoPageComponentModule { }
