import { Component, OnInit } from '@angular/core';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { UserService } from '../../../../_services/user.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { ChungService } from '../../../../_services/chung.service';
import { LopService } from '../../../../_services/lop.service';

@Component({
  selector: 'app-can-bo-home',
  templateUrl: './can-bo-home.component.html',
  // styleUrls: ['./can-bo-home.component.scss']
})
export class CanBoHomeComponent implements OnInit {

  thoigiandanhgia: any;

  quyenhan: any;

  infoLop: any;
  trangthai: any;

  DanhSachLop: any;

  tgcvhtdg: boolean;
  tghdkdg: boolean;
  tghdtdg: boolean;


  constructor(
    private lop_Service: LopService,
    private ctdg_Service: ChitietdanhgiaService,
    private user_Service: UserService,
    private tgdg_Service: ThoigiandanhgiaService,
    private chung_Service: ChungService
  ) { }

  ngOnInit() {
    // this.tgcvhtdg = false;
    // this.tghdkdg = false;
    // this.tghdtdg = false;
    this.tgcvhtdg = true;
    this.tghdkdg = true;
    this.tghdtdg = true;
    this.thoigiandanhgia = null;
    this.quyenhan = [];
    this.DanhSachLop = [];
    this.infoLop = {
      id: '',
      trangthai: ''
    };
    this.trangthai = 0;
    this.getHKHienTai();
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();
    this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.getInfoUserNow();
        this.getThoiGianDanhGia(responseData.hk.id);
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.thoigiandanhgia = responseData.thongtin;
        this.chung_Service.setIDThoiGianDanhGia(this.thoigiandanhgia.id);
        if (this.thoigiandanhgia !== null) {
          // this.checkTGDanhGiaTheoPhanQuyen(this.thoigiandanhgia, this.quyenhan);
        }
      }
    );
  }

  getInfoUserNow() {
    this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 2) {
          this.quyenhan = responseData.quyenhan;
          this.quyenhan.forEach(element => {
            if (element.id_nhomquyen === 3) {
              this.getLopCoVan(responseData.canbo.id, this.chung_Service.getIDThoiGianDanhGia());
            } else if (element.id_nhomquyen === 4) {
              this.getDanhSachLopHoiDongDanhGia(responseData.canbo.id);
            }
          });
        }
      }
    );
  }

  checkTGDanhGiaTheoPhanQuyen(thoigiandanhgia: any, quyenhan: any) {
    quyenhan.forEach(element => {
      // tslint:disable-next-line:max-line-length
      if (new Date() >= new Date(thoigiandanhgia.tgbatdau_cvht) && new Date() <= new Date(thoigiandanhgia.tgketthuc_cvht)) {
        if (element.id_nhomquyen === 3) {
          this.tgcvhtdg = true;
        }
        // tslint:disable-next-line:max-line-length
      } else if (new Date() >= new Date(thoigiandanhgia.tgbatdau_hoidongkhoa) && new Date() <= new Date(thoigiandanhgia.tgketthuc_hoidongkhoa)) {
        if (element.id_nhomquyen === 4) {
          this.tghdkdg = true;
        }
        // tslint:disable-next-line:max-line-length
      } else if (new Date() >= new Date(thoigiandanhgia.tgbatdau_hoidongtruong) && new Date() <= new Date(thoigiandanhgia.tgketthuc_hoidongtruong)) {
        if (element.id_nhomquyen === 5) {
          this.tghdtdg = true;
        }
      }
    });
  }

  getLopCoVan(id_canbo: string, id_thoigiandanhgia: string) {
    this.lop_Service.getLopCoVan(id_canbo, id_thoigiandanhgia).subscribe(
      responseData => {
        this.infoLop = responseData.lop;
        this.trangthai = responseData.trangthai;
      }
    );
  }

  getDanhSachLopHoiDongDanhGia(id_canbo: string) {
    this.lop_Service.getDanhSachLopHoiDongDanhGia(id_canbo).subscribe(
      responseData => {
        this.DanhSachLop = responseData.lop;
      }
    );
  }

}
