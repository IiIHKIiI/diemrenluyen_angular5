import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { LopService } from '../../../../_services/lop.service';
import { NganhService } from '../../../../_services/nganh.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachkhoa-truongduyet',
  templateUrl: './danhsachkhoa-truongduyet.component.html',
  // styleUrls: ['./danhsachkhoa-truongduyet.component.scss']
})
export class DanhsachkhoaTruongduyetComponent implements OnInit {
  DanhSachLop: any;
  DanhSachNganh: any;
  DanhSachKhoa: any;

  dskhoa_control = new FormControl('', [Validators.required]);
  dsnganh_control = new FormControl('', [Validators.required]);

  loaiLoc: number;
  id_nganh: number;
  id_khoa: number;

  search: string;
  total: number;
  p = 1; // Phân trang được load đầu tiên
  constructor(
    private lop_Service: LopService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.id_khoa = null;
    this.id_nganh = null;
    this.DanhSachLop = [];
    this.getDanhSachKhoa();
    this.getDanhSachLop();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachLop();
    } else if (this.loaiLoc === 2) {
      if (this.id_khoa !== null && this.id_nganh !== null) {
        this.getDanhSachLop_Nganh(this.id_nganh);
      }
    }
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      }
    );
  }

  getDanhSachLop() {
    this.lop_Service.getDanhSachLop().subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
        this.total = this.DanhSachLop.length;
      }
    );
  }

  getDanhSachLop_Nganh(id: number) {
    this.lop_Service.getDanhSachLop_Nganh(id).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
        this.total = this.DanhSachLop.length;
      }
    );
  }
}
