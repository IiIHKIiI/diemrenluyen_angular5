import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SvhomeComponent } from './svhome/svhome.component';
import { ActivateChildSVGuard } from '../../../_guards/activateChildSV-guard';
import { DashboardComponent } from '../../_layouts/dashboard/dashboard.component';
import { ChitietDanhgiaSinhvienComponent } from '../SVPage/chitiet-danhgia-sinhvien/chitiet-danhgia-sinhvien.component';
import { ChitietDanhgiaBancansuComponent } from './chitiet-danhgia-bancansu/chitiet-danhgia-bancansu.component';
import { DanhsachsinhvienDanhgiaComponent } from '../main_shared_component/danhsachsinhvien-danhgia/danhsachsinhvien-danhgia.component';
import { XemlichsudanhgiadialogComponent } from '../main_shared_component/xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { LichsudanhgiaComponent } from '../main_shared_component/lichsudanhgia/lichsudanhgia.component';
import { SuaDanhgiaSinhvienComponent } from './sua-danhgia-sinhvien/sua-danhgia-sinhvien.component';
import { SuaDanhgiaBancansuComponent } from './sua-danhgia-bancansu/sua-danhgia-bancansu.component';

const svRoutes: Routes = [
    /* Route trang chủ */
    {
        path: '',
        component: DashboardComponent,
        children: [
            { path: '', component: SvhomeComponent },
            { path: 'lichsudanhgia', component: LichsudanhgiaComponent },
            { path: 'danhgia', component: ChitietDanhgiaSinhvienComponent },
            { path: 'chinhsuadanhgia', component: SuaDanhgiaSinhvienComponent },
            { path: 'bancansu/danhsachsinhvien', component: DanhsachsinhvienDanhgiaComponent },
            { path: 'bancansu/lichsudanhgia/:id_sv', component: LichsudanhgiaComponent },
            { path: 'bancansu/chinhsuadanhgia/:id_sv', component: SuaDanhgiaBancansuComponent },
            { path: 'bancansu/danhgia/:id_sv', component: ChitietDanhgiaBancansuComponent },

            { path: 'chitietlichsudanhgia/:id_sv', component: XemlichsudanhgiadialogComponent },
        ]
    },


];

@NgModule({
    imports: [RouterModule.forChild(svRoutes)],
    exports: [RouterModule]
})

export class SVPageRouters { }
