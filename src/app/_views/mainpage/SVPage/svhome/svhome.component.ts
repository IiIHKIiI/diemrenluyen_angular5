import { Component, OnInit } from '@angular/core';
import { ChungService } from '../../../../_services/chung.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { UserService } from '../../../../_services/user.service';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';

@Component({
  selector: 'app-svhome',
  templateUrl: './svhome.component.html',
  // styleUrls: ['./svhome.component.scss']
})
export class SvhomeComponent implements OnInit {
  thoigiandanhgia: any;
  id_sv: string;
  quyenhan: any;
  trangthaidanhgiasv: any;

  tgsvdg: boolean;
  tgbcsdg: boolean;

  constructor(
    private ctdg_Service: ChitietdanhgiaService,
    private user_Service: UserService,
    private tgdg_Service: ThoigiandanhgiaService,
    private chung_Service: ChungService
  ) { }

  ngOnInit() {
    this.thoigiandanhgia = null;
    this.id_sv = null;
    this.trangthaidanhgiasv = 0;
    this.quyenhan = [];
    // this.tgsvdg = false;
    // this.tgbcsdg = false;
    this.tgsvdg = true;
    this.tgbcsdg = true;
    this.getInfoUserNow();
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();

    this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.getThoiGianDanhGia(responseData.hk.id);
        this.checkDanhGia(responseData.hk.id, this.id_sv);
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.thoigiandanhgia = responseData.thongtin;
        let check = false;
        this.quyenhan.forEach(element => {
          if (element.id_nhomquyen === 2) {
            check = true;
          }
        });
        if (this.thoigiandanhgia.id !== null) {
          this.taoBangDiemDanhGia(this.thoigiandanhgia.id, this.id_sv);
          // this.checkTGDanhGiaTheoPhanQuyen(this.thoigiandanhgia, check);
        }
      }
    );
  }

  checkTGDanhGiaTheoPhanQuyen(thoigiandanhgia: any, checkbcs: boolean) {
    // tslint:disable-next-line:max-line-length
    if (new Date() >= new Date(thoigiandanhgia.tgbatdau_sv) && new Date() <= new Date(thoigiandanhgia.tgketthuc_sv)) {
      this.tgsvdg = true;
      // tslint:disable-next-line:max-line-length
    } else if (new Date() >= new Date(thoigiandanhgia.tgbatdau_bancansu) && new Date() <= new Date(thoigiandanhgia.tgketthuc_bancansu)) {
      if (checkbcs) {
        this.tgbcsdg = true;
      }
    } else {
      this.tgsvdg = true;
      this.tgbcsdg = false;
    }
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 1) {
          this.id_sv = responseData.sv.id;
          this.quyenhan = responseData.quyenhan;
          this.getHKHienTai();
        }
      }
    );
  }

  checkDanhGia(id_hocky: string, id_sv: string) {
    this.ctdg_Service.checkDanhGia(id_hocky, id_sv).subscribe(
      responseData => {
        this.trangthaidanhgiasv = responseData.bddanhgia.trangthai_sv;
      }
    );
  }

  taoBangDiemDanhGia(id_thoigiandanhgia: string, id_sv: string) {
    this.ctdg_Service.postTaoBangDiemDanhGia(id_thoigiandanhgia, id_sv).subscribe(
      responseData => {
      }
    );
  }

}
