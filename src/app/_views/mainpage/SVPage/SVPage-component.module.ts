import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SvhomeComponent } from './svhome/svhome.component';
import { SVPageRouters } from './SVPage.router';
import { MaterialModule } from '../../../material.module';
import { SharedComponentModule } from '../../SharedComponent.module';
import { ChitietDanhgiaSinhvienComponent } from '../SVPage/chitiet-danhgia-sinhvien/chitiet-danhgia-sinhvien.component';
import { ChitietDanhgiaBancansuComponent } from './chitiet-danhgia-bancansu/chitiet-danhgia-bancansu.component';
import { FormsModule } from '@angular/forms';
import { ThongTinSinhVienComponent } from './thong-tin-sinh-vien/thong-tin-sinh-vien.component';
import { LichsudanhgiaComponent } from '../main_shared_component/lichsudanhgia/lichsudanhgia.component';
import { SuaDanhgiaSinhvienComponent } from './sua-danhgia-sinhvien/sua-danhgia-sinhvien.component';
import { SuaDanhgiaBancansuComponent } from './sua-danhgia-bancansu/sua-danhgia-bancansu.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    MaterialModule,
    SVPageRouters,
    SharedComponentModule,
    NgxPaginationModule,
  ],
  declarations: [
    SvhomeComponent,
    ThongTinSinhVienComponent,
    ChitietDanhgiaSinhvienComponent,
    ChitietDanhgiaBancansuComponent,
    SuaDanhgiaSinhvienComponent,
    SuaDanhgiaBancansuComponent,
  ],
  exports: [
    SvhomeComponent,
    ThongTinSinhVienComponent,
    ChitietDanhgiaSinhvienComponent,
    ChitietDanhgiaBancansuComponent,
    SuaDanhgiaSinhvienComponent,
    SuaDanhgiaBancansuComponent,
  ]
})
export class SVPageComponentModule { }
