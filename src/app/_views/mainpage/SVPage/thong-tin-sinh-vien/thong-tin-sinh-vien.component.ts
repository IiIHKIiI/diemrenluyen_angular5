import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../../_services/user.service';
import { ChungService } from '../../../../_services/chung.service';
import { MatDialog } from '@angular/material';
import { DoimatkhaudialogComponent } from '../../main_shared_component/doimatkhaudialog/doimatkhaudialog.component';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-thong-tin-sinh-vien',
  templateUrl: './thong-tin-sinh-vien.component.html',
  // styleUrls: ['./thong-tin-sinh-vien.component.scss']
})
export class ThongTinSinhVienComponent implements OnInit {
  infoSV: any;
  chucvu: any;
  quyentk: string;
  id_user: string;
  isDoiMatKhau = 0;
  constructor(
    private chung_Service: ChungService,
    private user_Service: UserService,
    private route: Router,
    private dialog: MatDialog,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.infoSV = {};
    this.chucvu = null;
    this.quyentk = null;
    this.getInfoUserNow();
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 1) {
          this.id_user = responseData.user.id;
          this.isDoiMatKhau = responseData.user.isfirst;
          this.infoSV = responseData.sv;
          if (responseData.chucvu != null) {
            this.chucvu = responseData.chucvu.chucvu;
            this.quyentk = responseData.tennhomquyen;
          }
        }
      }
    );
  }

  changePasswordDialog() {
    const dialogRef = this.dialog.open(DoimatkhaudialogComponent, {
      width: '600px',
      data: { id_user: this.id_user }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'success') {
        this.toastr.success('Đã thay đổi mật khẩu', 'THÀNH CÔNG !!!');
        this.getInfoUserNow();
      } else if (result === 'failed') {
        this.toastr.error(result, 'LỖI!!!');
      } else {
      }
    });
  }
}
