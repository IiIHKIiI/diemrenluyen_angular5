import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ChungService } from '../../../../_services/chung.service';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { UserService } from '../../../../_services/user.service';
import { ChitietdanhgiaService } from '../../../../_services/chitietdanhgia.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { ThemminhchungDialogComponent } from '../../main_shared_component/themminhchung-dialog/themminhchung-dialog.component';
import { KyluatService } from '../../../../_services/kyluat.service';

@Component({
  selector: 'app-chitiet-danhgia-sinhvien',
  templateUrl: './chitiet-danhgia-sinhvien.component.html',
  // styleUrls: ['./chitiet-danhgia-sinhvien.component.scss']
})
export class ChitietDanhgiaSinhvienComponent implements OnInit {
  mahocky: string;
  manamhoc: string;

  DanhSachTieuChi: any;
  DanhSachKyLuat = [];
  oldtc = {};
  danhgia = {};

  dsminhchung = {};
  name_minhchung = [];

  id_user: string;
  id_sv: string;
  id_bangdiemdanhgia: string;
  id_thoigiandanhgia: string;
  file_minhchung = [];
  constructor(
    private kyluat_Service: KyluatService,
    private dialog: MatDialog,
    private ctdg_Service: ChitietdanhgiaService,
    private user_Service: UserService,
    private tgdg_Service: ThoigiandanhgiaService,
    private tcdg_Service: TieuchidanhgiaService,
    private chung_Service: ChungService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.getHKHienTai();
  }

  backBtn() {
    this.location.back();
  }

  getHKHienTai() {
    const ht = new Date();
    const dateStr = ht.getFullYear() + '-' + (ht.getMonth() + 1) + '-' + ht.getDate();

    return this.chung_Service.postKiemTraHocKy(dateStr).subscribe(
      responseData => {
        this.mahocky = responseData.hk.mahocky;
        this.manamhoc = responseData.hk.manamhoc;
        this.getThoiGianDanhGia(responseData.hk.id);
      }
    );
  }

  getThoiGianDanhGia(id_hocky: string) {
    this.tgdg_Service.getThoiGianDanhGiaByIdHocKy(id_hocky).subscribe(
      responseData => {
        this.id_thoigiandanhgia = responseData.thongtin.id;
        this.getTieuChiDanhGia(responseData.thongtin.id_tieuchidanhgia);
        this.getInfoUserNow();
      }
    );
  }

  getTieuChiDanhGia(id_tieuchidanhgia: string) {
    this.tcdg_Service.getTieuChiDanhGia(id_tieuchidanhgia).subscribe(
      responseData => {
        this.DanhSachTieuChi = responseData.tieuchi;
      }
    );
  }

  getInfoUserNow() {
    return this.user_Service.getInfoUser().subscribe(
      responseData => {
        if (responseData.user.loaiuser === 1) {
          this.id_sv = responseData.sv.id;
          this.id_user = responseData.user.id;
          this.getIDBangDiem(this.id_thoigiandanhgia, responseData.sv.id);
          this.getAllDanhSachKyLuat(responseData.sv.id_lop);
        }
      }
    );
  }

  getAllDanhSachKyLuat(id_lop: string) {
    return this.kyluat_Service.getAllDanhSachKyLuat(id_lop).subscribe(
      responseData => {
        this.DanhSachKyLuat = responseData.danhsach;
      }
    );
  }

  getIDBangDiem(id_thoigiandanhgia: string, id_sv: string) {
    this.chung_Service.getBangDiemDanhGiaByIDThoiGian_SV(id_thoigiandanhgia, id_sv).subscribe(
      responseData => {
        this.id_bangdiemdanhgia = responseData.bangdiem.id;
      }
    );
  }

  objectToArray(obj: Object) {
    const tmp = [];
    // tslint:disable-next-line:forin
    for (const key in obj) {
      tmp.push([key, obj[key]]);
    }
    return tmp;
  }

  openDialogMinhhChung(tieuchi: any) {
    const dialogRef = this.dialog.open(ThemminhchungDialogComponent, {
      width: '700px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.dsminhchung[tieuchi.id] = result;
        this.name_minhchung = this.objectToArray(this.dsminhchung);
      }
    });

  }

  deleteFile(fileminhchung: any) {
    delete this.dsminhchung[fileminhchung[0]];
    this.name_minhchung.forEach((element, index) => {
      if (element[0] === fileminhchung[0]) {
        this.name_minhchung.splice(index, 1);
      }
    });
  }

  ganGiaTri(tieuchi: any) {
    this.danhgia[tieuchi.id] = tieuchi.diemtoida.toString();
    const tcc = tieuchi.id_tieuchicha;

    if (this.oldtc['' + tcc]) {

      delete this.danhgia[this.oldtc['' + tcc]];
    }
    this.oldtc['' + tcc] = tieuchi.id;
  }

  tinhTongDiem(diemdanhgia: any): any {
    let tongdiem = 0;
    // tslint:disable-next-line:forin
    for (const key in diemdanhgia) {
      let diem = 0;
      if (diemdanhgia[key] !== '') {
        // tslint:disable-next-line:radix
        diem = parseInt(diemdanhgia[key]);
      } else {
        diem = 0;
      }
      tongdiem += diem;
    }
    if (tongdiem > 100) {
      tongdiem = 100;
    }
    return tongdiem;
  }

  openDialog(): void {
    let data = {};

    const tong_diem = this.tinhTongDiem(this.danhgia);

    if (this.DanhSachKyLuat !== null) {
      this.DanhSachKyLuat.forEach(e => {
        if (e.id_sv === this.id_sv) {
          if (e.id_loaikyluat === 1) {
            // tslint:disable-next-line:max-line-length
            data = { message: 'Bạn đang bị kỷ luật mức: \"Khiển trách\". Tổng điểm đánh giá của bạn sẽ được không được vượt quá loại khá. Tổng điểm đánh giá:  ', name: tong_diem.toString() };
          } else if (e.id_loaikyluat === 2) {
            if (this.tinhTongDiem(this.danhgia) > 64) {
              // tslint:disable-next-line:max-line-length
              data = { message: 'Bạn đang bị kỷ luật mức: \"Cảnh cáo\". Tổng điểm đánh giá của bạn sẽ được không được vượt quá loại trung bình. Tổng điểm đánh giá:  ', name: tong_diem.toString() };
            }
          }
        } else {
          data = { message: 'Tổng điểm đánh giá: ', name: tong_diem.toString() };
        }
      });
    } else {
      data = { message: 'Tổng điểm đánh giá: ', name: tong_diem.toString() };
    }

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '400px',
      data: data
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {

    let tongdiem = this.tinhTongDiem(this.danhgia);
    if (this.DanhSachKyLuat !== null) {
      this.DanhSachKyLuat.forEach(e => {
        if (e.id_sv === this.id_sv) {
          if (e.id_loaikyluat === 1) {
            if (tongdiem > 79) {
              tongdiem = 79;
            }
          } else if (e.id_loaikyluat === 2) {
            if (tongdiem > 64) {
              tongdiem = 64;
            }
          }
        }
      });
    }
    const input: FormData = new FormData();
    input.append('id_sv', this.id_sv);
    input.append('loaiuserdanhgia', '1');
    input.append('id_user', this.id_user);
    input.append('id_bangdiemdanhgia', this.id_bangdiemdanhgia);
    input.append('tongdiem', tongdiem.toString());
    input.append('ctdanhgia', JSON.stringify(this.danhgia));
    // tslint:disable-next-line:forin
    for (const key in this.dsminhchung) {
      input.append('dsminhchung[' + key + ']', this.dsminhchung[key]);
    }

    return input;
  }


  onSubmit() {

    const formModel = this.prepareSave();

    return this.ctdg_Service.postChiTietDanhGia(formModel).subscribe(
      responseData => {
        if (responseData.error) {
          this.toastr.error('THÊM KHÔNG THÀNH CÔNG !!!');
        } else {
          this.toastr.success(responseData.message, 'ĐÁNH GIÁ THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/sinhvien/lichsudanhgia');
        }
      },
      err => {
        if (err) {
          console.log(err.error);
          this.toastr.error(err.error, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

}
