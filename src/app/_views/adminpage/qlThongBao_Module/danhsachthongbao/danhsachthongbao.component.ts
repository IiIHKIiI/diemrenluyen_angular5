import { Component, OnInit } from '@angular/core';
import { ThongbaoService } from '../../../../_services/thongbao.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachthongbao',
  templateUrl: './danhsachthongbao.component.html',
  // styleUrls: ['./danhsachthongbao.component.scss']
})
export class DanhsachthongbaoComponent implements OnInit {
  p = 0;
  total: any;
  DanhSachThongBao = [];
  constructor(
    private tb_Service: ThongbaoService,
    private route: Router,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getDanhSachThongBao();
  }

  backBtn() {
    this.location.back();
  }


  getDanhSachThongBao() {
    this.tb_Service.getAllDanhSachThongBao().subscribe(
      responseData => {
        this.DanhSachThongBao = responseData.danhsach;
        this.total = this.DanhSachThongBao.length;
      }
    );
  }

  openDialog(id: string, tieude: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: tieude, message: 'Bạn thực sự muốn xóa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  /* Xóa bộ môn theo id*/
  onDelete(id: string) {
    return this.tb_Service.deleteXoaThongBao(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa thông báo này.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA THÔNG BÁO THÀNH CÔNG !!!');
          this.getDanhSachThongBao();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialog2(id: string, trangthai: number, tieude: string): void {

    if (trangthai === 1) {

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: { name: tieude, message: 'Bạn thực sự muốn dừng hiển thị thông báo ' }
      });

      dialogRef.afterClosed().subscribe(selection => {
        if (selection) {
          this.lockOrOpen('0', id);
        }
      });
    } else {

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: { name: tieude, message: 'Bạn thực sự muốn hiển thị lại thông báo ' }
      });

      dialogRef.afterClosed().subscribe(selection => {
        if (selection) {
          this.lockOrOpen('1', id);
        }
      });
    }
  }

  lockOrOpen(trangthai_tmp: string, id: string) {

    const trangthai = { trangthai: trangthai_tmp };
    this.tb_Service.putLockOrOpenTB(trangthai, id).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
        this.getDanhSachThongBao();
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
