import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { StepperOptions } from 'ngx-stepper';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { UserService } from '../../../../_services/user.service';
import { ThongbaoService } from '../../../../_services/thongbao.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-suathongbao',
  templateUrl: './suathongbao.component.html',
  // styleUrls: ['./suathongbao.component.scss']
})
export class SuathongbaoComponent implements OnInit {
  infoThongBao_Form: FormGroup;
  public options: StepperOptions = {
    linear: false,
  };
  ckeditorContent = '';
  namhoc_control = new FormControl('', Validators.required);
  DanhSachNamHoc: any;
  DanhSachHocKy: any;
  fileToUpload: any;
  id_user: string;
  selectedValue: any;
  id: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private tb_Service: ThongbaoService,
    private user_Service: UserService,
    private nhoc_hky_Service: NamhochockyService,
    private route: Router,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.fileToUpload = null;
    this.setForm();
    this.getAllHocKy();
    this.getDanhSachNamHoc();
    this.getInfoUser();
    this.setDefaultData();
    this.getInfoThongBao();
  }

  backBtn() {
    this.location.back();
  }


  setForm() {
    this.infoThongBao_Form = new FormGroup({
      'tieude': new FormControl('', Validators.required),
      'noidung': new FormControl('', Validators.required),
      'id_hocky': new FormControl('', Validators.required),
      'trangthai': new FormControl('', Validators.required),
      'filename_thongbao': new FormControl(''),
    });
  }

  setDefaultData() {
    this.selectedValue = {};
  }

  getDanhSachNamHoc() {
    this.nhoc_hky_Service.getDanhSachNamHoc().subscribe(
      responseData => {
        this.DanhSachNamHoc = responseData.danhsach;
      }
    );
  }

  getAllHocKy() {
    this.nhoc_hky_Service.getDanhSachHocKy().subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }

  getDanhSachHocky_NamHoc(id_namhoc: string) {
    this.nhoc_hky_Service.getDanhSachAllHocKyByNamHoc(id_namhoc).subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }

  getInfoUser() {
    this.user_Service.getInfoUser().subscribe(
      responseData => {
        this.id_user = responseData.user.id;
      }
    );
  }

  getInfoThongBao() {
    this.tb_Service.getInfoThongBao(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.thongtin;
        this.ckeditorContent = this.selectedValue.noidung;
      }
    );
  }

  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {

    const data: FormData = new FormData();
    if (this.fileToUpload !== null) {
      data.append('filename_thongbao', this.fileToUpload, this.fileToUpload.name);
    }
    data.append('id_hocky', this.infoThongBao_Form.value.id_hocky);
    data.append('tieude', this.infoThongBao_Form.value.tieude);
    data.append('noidung', this.infoThongBao_Form.value.noidung);
    data.append('trangthai', this.infoThongBao_Form.value.trangthai);
    data.append('id_user', this.id_user);

    return data;
  }

  onSubmit() {
    const info = this.prepareSave();
    this.tb_Service.postThemThongBao(info).subscribe(
      responseData => {
        console.log(responseData);
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể cập nhật thông báo', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        } else {
          this.toastr.success(responseData.message, 'CẬP NHẬT THÔNG BÁO THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/quanly/quanlythongbao');
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error('LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }
}
