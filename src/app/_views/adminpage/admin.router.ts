import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DanhSachSinhVienComponent } from './qlSinhVien_Module/danh-sach-sinh-vien/danh-sach-sinh-vien.component';
import { ThemsinhvienComponent } from './qlSinhVien_Module/themsinhvien/themsinhvien.component';
import { SuasinhvienComponent } from './qlSinhVien_Module/suasinhvien/suasinhvien.component';
import { DanhsachkhoaComponent } from './qlKhoa_Module/danhsachkhoa/danhsachkhoa.component';
import { ThemkhoaComponent } from './qlKhoa_Module/themkhoa/themkhoa.component';
import { SuakhoaComponent } from './qlKhoa_Module/suakhoa/suakhoa.component';
import { DanhsachbomondonviComponent } from './qlBoMonDonVi_Module/danhsachbomondonvi/danhsachbomondonvi.component';
import { ThembomondonviComponent } from './qlBoMonDonVi_Module/thembomondonvi/thembomondonvi.component';
import { SuabomondonviComponent } from './qlBoMonDonVi_Module/suabomondonvi/suabomondonvi.component';
import { DanhsachnganhComponent } from './qlNganh_Module/danhsachnganh/danhsachnganh.component';
import { ThemnganhComponent } from './qlNganh_Module/themnganh/themnganh.component';
import { SuanganhComponent } from './qlNganh_Module/suanganh/suanganh.component';
import { DanhsachlopComponent } from './qlLop_Module/danhsachlop/danhsachlop.component';
import { ThemlopComponent } from './qlLop_Module/themlop/themlop.component';
import { SualopComponent } from './qlLop_Module/sualop/sualop.component';
import { DanhsachtinhhinhhoctapComponent } from './qlTHHocTap_Module/danhsachtinhhinhhoctap/danhsachtinhhinhhoctap.component';
import { SuathhoctapComponent } from './qlTHHocTap_Module/suathhoctap/suathhoctap.component';
import { DanhsachtinhhinhkyluatComponent } from './qlTHKyLuat_Module/danhsachtinhhinhkyluat/danhsachtinhhinhkyluat.component';
import { ThemkyluatComponent } from './qlTHKyLuat_Module/themkyluat/themkyluat.component';
import { SuakyluatComponent } from './qlTHKyLuat_Module/suakyluat/suakyluat.component';
import { AuthGuard } from '../../_guards/auth-guard';
import { ThongtinkyluatComponent } from './qlTHKyLuat_Module/thongtinkyluat/thongtinkyluat.component';
import { DashboardComponent } from '../_layouts/dashboard/dashboard.component';
import { DanhsachtksinhvienComponent } from './qlTaiKhoan_Module/tkSinhVien_Module/danhsachtksinhvien/danhsachtksinhvien.component';
import { DanhsachtkcanboComponent } from './qlTaiKhoan_Module/tkcanbo_Module/danhsachtkcanbo/danhsachtkcanbo.component';
import { DanhsachcanboComponent } from './qlCanBo_Module/danhsachcanbo/danhsachcanbo.component';
import { ThemcanboComponent } from './qlCanBo_Module/themcanbo/themcanbo.component';
import { SuacanboComponent } from './qlCanBo_Module/suacanbo/suacanbo.component';
import { DanhsachnamhochockyComponent } from './qlNamHocHocKy_Module/danhsachnamhochocky/danhsachnamhochocky.component';
import { ThemnamhocComponent } from './qlNamHocHocKy_Module/themnamhoc/themnamhoc.component';
import { SuanamhocComponent } from './qlNamHocHocKy_Module/suanamhoc/suanamhoc.component';
import { DanhsachcvhtComponent } from './qlCoVanHocTap_Module/danhsachcvht/danhsachcvht.component';
import { ThemcovanhoctapComponent } from './qlCoVanHocTap_Module/themcovanhoctap/themcovanhoctap.component';
import { SuacovanhoctapComponent } from './qlCoVanHocTap_Module/suacovanhoctap/suacovanhoctap.component';
import { LichsuthoigiandanhgiaComponent } from './qlThoiGianDanhGia_Module/lichsuthoigiandanhgia/lichsuthoigiandanhgia.component';
import { ThemthoigiandanhgiaComponent } from './qlThoiGianDanhGia_Module/themthoigiandanhgia/themthoigiandanhgia.component';
import { SuathoigiandanhgiaComponent } from './qlThoiGianDanhGia_Module/suathoigiandanhgia/suathoigiandanhgia.component';
import { ThemtieuchidanhgiaComponent } from './qlTieuChiDanhGia_Module/themtieuchidanhgia/themtieuchidanhgia.component';
import { DanhsachtieuchidanhgiaComponent } from './qlTieuChiDanhGia_Module/danhsachtieuchidanhgia/danhsachtieuchidanhgia.component';
import { SuaquyetdinhComponent } from './qlTieuChiDanhGia_Module/suaquyetdinh/suaquyetdinh.component';
import { DanhsachthongbaoComponent } from './qlThongBao_Module/danhsachthongbao/danhsachthongbao.component';
import { ThemthongbaoComponent } from './qlThongBao_Module/themthongbao/themthongbao.component';
import { SuathongbaoComponent } from './qlThongBao_Module/suathongbao/suathongbao.component';
import { TrangchinhquanlyComponent } from './trangchinhquanly/trangchinhquanly.component';


const adminRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'danhmuc'
    },
    {
        path: 'danhmuc',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: TrangchinhquanlyComponent
            }
        ]
    },
    /* Route quản lý sinh viên */
    {
        path: 'quanlysinhvien',
        component: DashboardComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'danhsachsinhvien'
            },
            {
                path: 'danhsachsinhvien', component: DanhSachSinhVienComponent
            },
            {
                path: 'danhsachsinhvien/:id_lop', component: DanhSachSinhVienComponent
            },
            {
                path: 'themsinhvien', component: ThemsinhvienComponent
            },
            {
                path: 'suasinhvien/:id', component: SuasinhvienComponent
            }
        ]
    },
    /* Route quản lý khoa */
    {
        path: 'quanlykhoa',
        component: DashboardComponent,
        children: [
            { path: '', component: DanhsachkhoaComponent },
            { path: 'themkhoa', component: ThemkhoaComponent },
            { path: 'suakhoa/:id', component: SuakhoaComponent }
        ]
    },
    /* Route quản lý bộ môn */
    {
        path: 'quanlybomon',
        component: DashboardComponent,
        children: [
            { path: '', component: DanhsachbomondonviComponent },
            { path: 'thembomon', component: ThembomondonviComponent },
            { path: 'suabomon/:id', component: SuabomondonviComponent }
        ]
    },
    /* Route quản lý ngành */
    {
        path: 'quanlynganh',
        component: DashboardComponent,
        children: [
            { path: '', component: DanhsachnganhComponent },
            { path: 'themnganh', component: ThemnganhComponent },
            { path: 'suanganh/:id', component: SuanganhComponent }
        ]
    },
    /* Route quản lý lớp */
    {
        path: 'quanlylop',
        component: DashboardComponent,
        children: [
            { path: '', component: DanhsachlopComponent },
            { path: 'themlop', component: ThemlopComponent },
            { path: 'sualop/:id', component: SualopComponent }
        ]
    },
    /* Route quản lý tình hình học tâp */
    {
        path: 'quanlythhoctap',
        component: DashboardComponent,
        children: [
            { path: '', component: DanhsachtinhhinhhoctapComponent },
            { path: 'suathhoctap/:id', component: SuathhoctapComponent }
        ]
    },
    /* Route quản lý tình hình kỷ luật */
    {
        path: 'quanlykyluat',
        component: DashboardComponent,
        children: [
            { path: '', component: DanhsachtinhhinhkyluatComponent },
            { path: 'themkyluat', component: ThemkyluatComponent },
            { path: 'themkyluat/thongtinkyluat/:id', component: ThongtinkyluatComponent },
            { path: 'suakyluat/:id', component: SuakyluatComponent }
        ]
    },
    /* Route quản lý tài khoản người dùng */
    {
        path: 'quanlytaikhoan',
        component: DashboardComponent,
        children: [
            {
                path: '', pathMatch: 'full', redirectTo: 'sinhvien'
            },
            {
                path: 'sinhvien',
                children: [
                    { path: '', component: DanhsachtksinhvienComponent },
                ]
            },
            {
                path: 'canbo',
                children: [
                    { path: '', component: DanhsachtkcanboComponent },
                ]
            }
        ]
    },
    /* Route quản lý cán bộ */
    {
        path: 'quanlycanbo',
        component: DashboardComponent,
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'danhsachcanbo'
            },
            {
                path: 'danhsachcanbo', component: DanhsachcanboComponent
            },
            {
                path: 'danhsachcanbo/:id_bomon', component: DanhsachcanboComponent
            },
            {
                path: 'themcanbo',
                component: ThemcanboComponent
            },
            {
                path: 'suacanbo/:id',
                component: SuacanboComponent
            }
        ]
    },
    /* Route quản lý năm học - học kỳ */
    {
        path: 'quanlynamhochocky',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: DanhsachnamhochockyComponent
            },
            {
                path: 'themnamhoc',
                component: ThemnamhocComponent
            },
            {
                path: 'suanamhoc/:id',
                component: SuanamhocComponent
            }
        ]
    },
    /* Route quản lý cố vấn học tập từng lớp */
    {
        path: 'quanlycovanhoctap',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: DanhsachcvhtComponent
            },
            {
                path: 'themcovanhoctap',
                component: ThemcovanhoctapComponent
            },
            {
                path: 'suacovanhoctap/:id',
                component: SuacovanhoctapComponent
            }
        ]
    },
    /* Route quản lý thời gian đánh giá học phần */
    {
        path: 'quanlythoigiandanhgia',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: LichsuthoigiandanhgiaComponent
            },
            {
                path: 'themthoigiandanhgia',
                component: ThemthoigiandanhgiaComponent
            },
            {
                path: 'suathoigiandanhgia/:id',
                component: SuathoigiandanhgiaComponent
            }
        ]
    },
    /* Route quản lý tiêu chí đánh giá */
    {
        path: 'quanlytieuchidanhgia',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: DanhsachtieuchidanhgiaComponent
            },
            {
                path: 'themtieuchidanhgia',
                component: ThemtieuchidanhgiaComponent
            },
            {
                path: 'suaquyetdinh/:id',
                component: SuaquyetdinhComponent
            }
        ]
    },
    /* Route quản lý thông báo */
    {
        path: 'quanlythongbao',
        component: DashboardComponent,
        children: [
            {
                path: '',
                component: DanhsachthongbaoComponent
            },
            {
                path: 'themthongbao',
                component: ThemthongbaoComponent
            },
            {
                path: 'suathongbao/:id',
                component: SuathongbaoComponent
            }
        ]
    },


];

@NgModule({
    imports: [RouterModule.forChild(adminRoutes)],
    exports: [RouterModule]
})

export class AdminRouters { }
