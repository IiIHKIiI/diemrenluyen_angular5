import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoaitthoctapService } from '../../../../_services/loaitthoctap.service';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { TthoctapService } from '../../../../_services/tthoctap.service';
import * as _moment from 'moment';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { Location } from '@angular/common';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-suathhoctap',
  templateUrl: './suathhoctap.component.html',
  // styleUrls: ['./suathhoctap.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class SuathhoctapComponent implements OnInit {
  id: string;
  selectedValue: any;
  infoTTHT_form: FormGroup;

  DanhSachLoaiTTHT: any;

  fileToUpload: File = null;

  infoSV: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private loaittht_Service: LoaitthoctapService,
    private ttht_Service: TthoctapService,
    private sv_Service: SinhvienService,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setForm();
    this.setDefaultData();
    this.getDanhSachLoaiTTHT();
    this.getInfoTTHT();

  }

  backBtn() {
    this.location.back();
  }

  setDefaultData() {
    this.selectedValue = {
      'id_loaitthoctap': '',
      'trangthai': '',
      'tgbatdau': '',
      'tgketthuc': '',
      'filename_quyetdinh': ''
    };
    this.infoSV = {
      'mssv': '',
      'hoten': '',
      'tenlop': '',
      'tennganh': '',
      'tenkhoa': ''
    };
  }

  setForm() {
    this.infoTTHT_form = new FormGroup({
      id_loaitthoctap: new FormControl('', [Validators.required]),
      trangthai: new FormControl(''),
      tgbatdau: new FormControl('', [Validators.required]),
      tgketthuc: new FormControl('', [Validators.required]),
      filename_quyetdinh: new FormControl('')
    });
  }

  getDanhSachLoaiTTHT() {
    return this.loaittht_Service.getDanhSachLoaiTTHT().subscribe(
      responseData => {
        this.DanhSachLoaiTTHT = responseData.danhsach;
      }
    );
  }

  getInfoTTHT() {
    return this.ttht_Service.getInfoTTHT(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.thongtin[0];
        this.getInfoSinhVien(this.selectedValue.id_sv);
      }
    );
  }

  getInfoSinhVien(id: string) {
    this.sv_Service.getThongTinSinhVien(id).subscribe(
      responseData => {
        this.infoSV = responseData.thongtin;
      }
    );
  }


  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }

  formatDate_Picker(value: any) {
    const days = moment(value).date();
    const month = moment(value).month();
    const year = moment(value).year();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: 'sinh viên ' + this.selectedValue.hoten, message: 'Bạn thực sự muốn chỉnh sửa thông tin học tập của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {
    const input: FormData = new FormData();
    input.append('id_loaitthoctap', this.infoTTHT_form.value.id_loaitthoctap);
    input.append('trangthai', this.infoTTHT_form.value.trangthai);
    input.append('tgbatdau', this.formatDate_Picker(this.infoTTHT_form.value.tgbatdau));
    input.append('tgketthuc', this.formatDate_Picker(this.infoTTHT_form.value.tgketthuc));
    if (this.fileToUpload !== null) {
      input.append('filename_quyetdinh', this.fileToUpload, this.fileToUpload.name);
    }
    return input;
  }

  onSubmit() {
    const formModel: FormData = this.prepareSave();
    return this.ttht_Service.putSuaTTHT(this.id, formModel).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'CHỈNH SỬA THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlythhoctap');
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  onCancel() {
    this.location.back();
  }
}
