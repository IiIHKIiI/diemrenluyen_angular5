import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CanboService } from '../../../../_services/canbo.service';

@Component({
  selector: 'app-import-dscan-bo-dialog',
  templateUrl: './import-dscan-bo-dialog.component.html',
  // styleUrls: ['./import-dscan-bo-dialog.component.scss']
})
export class ImportDscanBoDialogComponent {
  fileToUpload: File = null;
  constructor(
    public dialogRef: MatDialogRef<ImportDscanBoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cb_Service: CanboService
  ) {
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {
    const input: FormData = new FormData();
    input.append('id_bomon_donvi', this.data.id_bomon_donvi.toString());
    if (this.fileToUpload !== null) {
      input.append('file_danhsach', this.fileToUpload, this.fileToUpload.name);
    }
    return input;
  }


  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }

  confirmSelection() {
    const info = this.prepareSave();

    this.cb_Service.importDSCanBo(info).subscribe(
      responseData => {
        if (responseData.status !== 200) {
          this.dialogRef.close('failed');
        } else {
          this.dialogRef.close('success');
        }
      },
      err => {
        this.dialogRef.close('failed');
      }
    );
  }
}
