import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { CanboService } from '../../../../_services/canbo.service';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { ImportDscanBoDialogComponent } from '../import-dscan-bo-dialog/import-dscan-bo-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachcanbo',
  templateUrl: './danhsachcanbo.component.html',
  // styleUrls: ['./danhsachcanbo.component.scss']
})
export class DanhsachcanboComponent implements OnInit {
  DanhSachCanBo = [];
  DanhSachBoMon: any;
  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;
  loaiLoc: number;
  id_bomon = '';
  DanhSachKhoa: any;
  dskhoa_control = new FormControl('', [Validators.required]);
  dsbomon_control = new FormControl('', [Validators.required]);


  isDatasource: boolean;
  constructor(
    private activatedRoute: ActivatedRoute,
    private cb_Service: CanboService,
    private bm_Service: BomondonviService,
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id_bomon = this.activatedRoute.snapshot.paramMap.get('id_bomon');
    this.loaiLoc = 1;
    this.getDanhSachKhoa();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

   reset() {
    this.dskhoa_control = new FormControl('', Validators.required);
    this.dsbomon_control = new FormControl('', Validators.required);
  }


  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachCanBo('%');
    } else if (this.loaiLoc === 2) {
      if (this.id_bomon !== null) {
        this.getDanhSachCanBo(this.id_bomon);
      }
    }
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  getDanhSachCanBo(id: string) {
    return this.cb_Service.getDanhSachCanBo_BoMon(id).subscribe(
      responseData => {
        this.DanhSachCanBo = responseData.danhsach;
        this.total = this.DanhSachCanBo.length;
        if (this.DanhSachCanBo.length === 0) {
          this.isDatasource = false;
        } else {
          this.isDatasource = true;
        }
      }
    );
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa thông tin (thông tin cá nhân, chức vụ) của cán bộ ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.cb_Service.deleteXoaCanBo(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa thông tin cán bộ này vì cán bộ này, vui lòng xóa các thông tin liên quan đến cán bộ này rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA THÔNG TIN CÁN BỘ THÀNH CÔNG !!!');
          this.locDanhSach();
        }
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialogImport() {
    const dialogRef = this.dialog.open(ImportDscanBoDialogComponent, {
      width: '700px',
      data: { id_bomon_donvi: this.id_bomon }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'success') { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.toastr.success('Đã import danh sách sinh viên', 'THÀNH CÔNG !!!');
        this.getDanhSachCanBo(this.id_bomon);
      } else if (result === 'failed') {
        this.toastr.error('Import danh sách sinh viên thất bại', 'KHÔNG THÀNH CÔNG !!!');
        this.getDanhSachCanBo(this.id_bomon);
      }
    });
  }
}
