import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { LoaichucvubcsService } from '../../../../_services/loaichucvubcs.service';
import { CanboService } from '../../../../_services/canbo.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { NgxStepperComponent, StepperOptions } from 'ngx-stepper';
import { LoaichucvucbService } from '../../../../_services/loaichucvucb.service';
import { Location } from '@angular/common';

import * as _moment from 'moment';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-themcanbo',
  templateUrl: './themcanbo.component.html',
  // styleUrls: ['./themcanbo.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class ThemcanboComponent implements OnInit {
  public options: StepperOptions = {
    linear: true,
  };
  infoCanBo_Form: FormGroup; // form group
  correctDate: any;
  dskhoa_control = new FormControl('', Validators.required);

  DanhSachKhoa: any;
  DanhSachBoMon: any;
  DanhSachChucVu: any;
  constructor(
    private loaichucvucb_Service: LoaichucvucbService,
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private cb_Service: CanboService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.setForm();
    this.getDanhSachKhoa();
    this.getDanhSachChucVuCB();
  }

  backBtn() {
    this.location.back();
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  setForm() {
    this.infoCanBo_Form = new FormGroup({
      macanbo: new FormControl('', Validators.required),
      hotencanbo: new FormControl('', Validators.required),
      gioitinh: new FormControl(),
      ngaysinh_tmp: new FormControl(new Date(1985, 0, 1)),
      cmnd: new FormControl('', [Validators.required, Validators.minLength(9)]),
      sdt: new FormControl('', [Validators.required, Validators.minLength(9)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      diachi: new FormControl('', Validators.required),
      id_bomon_donvi: new FormControl('', Validators.required),
      id_chucvu_quanly: new FormControl('', Validators.required),
    });
  }

  formatDate_Picker(value: any) {

    const days = moment(value).dates();
    const month = moment(value).months();
    const year = moment(value).years();

    // this.correctDate = days + '/' + (month + 1) + '/' + year;
    this.correctDate = year + '-' + (month + 1) + '-' + days;
    return this.correctDate;
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  getDanhSachChucVuCB() {
    this.loaichucvucb_Service.getAllDSLoaiChucVuCB().subscribe(
      reponseData => {
        this.DanhSachChucVu = reponseData.danhsach;
      }
    );
  }

  onSubmit() {

    const info = {
      macanbo: this.infoCanBo_Form.value.macanbo,
      hotencanbo: this.infoCanBo_Form.value.hotencanbo,
      gioitinh: this.infoCanBo_Form.value.gioitinh,
      ngaysinh: this.formatDate_Picker(this.infoCanBo_Form.value.ngaysinh_tmp),
      cmnd: this.infoCanBo_Form.value.cmnd,
      sdt: this.infoCanBo_Form.value.sdt,
      email: this.infoCanBo_Form.value.email,
      diachi: this.infoCanBo_Form.value.diachi,
      id_bomon_donvi: this.infoCanBo_Form.value.id_bomon_donvi,
      id_chucvu_quanly: this.infoCanBo_Form.value.id_chucvu_quanly
    };
    return this.cb_Service.postThemCanBo(info).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới mã số cán bộ này vì đã tồn tại cán bộ với mã số cán bộ được thêm', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }else {
        this.toastr.success(responseData.message, 'THÊM CÁN BỘ MỚI THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlycanbo');
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error('LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }

  onCancel() {
    this.location.back();
  }

}
