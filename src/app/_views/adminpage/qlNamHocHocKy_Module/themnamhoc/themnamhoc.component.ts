import { Component, OnInit } from '@angular/core';
import { StepperOptions } from 'ngx-stepper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NamhochockyService } from '../../../../_services/namhochocky.service';


import * as _moment from 'moment';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { Location } from '@angular/common';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-themnamhoc',
  templateUrl: './themnamhoc.component.html',
  // styleUrls: ['./themnamhoc.component.scss']
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class ThemnamhocComponent implements OnInit {
  public options: StepperOptions = {
    linear: true,
  };
  infoNamHoc_form: FormGroup;
  DanhSachNamHoc = [];
  constructor(
    private nhoc_hky_Service: NamhochockyService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.setForm();
    this.setDanhSachNamHoc();
  }

  backBtn() {
    this.location.back();
  }

  formatDate_Picker(value: any) {
    const days = moment(value).date();
    const month = moment(value).month();
    const year = moment(value).year();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  setForm() {
    this.infoNamHoc_form = new FormGroup({
      manamhoc: new FormControl('', [Validators.required]),
      tgbatdau_hk1: new FormControl('', [Validators.required]),
      tgketthuc_hk1: new FormControl('', [Validators.required]),
      tgbatdau_hk2: new FormControl('', [Validators.required]),
      tgketthuc_hk2: new FormControl('', [Validators.required]),
    });
  }

  setDanhSachNamHoc() {
    const ht = new Date();
    const yearNow = ht.getFullYear();
    for (let i = yearNow; i < yearNow + 10; i++) {
      const namhoc = i + ' - ' + (i + 1);
      this.DanhSachNamHoc.push(namhoc);
    }
  }

  onSubmit() {
    const info = {
      manamhoc: this.infoNamHoc_form.value.manamhoc,
      tgbatdau_hk1: this.formatDate_Picker(this.infoNamHoc_form.value.tgbatdau_hk1),
      tgketthuc_hk1: this.formatDate_Picker(this.infoNamHoc_form.value.tgketthuc_hk1),
      tgbatdau_hk2: this.formatDate_Picker(this.infoNamHoc_form.value.tgbatdau_hk2),
      tgketthuc_hk2: this.formatDate_Picker(this.infoNamHoc_form.value.tgketthuc_hk2)
    };

    this.nhoc_hky_Service.postThemNamHocHocKy(info).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          //  tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới năm học mới vì đã tồn tại năm học có cùng năm học được thêm.', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        } else {
          this.toastr.success(responseData.message, 'THÊM NĂM HỌC THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/quanly/quanlynamhochocky');
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }
}
