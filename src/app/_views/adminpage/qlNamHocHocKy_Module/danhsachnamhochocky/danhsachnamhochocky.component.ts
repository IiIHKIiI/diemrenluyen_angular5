import { Component, OnInit } from '@angular/core';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachnamhochocky',
  templateUrl: './danhsachnamhochocky.component.html',
  // styleUrls: ['./danhsachnamhochocky.component.scss']
})
export class DanhsachnamhochockyComponent implements OnInit {
  DanhSachNamHoc = [];
  p = 0;
  total: any;
  constructor(
    private nhoc_hky_Service: NamhochockyService,
    private toastr: ToastrService,
    private route: Router,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getDanhSachNamHoc();
  }

  backBtn() {
    this.location.back();
  }

  getDanhSachNamHoc() {
    this.nhoc_hky_Service.getDanhSachNamHocHocKy().subscribe(
      responseData => {
        this.DanhSachNamHoc = responseData.danhsach;
        this.total = this.DanhSachNamHoc.length;
      }
    );
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa thông tin năm học và các học kỳ của năm học ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.nhoc_hky_Service.deleteXoaNamHocHocKy(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa năm học này vì đã được sử dụng ở nghiệp vụ khác hoặc năm học này đang chứa dữ liệu, vui lòng xóa các dữ liệu liên quan đến năm học này rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA NĂM HỌC THÀNH CÔNG !!!');
          this.getDanhSachNamHoc();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
