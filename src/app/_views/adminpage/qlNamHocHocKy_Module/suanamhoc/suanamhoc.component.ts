import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';

import * as _moment from 'moment';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { StepperOptions } from 'ngx-stepper';
import { Location } from '@angular/common';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-suanamhoc',
  templateUrl: './suanamhoc.component.html',
  // styleUrls: ['./suanamhoc.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class SuanamhocComponent implements OnInit {
  public options: StepperOptions = {
    linear: false,
  };
  infoNamHoc_form: FormGroup;
  DanhSachNamHoc = [];

  id: string;
  selectedValue: any;
  selectedValue2: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private nhoc_hky_Service: NamhochockyService,
    private toastr: ToastrService,
    private route: Router,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setForm();
    this.setDanhSachNamHoc();
    this.setDefaultData();
    this.getInfoNamHoc();
  }

  backBtn() {
    this.location.back();
  }

  setDefaultData() {
    this.selectedValue = {};
    this.selectedValue2 = {};
  }

  setForm() {
    this.infoNamHoc_form = new FormGroup({
      manamhoc: new FormControl('', [Validators.required]),
      tgbatdau_hk1: new FormControl('', [Validators.required]),
      tgketthuc_hk1: new FormControl('', [Validators.required]),
      tgbatdau_hk2: new FormControl('', [Validators.required]),
      tgketthuc_hk2: new FormControl('', [Validators.required]),
    });
  }

  setDanhSachNamHoc() {
    const ht = new Date();
    const yearNow = ht.getFullYear();
    for (let i = yearNow; i < yearNow + 10; i++) {
      const namhoc = i + ' - ' + (i + 1);
      this.DanhSachNamHoc.push(namhoc);
    }
  }

  getInfoNamHoc() {
    this.nhoc_hky_Service.getInfo_NamHocHocKy(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.hk[0];
        this.selectedValue2 = responseData.hk[1];
      }
    );
  }

  formatDate_Picker(value: any) {
    const days = moment(value).date();
    const month = moment(value).month();
    const year = moment(value).year();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: this.infoNamHoc_form.value.manamhoc, message: 'Bạn thực sự muốn chỉnh sửa thông tin của năm học ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    const info = {
      manamhoc: this.infoNamHoc_form.value.manamhoc,
      tgbatdau_hk1: this.formatDate_Picker(this.infoNamHoc_form.value.tgbatdau_hk1),
      tgketthuc_hk1: this.formatDate_Picker(this.infoNamHoc_form.value.tgketthuc_hk1),
      tgbatdau_hk2: this.formatDate_Picker(this.infoNamHoc_form.value.tgbatdau_hk2),
      tgketthuc_hk2: this.formatDate_Picker(this.infoNamHoc_form.value.tgketthuc_hk2)
    };

    return this.nhoc_hky_Service.putSuaNamHocHocKy(info, this.id).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'CHỈNH SỬA NĂM HỌC THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlynamhochocky');
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }


}
