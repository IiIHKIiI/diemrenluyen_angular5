import { Component, OnInit } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { KhoaService } from '../../../../_services/khoa.service';
import { LopService } from '../../../../_services/lop.service';
import { NganhService } from '../../../../_services/nganh.service';
import { KyluatService } from '../../../../_services/kyluat.service';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { LoaikyluatService } from '../../../../_services/loaikyluat.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachtinhhinhkyluat',
  templateUrl: './danhsachtinhhinhkyluat.component.html',
  // styleUrls: ['./danhsachtinhhinhkyluat.component.scss']
})
export class DanhsachtinhhinhkyluatComponent implements OnInit {

  DanhSachLop = [];
  DanhSachNganh = [];
  DanhSachKhoa = [];
  DanhSachLoaiKyLuat = [];
  DanhSachKyLuat: any;

  dskhoa_control = new FormControl('', [Validators.required]);
  dsnganh_control = new FormControl('', [Validators.required]);
  dslop_control = new FormControl('', [Validators.required]);

  loaiLoc: number;
  id_nganh: number;
  id_khoa: number;
  id_lop: string;

  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;

  selectedValue_Lop: any;

  SLuong = [];

  isShowThongTinLop: boolean;
  constructor(
    private route: Router,
    private toastr: ToastrService,
    private kyluat_Service: KyluatService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private lop_Service: LopService,
    private dialog: MatDialog,
    private loaikyluat_Service: LoaikyluatService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.id_khoa = null;
    this.id_nganh = null;
    this.DanhSachKyLuat = [];
    this.setDefaultData();
    this.getDanhSachKhoa();
    this.locDanhSach();
    this.getDanhSachLoaiKyLuat();
  }

  backBtn() {
    this.location.back();
  }

  reset() {
    this.dskhoa_control = new FormControl('', [Validators.required]);
    this.dsnganh_control = new FormControl('', [Validators.required]);
    this.dslop_control = new FormControl('', [Validators.required]);
  }


  setDefaultData() {
    this.selectedValue_Lop = {
      'tenlop': '',
      'nambatdau': '',
      'namketthuc': '',
      'siso': '',
      'tencanbo': '',
      'tennganh': '',
      'tenkhoa': ''
    };
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getAllDanhSachKyLuat('%');
      this.getDemSoLuong('%');
      this.isShowThongTinLop = false;
    } else if (this.loaiLoc === 2) {
      if (this.id_khoa !== null && this.id_nganh !== null && this.id_lop !== null) {
        this.SLuong = [];
        this.getAllDanhSachKyLuat(this.id_lop);
        this.getInfoLop(this.id_lop);
        this.getDemSoLuong(this.id_lop);
        this.isShowThongTinLop = true;
      }
    }
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các lớp thuộc ngành đươc chọn */
  getDanhSachLop(id_nganh: number): void {
    this.lop_Service.getDanhSachLop_Nganh(id_nganh).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      }
    );
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  getAllDanhSachKyLuat(id_lop: string) {
    return this.kyluat_Service.getAllDanhSachKyLuat(id_lop).subscribe(
      responseData => {
        this.DanhSachKyLuat = responseData.danhsach;
        this.total = this.DanhSachKyLuat.length;
      }
    );
  }

  getInfoLop(id: string) {
    this.lop_Service.getInfoLop(id).subscribe(
      responseData => {
        this.selectedValue_Lop = responseData.lop;
        this.selectedValue_Lop.siso = responseData.siso;
      }
    );
  }

  getDanhSachLoaiKyLuat() {
    this.loaikyluat_Service.getDanhSachLoaiKyLuat().subscribe(
      responseData => {
        this.DanhSachLoaiKyLuat = responseData.danhsach;
      }
    );

  }

  getDemSoLuong(id_lop: string) {
    this.kyluat_Service.getDemSoLuong(id_lop).subscribe(
      reponseData => {
        this.SLuong = reponseData.soluong;
      }
    );
  }

  openDialog(id: string, hoten: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: 'sinh viên ' + hoten, message: 'Bạn thực sự muốn xóa thông tin kỷ luật của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.kyluat_Service.deleteXoaKyLuat(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          this.toastr.error(responseData.maloi, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');

        } else {
          this.toastr.success(responseData.message, 'XÓA THÔNG TIN KỶ LUẬT SINH VIÊN THÀNH CÔNG !!!');
          this.locDanhSach();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  downloadFile(id_sv: string) {
    this.kyluat_Service.getFileKyLuat(id_sv).subscribe();
  }

}
