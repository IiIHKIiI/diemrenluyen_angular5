import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import * as _moment from 'moment';
import { LoaikyluatService } from '../../../../_services/loaikyluat.service';
import { KyluatService } from '../../../../_services/kyluat.service';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { Location } from '@angular/common';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
@Component({
  selector: 'app-suakyluat',
  templateUrl: './suakyluat.component.html',
  // styleUrls: ['./suakyluat.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class SuakyluatComponent implements OnInit {
  id: string;
  selectedValue: any;
  infoKyLuat_form: FormGroup;

  DanhSachLoaiKyLuat: any;

  fileToUpload: File = null;

  infoSV: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private loaikyluat_Service: LoaikyluatService,
    private kyluat_Service: KyluatService,
    private sv_Service: SinhvienService,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id'); // ID kỷ luật
    this.infoSV = [];
    this.setForm();
    this.setDefaultData();
    this.getDanhSachLoaiKyLuat();
    this.getInfoKyLuat();
  }

  backBtn() {
    this.location.back();
  }

  setDefaultData() {
    this.selectedValue = {
      'id_loaikyluat': '',
      'trangthai': '',
      'tgbatdau': '',
      'tgketthuc': '',
      'filename_quyetdinh': ''
    };
    this.infoSV = {
      'mssv': '',
      'hoten': '',
      'tenlop': '',
      'tennganh': '',
      'tenkhoa': ''
    };
  }

  setForm() {
    this.infoKyLuat_form = new FormGroup({
      id_loaikyluat: new FormControl('', [Validators.required]),
      trangthai: new FormControl(''),
      tgbatdau: new FormControl(''),
      tgketthuc: new FormControl(''),
      filename_quyetdinh: new FormControl('')
    });
  }

  getInfoKyLuat() {
    return this.kyluat_Service.getInfoKyLuat(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.thongtin;
        this.getInfoSinhVien(this.selectedValue.id_sv);
      }
    );
  }


  getDanhSachLoaiKyLuat() {
    return this.loaikyluat_Service.getDanhSachLoaiKyLuat().subscribe(
      responseData => {
        this.DanhSachLoaiKyLuat = responseData.danhsach;
      }
    );
  }

  getInfoSinhVien(id: string) {
    this.sv_Service.getThongTinSinhVien(id).subscribe(
      responseData => {
        this.infoSV = responseData.thongtin;
      }
    );
  }

  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }
  formatDate_Picker(value: any) {
    const days = moment(value).date();
    const month = moment(value).month();
    const year = moment(value).year();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }
  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {
    const input: FormData = new FormData();
    input.append('id_loaikyluat', this.infoKyLuat_form.value.id_loaikyluat);
    input.append('trangthai', this.infoKyLuat_form.value.trangthai);
    if (this.infoKyLuat_form.value.tgbatdau != null) {
      input.append('tgbatdau', this.formatDate_Picker(this.infoKyLuat_form.value.tgbatdau));
    }
    if (this.infoKyLuat_form.value.tgketthuc != null) {
      input.append('tgketthuc', this.formatDate_Picker(this.infoKyLuat_form.value.tgketthuc));
    }
    if (this.fileToUpload !== null) {
      input.append('filename_quyetdinh', this.fileToUpload, this.fileToUpload.name);
    }
    return input;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: 'sinh viên ' + this.infoSV.hoten, message: 'Bạn thực sự muốn chỉnh sửa thông tin kỷ luật của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    const formModel = this.prepareSave();
    return this.kyluat_Service.putSuaKyLuat(formModel, this.id).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'CHỈNH SỬA THÔNG TIN KỶ LUẬT THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlykyluat');
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  onCancel() {
    this.location.back();
  }

}
