import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { KyluatService } from '../../../../_services/kyluat.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { LopService } from '../../../../_services/lop.service';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';

@Component({
  selector: 'app-themkyluat',
  templateUrl: './themkyluat.component.html',
  // styleUrls: ['./themkyluat.component.scss']
})
export class ThemkyluatComponent implements OnInit {

  DanhSachLop = [];
  DanhSachNganh = [];
  DanhSachKhoa = [];
  DanhSachSinhVien: any;

  dskhoa_control = new FormControl('', [Validators.required]);
  dsnganh_control = new FormControl('', [Validators.required]);
  dslop_control = new FormControl('', [Validators.required]);

  loaiLoc: number;
  id_nganh: number;
  id_khoa: number;
  id_lop: string;

  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;

  constructor(
    private route: Router,
    private toastr: ToastrService,
    private kyluat_Service: KyluatService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private lop_Service: LopService,
    private sinhvien_Service: SinhvienService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.id_khoa = null;
    this.id_nganh = null;
    this.DanhSachSinhVien = [];
    this.getDanhSachKhoa();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachSinhVien('%');
    } else if (this.loaiLoc === 2) {
      if (this.id_khoa !== null && this.id_nganh !== null && this.id_lop !== null) {
        this.getDanhSachSinhVien(this.id_lop);
      }
    }
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các lớp thuộc ngành đươc chọn */
  getDanhSachLop(id_nganh: number): void {
    this.lop_Service.getDanhSachLop_Nganh(id_nganh).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      }
    );
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  /*Lấy danh sách các sinh viên thuộc lớp đươc chọn */
  getDanhSachSinhVien(id_lop: string): void {
    this.sinhvien_Service.getDanhSachSinhVien_TheoLop(id_lop).subscribe(
      responseData => {
        this.DanhSachSinhVien = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }
}
