import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { Bomondonvi } from '../../../../_models/bomondonvi';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-thembomondonvi',
  templateUrl: './thembomondonvi.component.html',
  // styleUrls: ['./thembomondonvi.component.scss']
})
export class ThembomondonviComponent implements OnInit {

  infoBoMon_Form: FormGroup;
  DanhSachKhoa: any;
  bomon: Bomondonvi;
  constructor(
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.setForm();
    this.getDanhSachKhoa();
  }

  backBtn() {
    this.location.back();
  }

  setForm() {
    this.infoBoMon_Form = new FormGroup({
      'mabomon': new FormControl('', [Validators.required]),
      'tenbomon': new FormControl('', [Validators.required]),
      'tructhuoc': new FormControl('', [Validators.required])
    });
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  onSubmit() {
    this.bomon = this.infoBoMon_Form.value;
    this.bm_Service.postBoMon(this.bomon).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới bộ môn vì đã tồn tại bộ môn có cùng mã bộ môn được thêm.', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'THÊM BỘ MÔN MỚI THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlybomon');
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
