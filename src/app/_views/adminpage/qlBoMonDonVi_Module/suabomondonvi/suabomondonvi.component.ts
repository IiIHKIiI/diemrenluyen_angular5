import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { KhoaService } from '../../../../_services/khoa.service';
import { Bomondonvi } from '../../../../_models/bomondonvi';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { Khoa } from '../../../../_models/khoa';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-suabomondonvi',
  templateUrl: './suabomondonvi.component.html',
  // styleUrls: ['./suabomondonvi.component.scss']
})
export class SuabomondonviComponent implements OnInit {
  id: string;
  infoBoMon_Form: FormGroup;
  selectedValue: any;
  DanhSachKhoa: Khoa[] = [];
  bomon: Bomondonvi;
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private bm_Service: BomondonviService,
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setDefaultData();
    this.setForm();
    this.getDanhSachKhoa();
    this.getInfoBoMon();
  }

  backBtn() {
    this.location.back();
  }

  setDefaultData(): void {
    this.selectedValue = {
      'mabomon': '',
      'tenbomon': '',
      'tructhuoc': ''
    };
  }

  setForm() {
    this.infoBoMon_Form = new FormGroup({
      'mabomon': new FormControl('', [Validators.required]),
      'tenbomon': new FormControl('', [Validators.required]),
      'tructhuoc': new FormControl('', [Validators.required])
    });
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getInfoBoMon() {
    this.bm_Service.getInfoBoMon(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.bomon;
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: this.infoBoMon_Form.value.tenbomon, message: 'Bạn thực sự muốn chỉnh sửa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }


  onSubmit() {
    this.bomon = this.infoBoMon_Form.value;
    return this.bm_Service.putSuaBoMon(this.bomon, this.id).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          this.toastr.error('Không thể chỉnh sửa mã bộ môn vì đã tồn tại mã bộ môn này', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'CHỈNH SỬA THÔNG TIN BỘ MÔN THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlybomon');
      },
      err => {
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }
}
