import { Component, OnInit } from '@angular/core';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { ToastrService } from 'ngx-toastr';
import { Bomondonvi } from '../../../../_models/bomondonvi';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachbomondonvi',
  templateUrl: './danhsachbomondonvi.component.html',
  // styleUrls: ['./danhsachbomondonvi.component.scss']
})
export class DanhsachbomondonviComponent implements OnInit {
  DanhSachBoMon: any;
  loaiLoc: number;
  DanhSachKhoa: any;
  id_khoa: number;
  p = 1; // Phân trang được load đầu tiên
  total: number; // Tổng số record được load
  search: string;
  dskhoa_control = new FormControl('', [Validators.required]);
  constructor(
    private bmdv_Service: BomondonviService,
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.id_khoa = null;
    this.DanhSachBoMon = [];
    this.getDanhSachKhoa();
    this.getDanhSachBoMon();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  /* Lấy tất cả danh sách bộ môn */
  getDanhSachBoMon() {
    return this.bmdv_Service.getDanhSachBoMon().subscribe(responseData => {
      this.DanhSachBoMon = responseData.danhsach;
      this.total = this.DanhSachBoMon.length;
    });
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  /* Lấy tất cả danh sách bộ môn theo đơn vị trực thuộc*/
  getDanhSachBoMon_TrucThuoc(tructhuoc: number) {
    return this.bmdv_Service.getDanhSachBoMonByTrucThuoc(tructhuoc).subscribe(responseData => {
      this.DanhSachBoMon = responseData.danhsach;
      this.total = this.DanhSachBoMon.length;
    });
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachBoMon();
    } else if (this.loaiLoc === 2) {
      this.getDanhSachBoMon_TrucThuoc(1);
    } else if (this.loaiLoc === 3) {
      if (this.id_khoa !== null) {
        this.getDanhSachBoMon_TrucThuoc(this.id_khoa);
      }
    }
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  /* Xóa bộ môn theo id*/
  onDelete(id: string) {
    return this.bmdv_Service.deleteXoaBoMon(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa bộ môn này vì bộ môn này đã có ngành trực thuộc, vui lòng xóa các ngành hoặc chuyển các ngành thuộc bộ môn này sang bộ môn khác rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA BỘ MÔN THÀNH CÔNG !!!');
          this.getDanhSachBoMon();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
