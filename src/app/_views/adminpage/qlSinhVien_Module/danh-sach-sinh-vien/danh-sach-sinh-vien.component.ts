import { Component, ViewChild, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, ReactiveFormsModule, Validators, FormGroup } from '@angular/forms';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Sinhvien } from '../../../../_models/sinhvien';
import { Khoa } from '../../../../_models/khoa';
import { Nganh } from '../../../../_models/nganh';
import { Lop } from '../../../../_models/lop';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { LopService } from '../../../../_services/lop.service';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { XemchitietsinhvienDialogComponent } from '../xemchitietsinhvien-dialog/xemchitietsinhvien-dialog.component';
import { ThemsinhvienComponent } from '../themsinhvien/themsinhvien.component';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { ImportDssinhVienDialogComponent } from '../import-dssinh-vien-dialog/import-dssinh-vien-dialog.component';
import { Location } from '@angular/common';


@Component({
  selector: 'app-danh-sach-sinh-vien',
  templateUrl: './danh-sach-sinh-vien.component.html',
  // styleUrls: ['./danh-sach-sinh-vien.component.scss']
})
export class DanhSachSinhVienComponent implements OnInit {

  xemDSForm: FormGroup;

  DanhSachKhoa = []; // Lưu danh sách các khoa
  DanhSachNganh = []; // Lưu danh sách ngành
  DanhSachLop = []; // Lưu danh sách lớp
  DanhSachSinhVien = [];

  isDatasource = false;
  isShowDanhSach = false;
  isShowXemButton = false; // Enable nút Xem khi lớp được chọn
  tenlop_selected: string; // Lưu tên lớp
  idlop: string; // Lưu id lớp được chọn

  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;

  hedaotao: number;

  idlop_route = '';

  infoLop: any;
  fileToUpload: File;
  constructor(
    private activatedRoute: ActivatedRoute,
    private khoa_Service: KhoaService,
    private nganhService: NganhService,
    private lop_Service: LopService,
    private sinhvien_Service: SinhvienService,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.infoLop = {};
    this.idlop_route = this.activatedRoute.snapshot.paramMap.get('id_lop');
    this.setForm();
    this.getDanhSachKhoa();
    if (this.idlop_route !== null) {
      this.getAllDanhSachLop();
      this.getAllDanhSachNganh();
      this.getThongTinLop_DSSinhVien(this.idlop_route);
    }
  }

  backBtn() {
    this.location.back();
  }

  getThongTinLop_DSSinhVien(idlop_route: string) {
    this.isShowDanhSach = true;
    this.sinhvien_Service.setinfoKhoa_Nganh_Lop(this.xemDSForm.value, this.idlop, this.hedaotao);
    this.sinhvien_Service.getDanhSachSinhVien_TheoLop(idlop_route).subscribe(
      responseData => {
        this.DanhSachSinhVien = responseData.danhsach;
        this.tenlop_selected = responseData.danhsach[0].tenlop;
        this.infoLop = {
          'tructhuoc': responseData.danhsach[0].tructhuoc,
          'id_nganh': responseData.danhsach[0].id_nganh,
          'id_lop': responseData.danhsach[0].id_lop,
        };
        if (this.DanhSachSinhVien.length === 0) {
          this.isDatasource = false;
        } else {
          this.isDatasource = true;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  applyFilter(value: string) {
    this.search = value.trim();
  }

  /* Tạo form để lưu dữ liệu truyền đi các component khác */
  setForm(): void {
    this.xemDSForm = new FormGroup({
      // Check điều kiện xem danh sách lớp: nút xem chỉ hiện khi các thành phần bên trên được chọn
      khoaControl: new FormControl('', Validators.required),
      nganhControl: new FormControl('', Validators.required),
      lopControl: new FormControl('', Validators.required),
    });
  }


  /* Lấy danh sách các khoa trong trường */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(
      responseData => {
        this.DanhSachKhoa = responseData.danhsach;
      },
      error => {
        console.log(error.status);
      }
    );
  }

  /* Lấy tất cả danh sách ngành */
  getAllDanhSachNganh() {
    return this.nganhService.getAllDanhSachNganh().subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganhService.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  /* Lấy tất cả danh sách lớp */
  getAllDanhSachLop() {
    return this.lop_Service.getDanhSachLop().subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các lớp thuộc ngành đươc chọn */
  getDanhSachLop(id_nganh: number): void {
    this.lop_Service.getDanhSachLop_Nganh(id_nganh).subscribe(
      responseData => {
        this.isShowXemButton = true;
        this.DanhSachLop = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  /* Lấy danh sách các sinh viên thuộc lớp đươc chọn */
  getTenLop(idlop: string, tenlop: string, hedaotao: number): void {
    this.idlop = idlop;
    this.hedaotao = hedaotao;
    this.tenlop_selected = tenlop;
  }

  /*Lấy danh sách các sinh viên thuộc lớp đươc chọn */
  getDanhSachSinhVien(): void {
    this.isShowDanhSach = true;
    // this.tenlop_selected = this.xemDSForm.value.lopControl;
    this.sinhvien_Service.setinfoKhoa_Nganh_Lop(this.xemDSForm.value, this.idlop, this.hedaotao);
    this.sinhvien_Service.getDanhSachSinhVien_TheoLop(this.idlop).subscribe(
      responseData => {
        this.DanhSachSinhVien = responseData.danhsach;
        if (this.DanhSachSinhVien.length === 0) {
          this.isDatasource = false;
        } else {
          this.isDatasource = true;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  openDialog(id_sv: number): void {
    this.dialog.open(XemchitietsinhvienDialogComponent, {
      width: '1200px',
      data: { id_sv: id_sv }
    });
  }

  openConfirmDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      // tslint:disable-next-line:max-line-length
      data: { name: 'sinh viên ' + name, message: 'Bạn thực sự muốn xóa thông tin (thông tin cá nhân, tài khoản,...và các thông tin liên quan) của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.sinhvien_Service.deleteXoaSinhVien(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa sinh viên này vì sinh viên này đã có các dữ liệu đã liên kết đến sinh viên này, vui lòng xóa các dữ liệu thuộc sinh viên này rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA SINH VIÊN THÀNH CÔNG !!!');
          this.getDanhSachSinhVien();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialogImport() {
    const dialogRef = this.dialog.open(ImportDssinhVienDialogComponent, {
      width: '700px',
      data: { hedaotao: this.hedaotao, id_lop: this.idlop }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'success') { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.toastr.success('Đã import danh sách sinh viên', 'THÀNH CÔNG !!!');
        this.getDanhSachSinhVien();
      } else if (result === 'failed') {
        this.toastr.error('Import danh sách sinh viên thất bại', 'KHÔNG THÀNH CÔNG !!!');
        this.getDanhSachSinhVien();
      }
    });
  }

}
