import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SinhvienService } from '../../../../_services/sinhvien.service';

@Component({
  selector: 'app-import-dssinh-vien-dialog',
  templateUrl: './import-dssinh-vien-dialog.component.html',
  // styleUrls: ['./import-dssinh-vien-dialog.component.scss']
})
export class ImportDssinhVienDialogComponent {

  fileToUpload: File = null;
  constructor(
    public dialogRef: MatDialogRef<ImportDssinhVienDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private sv_Service: SinhvienService
  ) {
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {
    const input: FormData = new FormData();
    input.append('hedaotao', this.data.hedaotao.toString());
    input.append('id_lop', this.data.id_lop.toString());
    if (this.fileToUpload !== null) {
      input.append('file_danhsach', this.fileToUpload, this.fileToUpload.name);
    }
    return input;
  }


  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }

  confirmSelection() {
    const info = this.prepareSave();

    this.sv_Service.importDSSinhVien(info).subscribe(
      responseData => {
        if (responseData.status !== 200) {
          this.dialogRef.close('failed');
        } else {
          this.dialogRef.close('success');
        }
      },
      err => {
        this.dialogRef.close('failed');
      }
    );
  }
}
