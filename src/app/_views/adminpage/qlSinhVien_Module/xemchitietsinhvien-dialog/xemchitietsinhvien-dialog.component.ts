import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { KyluatService } from '../../../../_services/kyluat.service';

@Component({
  selector: 'app-xemchitietsinhvien-dialog',
  templateUrl: './xemchitietsinhvien-dialog.component.html',
  // styleUrls: ['./xemchitietsinhvien-dialog.component.scss']
})
export class XemchitietsinhvienDialogComponent implements OnInit {
  infoSinhVien = {};
  ttht = {};
  kl = [];
  constructor(
    private svService: SinhvienService,
    private kyluat_Service: KyluatService,
    public dialogRef: MatDialogRef<XemchitietsinhvienDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.getThongTinSV(this.data.id_sv);
  }

  getThongTinSV(id: string) {
    this.svService.getChiTietSinhVien(id).subscribe(
      responseData => {
        this.infoSinhVien = responseData.thongtin;
        this.ttht = responseData.ttht;
        this.kl = responseData.kl;
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  downloadFile(id_sv: string) {
    this.kyluat_Service.getFileKyLuat(this.data.id_sv).subscribe();
  }

}
