import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { Sinhvien } from '../../../../_models/sinhvien';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Location } from '@angular/common';
import { LoaichucvubcsService } from '../../../../_services/loaichucvubcs.service';

import * as _moment from 'moment';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-themsinhvien',
  templateUrl: './themsinhvien.component.html',
  styleUrls: ['./themsinhvien.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class ThemsinhvienComponent implements OnInit {

  infoSinhVien_Form: FormGroup; // form group
  infoKhoa_Nganh_Lop: any;
  infoSinhVien: Sinhvien;
  correctDate: any;
  DanhSachChucVu: any;
  constructor(
    private svService: SinhvienService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
    private loaichucvu_bcs_Service: LoaichucvubcsService
  ) { }

  ngOnInit() {
    this.DanhSachChucVu = [];
    this.setForm();
    this.getinfoKhoa_Nganh_Lop();
    this.getDanhSachChucVu();
  }

  backBtn() {
    this.location.back();
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  setForm() {
    this.infoSinhVien_Form = new FormGroup({
      mssv: new FormControl('', Validators.required),
      hoten: new FormControl('', Validators.required),
      gioitinh: new FormControl(),
      ngaysinh_tmp: new FormControl(new Date(1990, 0, 1)),
      cmnd: new FormControl('', [Validators.required, Validators.minLength(9)]),
      sdt_canhan: new FormControl('', [Validators.required, Validators.minLength(9)]),
      sdt_gd: new FormControl('', Validators.minLength(9)),
      email: new FormControl('', [Validators.required, Validators.email]),
      diachi: new FormControl('', Validators.required),
      id_chucvu: new FormControl('')
    });
  }

  getDanhSachChucVu() {
    this.loaichucvu_bcs_Service.getAllDSLoaiChucVu().subscribe(
      responseData => {
        this.DanhSachChucVu = responseData.danhsach;
      }
    );
  }

  formatDate_Picker(value: any) {
    const days = moment(value).date();
    const month = moment(value).month();
    const year = moment(value).year();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  onSubmit() {

    const info = {
      mssv: this.infoSinhVien_Form.value.mssv,
      hoten: this.infoSinhVien_Form.value.hoten,
      gioitinh: this.infoSinhVien_Form.value.gioitinh,
      ngaysinh: this.formatDate_Picker(this.infoSinhVien_Form.value.ngaysinh_tmp),
      cmnd: this.infoSinhVien_Form.value.cmnd,
      sdt_canhan: this.infoSinhVien_Form.value.sdt_canhan,
      sdt_giadinh: this.infoSinhVien_Form.value.sdt_gd,
      email: this.infoSinhVien_Form.value.email,
      diachi: this.infoSinhVien_Form.value.diachi,
      id_lop: this.infoKhoa_Nganh_Lop.id_lop,
      id_chucvu: this.infoSinhVien_Form.value.id_chucvu,
      hedaotao: this.infoKhoa_Nganh_Lop.hedaotao
    };

    this.infoSinhVien = info;
    // console.log(info);
    return this.svService.postThemSinhVien(info).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới mã số sinh viên này vì đã tồn tại sinh viên với mã số sinh viên được thêm', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'THÊM SINH VIÊN MỚI THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlysinhvien/danhsachsinhvien/' + info.id_lop);
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error('LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }

  /* Lây tên khoa, ngành, lớp  */
  getinfoKhoa_Nganh_Lop() {
    return this.infoKhoa_Nganh_Lop = this.svService.getinfoKhoa_Nganh_Lop();
  }

  onCancel() {
    this.location.back();
  }
}
