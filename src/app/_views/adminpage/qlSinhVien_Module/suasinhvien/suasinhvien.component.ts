import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
import { SinhvienService } from '../../../../_services/sinhvien.service';
import { Sinhvien } from '../../../../_models/sinhvien';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { LopService } from '../../../../_services/lop.service';
import { Location } from '@angular/common';
import { LoaichucvubcsService } from '../../../../_services/loaichucvubcs.service';

const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-suasinhvien',
  templateUrl: './suasinhvien.component.html',
  // styleUrls: ['./suasinhvien.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class SuasinhvienComponent implements OnInit {
  infoSinhVien_Form: FormGroup; // form group
  infoSinhVien: Sinhvien;
  correctDate: any;
  id: string;
  selectedValue: any;

  DanhSachKhoa: any;
  DanhSachNganh: any;
  DanhSachLop: any;
  DanhSachChucVu: any;
  hedaotao_new: string;

  constructor(
    private khoa_Service: KhoaService,
    private nganhService: NganhService,
    private lop_Service: LopService,
    private svService: SinhvienService,
    private loaichucvu_bcs_Service: LoaichucvubcsService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private dialog: MatDialog,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setDefaultData();
    this.getDanhSachKhoa();
    this.getDanhSachNganh();
    this.getDanhSachLop();
    this.getInfoSinhvien();
    this.getDanhSachChucVu();
    this.setForm();

  }

  backBtn() {
    this.location.back();
  }

  setDefaultData(): void {
    this.selectedValue = {
      'mssv': '',
      'hoten': '',
      'gioitinh': '',
      'ngaysinh': '',
      'cmnd': '',
      'sdt_canhan': '',
      'sdt_giadinh': '',
      'email': '',
      'diachi': '',
      'tructhuoc': '',
      'id_nganh': '',
      'id_lop': '',
      'id_loaichucvu_bcs': '',
      'hedaotao': ''
    };
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  setForm() {
    this.infoSinhVien_Form = new FormGroup({
      mssv: new FormControl('', Validators.required),
      hoten: new FormControl('', Validators.required),
      gioitinh: new FormControl('', Validators.required),
      ngaysinh_tmp: new FormControl(new Date(1990, 0, 1)),
      cmnd: new FormControl('', [Validators.required, Validators.minLength(9)]),
      sdt_canhan: new FormControl('', [Validators.required, Validators.minLength(9)]),
      sdt_giadinh: new FormControl('', Validators.minLength(9)),
      email: new FormControl('', [Validators.required, Validators.email]),
      diachi: new FormControl('', Validators.required),
      id_lop: new FormControl('', Validators.required),
      id_chucvu: new FormControl('')
    });
  }

  /* Lấy danh sách các khoa trong trường */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(
      responseData => {
        this.DanhSachKhoa = responseData.danhsach;
      },
      error => {
        console.log(error.status);
      }
    );
  }

  /* Lấy tất cả danh sách ngành */
  getDanhSachNganh() {
    return this.nganhService.getAllDanhSachNganh().subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh_khoa(id_khoa: number): void {
    this.nganhService.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  /* Lấy tất cả danh sách lớp */
  getDanhSachLop() {
    return this.lop_Service.getDanhSachLop().subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các lớp thuộc ngành đươc chọn */
  getDanhSachLop_nganh(id_nganh: number): void {
    this.lop_Service.getDanhSachLop_Nganh(id_nganh).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      },
      error => {
        console.log(error);
      }
    );
  }

  getInfoSinhvien() {
    return this.svService.getThongTinSinhVien(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.thongtin;
      }
    );
  }

  getDanhSachChucVu() {
    this.loaichucvu_bcs_Service.getAllDSLoaiChucVu().subscribe(
      responseData => {
        this.DanhSachChucVu = responseData.danhsach;
      }
    );
  }

  getInfoLop(id_lop: string) {
    this.lop_Service.getInfoLop(id_lop).subscribe(
      responseData => {
        this.hedaotao_new = responseData.lop.hedaotao;
      }
    );
  }

  formatDate_Picker(value: any) {
    const days = moment(value).date();
    const month = moment(value).month();
    const year = moment(value).year();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      // tslint:disable-next-line:max-line-length
      data: { message: 'Bạn thực sự muốn chỉnh sửa thông tin của ', name: 'sinh viên ' + this.infoSinhVien_Form.value.hoten }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    let hedaotao = '';
    if (this.hedaotao_new !== null && this.hedaotao_new !== this.selectedValue.hedaotao) {
      hedaotao = this.hedaotao_new;
    }

    const info = {
      mssv: this.infoSinhVien_Form.value.mssv,
      hoten: this.infoSinhVien_Form.value.hoten,
      gioitinh: this.infoSinhVien_Form.value.gioitinh,
      ngaysinh: this.formatDate_Picker(this.infoSinhVien_Form.value.ngaysinh_tmp),
      cmnd: this.infoSinhVien_Form.value.cmnd,
      sdt_canhan: this.infoSinhVien_Form.value.sdt_canhan,
      sdt_giadinh: this.infoSinhVien_Form.value.sdt_giadinh,
      email: this.infoSinhVien_Form.value.email,
      diachi: this.infoSinhVien_Form.value.diachi,
      id_lop: this.infoSinhVien_Form.value.id_lop,
      id_chucvu: this.infoSinhVien_Form.value.id_chucvu,
      hedaotao: hedaotao
    };

    this.infoSinhVien = info;

    return this.svService.putSuaSinhVien(this.id, this.infoSinhVien).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          //  tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể chỉnh sửa mã số sinh viên này vì đã tồn tại sinh viên với mã số sinh viên được chỉnh sửa', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'CHỈNH SỬA THÔNG TIN SINH VIÊN THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlysinhvien/danhsachsinhvien/' + info.id_lop);
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  onCancel() {
    this.location.back();
  }
}
