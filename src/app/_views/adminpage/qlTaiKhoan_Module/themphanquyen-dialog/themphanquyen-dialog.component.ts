import { Component, OnInit, Inject } from '@angular/core';
import { NhomquyenService } from '../../../../_services/nhomquyen.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { PhanquyenService } from '../../../../_services/phanquyen.service';
import { Observable } from 'rxjs/Observable';
import { CanboService } from '../../../../_services/canbo.service';
import { UserService } from '../../../../_services/user.service';

@Component({
  selector: 'app-themphanquyen-dialog',
  templateUrl: './themphanquyen-dialog.component.html',
  // styleUrls: ['./themphanquyen-dialog.component.scss']
})
export class ThemphanquyenDialogComponent implements OnInit {
  phanQuyen_Form: FormGroup;
  DanhSachLoaiQuyen: any;
  selectedValue: any;
  constructor(
    private user_Service: UserService,
    private pq_Service: PhanquyenService,
    private nquyen_Service: NhomquyenService,
    public dialogRef: MatDialogRef<ThemphanquyenDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.setForm();
    this.getDanhSachLoaiQuyenSV();
    if (!this.data.isCreateCB) {
      this.getPQ(this.data.id);
    }
  }

  setForm() {
    this.phanQuyen_Form = new FormGroup({
      nhomquyen: new FormControl('', Validators.required)
    });
  }

  /* Lấy loại quyền dựa trên biến data: loaiuser */
  getDanhSachLoaiQuyenSV() {
    if (this.data.loaiuser === 0) {
      this.nquyen_Service.getDanhSachLoaiQuyenSV().subscribe(
        responseData => {
          this.DanhSachLoaiQuyen = responseData.danhsach;
        }
      );
    } else {
      this.nquyen_Service.getDanhSachLoaiQuyenCB().subscribe(
        responseData => {
          this.DanhSachLoaiQuyen = responseData.danhsach;
        }
      );
    }
  }

  /* Lấy thông tin phân quyền theo id user */
  getPQ(id: string) {
    this.pq_Service.getDSPhanQuyenByIDUser(id).subscribe(
      responseData => {
        const arr = [];
        responseData.danhsach.forEach(element => {
          arr.push(element.id_nhomquyen);
        });
        this.selectedValue = arr;
      }
    );

  }

  /* Tạo tài khoản cho một cán bộ */
  createSingleAccount(macanbo_tmp: string, nhomquyen: any) {
    const macanbo = { macanbo: macanbo_tmp, id_nhomquyen: nhomquyen };
    return this.user_Service.postCreateAccountCB(macanbo).subscribe(
      (response: Response) => {

      }, err => {
        if (err) {
          console.log(err);

        }
      }
    );
  }

  confirmSelection() {
    if (this.data.isCreateCB) {
      if (this.createSingleAccount(this.data.macanbo, this.phanQuyen_Form.value.nhomquyen)) {
        this.dialogRef.close('Closed');
      }
    } else {
      this.addPhanQuyen();
      this.dialogRef.close('Closed');

    }
  }

  addPhanQuyen() {
    return this.pq_Service.postPhanQuyen(this.phanQuyen_Form.value.nhomquyen, this.data.id).subscribe(response => { });
  }

}
