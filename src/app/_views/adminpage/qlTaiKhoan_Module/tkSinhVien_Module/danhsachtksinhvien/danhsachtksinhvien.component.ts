import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TthoctapService } from '../../../../../_services/tthoctap.service';
import { KhoaService } from '../../../../../_services/khoa.service';
import { NganhService } from '../../../../../_services/nganh.service';
import { LopService } from '../../../../../_services/lop.service';
import { MatDialog } from '@angular/material';
import { UserService } from '../../../../../_services/user.service';
import { ConfirmDialogComponent } from '../../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Observable } from 'rxjs/Observable';
import { ThemphanquyenDialogComponent } from '../../themphanquyen-dialog/themphanquyen-dialog.component';
import { PhanquyenService } from '../../../../../_services/phanquyen.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachtksinhvien',
  templateUrl: './danhsachtksinhvien.component.html',
  // styleUrls: ['./danhsachtksinhvien.component.scss']
})
export class DanhsachtksinhvienComponent implements OnInit {
  DanhSachLop = [];
  DanhSachNganh = [];
  DanhSachKhoa = [];
  DanhSachTaiKhoan = [];
  DanhSachSVChuaCoTaiKhoan = [];
  DanhSachPhanQuyen = [];

  dskhoa_control = new FormControl('', [Validators.required]);
  dsnganh_control = new FormControl('', [Validators.required]);
  dslop_control = new FormControl('', [Validators.required]);

  loaiLoc: number;
  id_nganh: number;
  id_khoa: number;
  id_lop: string;

  p = 1; // Phân trang được load đầu tiên
  p2 = 1; // Phân trang được load đầu tiên
  total: number;
  total2: number;
  search: string;
  search2: string;

  removable = true;
  constructor(
    private route: Router,
    private toastr: ToastrService,
    private user_Service: UserService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private lop_Service: LopService,
    private dialog: MatDialog,
    private phanquyen_Service: PhanquyenService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id_khoa = null;
    this.id_nganh = null;
    this.getDanhSachKhoa();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  locDanhSach() {
    if (this.id_khoa !== null && this.id_nganh !== null && this.id_lop !== null) {
      this.getAllDanhSachTaiKhoan(this.id_lop);
    }
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các lớp thuộc ngành đươc chọn */
  getDanhSachLop(id_nganh: number): void {
    this.lop_Service.getDanhSachLop_Nganh(id_nganh).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      }
    );
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  applyFilter2(value: string) {
    value = value.trim();
    this.search2 = value;
  }

  getAllDanhSachTaiKhoan(id_lop: string) {
    this.DanhSachPhanQuyen = [];
    this.user_Service.getAllDanhSachTaiKhoanSV(id_lop).subscribe(
      responseData => {
        this.DanhSachTaiKhoan = responseData.danhsachuser;
        this.DanhSachSVChuaCoTaiKhoan = responseData.danhsachuser_chuacotaikhoan;
        this.total = this.DanhSachTaiKhoan.length;
        this.total2 = this.DanhSachSVChuaCoTaiKhoan.length;
      }
    );
  }

  loopCreateAccount() {
    return this.DanhSachSVChuaCoTaiKhoan.forEach(element => {
      this.user_Service.postCreateAccount(element)
        .subscribe(
          (response: Response) => { });
    });
  }

  /* Tạo tất cả tài khoản cho tất cả sinh viên chưa có tài khoản */
  createAllAccount() {
    if (this.loopCreateAccount()) {
      this.toastr.success('Đã tạo tài khoản cho các sinh viên chưa có tài khoản', 'THÀNH CÔNG !!!');
      this.locDanhSach();
    } else {
      this.toastr.error('Đã có lỗi trong quá trình tạo', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      this.locDanhSach();
    }
  }

  /* Tạo tài khoản cho một sinh viên */
  createSingleAccount(mssv_tmp: string) {
    const mssv = { mssv: mssv_tmp };
    this.user_Service.postCreateAccount(mssv).subscribe(
      (response: Response) => {
        this.toastr.success('Đã tạo tài khoản cho các sinh viên chưa có tài khoản', 'THÀNH CÔNG !!!');
        this.locDanhSach();
      }, err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'Tạo tài khoản không thành công. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa tài khoản của ' }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.user_Service.deleteAccount(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa tài khoản này...', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success('XÓA TÀI KHOẢN THÀNH CÔNG !!!');
          this.locDanhSach();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialog2(id: string, trangthai: number, name: string): void {

    if (trangthai === 1) {

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: { name: name, message: 'Bạn thực sự muốn khóa tài khoản của ' }
      });

      dialogRef.afterClosed().subscribe(selection => {
        if (selection) {
          this.lockOrOpen('0', id);
        }
      });
    } else {

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: { name: name, message: 'Bạn thực sự muốn kích hoạt lại tài khoản của ' }
      });

      dialogRef.afterClosed().subscribe(selection => {
        if (selection) {
          this.lockOrOpen('1', id);
        }
      });
    }
  }

  lockOrOpen(trangthai_tmp: string, id_user: string) {

    const trangthai = { trangthai: trangthai_tmp };
    this.user_Service.putLockOrOpenAccount(trangthai, id_user).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
        this.locDanhSach();
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialogAddPQ(id_user: string) {
    const dialogRef = this.dialog.open(ThemphanquyenDialogComponent, {
      width: '500px',
      data: { id: id_user, message: 'Bạn thực sự muốn thay đổi quyền người dùng tài khoản của ', loaiuser: 0, isCreateCB: 0 }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) {
        this.toastr.success('Thêm quyền thành công', 'THÀNH CÔNG !!!');
        this.locDanhSach();
      }
    });
  }

  openDialogResetPass(id_user: string, name: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn khôi phục lại mật khẩu mặc định cho tài khoản của sinh viên ' }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.resetPass(id_user);
      }
    });
  }

  resetPass(id_user: string) {
    this.user_Service.resetPass(id_user).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
