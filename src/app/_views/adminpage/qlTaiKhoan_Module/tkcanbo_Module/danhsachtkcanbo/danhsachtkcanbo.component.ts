import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../../../../_services/user.service';
import { KhoaService } from '../../../../../_services/khoa.service';
import { BomondonviService } from '../../../../../_services/bomondonvi.service';
import { MatDialog } from '@angular/material';
import { PhanquyenService } from '../../../../../_services/phanquyen.service';
import { ConfirmDialogComponent } from '../../../../_layouts/confirm-dialog/confirm-dialog.component';
import { ThemphanquyenDialogComponent } from '../../themphanquyen-dialog/themphanquyen-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachtkcanbo',
  templateUrl: './danhsachtkcanbo.component.html',
  // styleUrls: ['./danhsachtkcanbo.component.scss']
})
export class DanhsachtkcanboComponent implements OnInit {
  DanhSachBoMon: any;
  loaiLoc: number;
  // id_bomon: string;
  // id_khoa: number;
  DanhSachKhoa: any;
  dskhoa_control = new FormControl('', [Validators.required]);
  dsbomon_control = new FormControl('', [Validators.required]);

  p = 1; // Phân trang được load đầu tiên
  p2 = 1; // Phân trang được load đầu tiên
  total: number;
  total2: number;
  search: string;
  search2: string;

  DanhSachTaiKhoan = [];
  DanhSachCBChuaCoTaiKhoan = [];

  constructor(
    private route: Router,
    private toastr: ToastrService,
    private user_Service: UserService,
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private dialog: MatDialog,
    private phanquyen_Service: PhanquyenService,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.getDanhSachKhoa();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  reset() {
    this.dskhoa_control = new FormControl('', Validators.required);
    this.dsbomon_control = new FormControl('', Validators.required);
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getAllDanhSachTaiKhoan('%');
    } else if (this.loaiLoc === 2) {
      if (this.dskhoa_control.value !== null && this.dsbomon_control.value !== null) {
        this.getAllDanhSachTaiKhoan(this.dsbomon_control.value);
      }
    }

  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  applyFilter2(value: string) {
    value = value.trim();
    this.search2 = value;
  }

  getAllDanhSachTaiKhoan(id_bomon_donvi: string) {
    this.user_Service.getAllDanhSachTaiKhoanCB(id_bomon_donvi).subscribe(
      responseData => {
        this.DanhSachTaiKhoan = responseData.danhsachuser;
        this.DanhSachCBChuaCoTaiKhoan = responseData.danhsachuser_chuacotaikhoan;
        this.total2 = this.DanhSachCBChuaCoTaiKhoan.length;
        this.total = this.DanhSachTaiKhoan.length;
      }
    );
  }

  openDialog2(id: string, trangthai: number, name: string): void {

    if (trangthai === 1) {

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: { name: name, message: 'Bạn thực sự muốn khóa tài khoản của ' }
      });

      dialogRef.afterClosed().subscribe(selection => {
        if (selection) {
          this.lockOrOpen('0', id);
        }
      });
    } else {

      const dialogRef = this.dialog.open(ConfirmDialogComponent, {
        data: { name: name, message: 'Bạn thực sự muốn kích hoạt lại tài khoản của ' }
      });

      dialogRef.afterClosed().subscribe(selection => {
        if (selection) {
          this.lockOrOpen('1', id);
        }
      });
    }
  }

  lockOrOpen(trangthai_tmp: string, id_user: string) {

    const trangthai = { trangthai: trangthai_tmp };
    this.user_Service.putLockOrOpenAccount(trangthai, id_user).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
        this.locDanhSach();
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialogResetPass(id_user: string, name: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: 'cán bộ ' + name, message: 'Bạn thực sự muốn khôi phục lại mật khẩu mặc định cho tài khoản của cán bộ ' }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.resetPass(id_user);
      }
    });
  }

  resetPass(id_user: string) {
    this.user_Service.resetPass(id_user).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialogAddPQ(id_user: string, hoten: string) {
    const dialogRef = this.dialog.open(ThemphanquyenDialogComponent, {
      width: '500px',
      // tslint:disable-next-line:max-line-length
      data: { name: 'cán bộ ' + hoten, id: id_user, message: 'Bạn thực sự muốn thay đổi quyền người dùng tài khoản của ', loaiuser: 1, isCreateCB: 0 }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) {
        this.toastr.success('Đã cập nhật phân quyền', 'THÀNH CÔNG !!!');
        this.locDanhSach();
      }
    });
  }

  openDialog(id: string, hoten: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: 'cán bộ ' + name, message: 'Bạn thực sự muốn xóa tài khoản của ' }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.user_Service.deleteAccount(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa tài khoản này...', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success('XÓA TÀI KHOẢN THÀNH CÔNG !!!');
          this.locDanhSach();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  openDialogCreate(macanbo: string) {
    const dialogRef = this.dialog.open(ThemphanquyenDialogComponent, {
      // tslint:disable-next-line:max-line-length
      data: { macanbo: macanbo, message: 'Bạn thực sự muốn thay đổi quyền người dùng tài khoản của ', loaiuser: 1, isCreateCB: 1 }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) {
        this.toastr.success('Thêm quyền thành công', 'THÀNH CÔNG !!!');
        this.locDanhSach();
      }
    });
  }
}
