import { Component, OnInit, AfterContentChecked, ChangeDetectorRef } from '@angular/core';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MAT_CHECKBOX_CLICK_ACTION, MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { SuatieuchidanhgiadialogComponent } from '../suatieuchidanhgiadialog/suatieuchidanhgiadialog.component';
import * as _moment from 'moment';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { Location } from '@angular/common';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-suaquyetdinh',
  templateUrl: './suaquyetdinh.component.html',
  // styleUrls: ['./suaquyetdinh.component.scss']
  providers: [
    { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check' },
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class SuaquyetdinhComponent implements OnInit, AfterContentChecked {
  arrdiemtoida = [];

  infoTieuchi_Form: FormGroup;
  id_loaidiem = new FormControl('');
  tgbatdauhieuluc = new FormControl('', Validators.required);
  DanhSachLoaiDiem = [];
  DanhSachTieuChiThem = [];
  DanhSachChiMuc: any;

  fileToUpload: any;
  id_quyetdinh: string;
  isdiemthuong: number;
  iskhongdanhgia = 1;
  p = 0;
  total: any;
  id: string;

  selectedQD: any;
  isCheckDiemThuong = 0;
  constructor(
    private activatedRoute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private tcdanhgia_Service: TieuchidanhgiaService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id_quyetdinh = this.activatedRoute.snapshot.paramMap.get('id');
    this.isdiemthuong = 0;
    this.fileToUpload = null;
    this.selectedQD = {};
    this.mangDiemToiDa();
    this.setForm();
    this.getDanhSachLoaiDiem();
    this.getDanhSachChiMucTieuChi();
    this.getDanhSachTieuChiThem();
    this.getInfoQD_TC();
  }

  ngAfterContentChecked() {
    this.cdRef.detectChanges();
  }

  backBtn() {
    this.location.back();
  }

  getInfoQD_TC() {
    this.tcdanhgia_Service.getInfoQD_TC(this.id_quyetdinh).subscribe(
      responseData => {
        this.selectedQD = responseData.thongtin;
      }
    );
  }

  mangDiemToiDa() {
    for (let i = 1; i <= 25; i++) {
      this.arrdiemtoida.push(i);
    }
  }

  setForm() {
    this.infoTieuchi_Form = new FormGroup({
      id_tieuchicha: new FormControl(''),
      khongdanhgia: new FormControl('', Validators.required),
      chimuc_tieuchi: new FormControl('', Validators.required),
      tentieuchi: new FormControl('', Validators.required),
      is_tieuchigoc: new FormControl(''),
      diemtoida: new FormControl('', Validators.required),
      can_minhchung: new FormControl('', Validators.required),
      diemthuong: new FormControl(''),
    });
  }

  resetForm() {
    this.id_loaidiem.reset();
    this.infoTieuchi_Form.get('id_tieuchicha').setValue('');
    this.infoTieuchi_Form.get('chimuc_tieuchi').setValue('');
    this.infoTieuchi_Form.get('tentieuchi').setValue('');
    this.infoTieuchi_Form.get('is_tieuchigoc').setValue('');
    this.infoTieuchi_Form.get('diemtoida').setValue('');
    this.infoTieuchi_Form.get('can_minhchung').setValue('0');
    this.infoTieuchi_Form.get('diemthuong').setValue('0');
    this.isCheckDiemThuong = 0;
  }

  getDanhSachLoaiDiem() {
    this.tcdanhgia_Service.getDanhSachLoaiDiem().subscribe(
      responseData => {
        this.DanhSachLoaiDiem = responseData.danhsach;
      }
    );
  }

  formatDate_Picker(value: any) {

    const days = moment(value).dates();
    const month = moment(value).months();
    const year = moment(value).years();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  /* Lấy file được upload cho vào fileToUpload */
  myUploader(event) {
    if (event.files.length > 0) {
      const file = event.files[0];
      this.fileToUpload = file;
    }
  }

  removeFile() {
    this.fileToUpload = null;
  }

  /* Tạo Form Data để gửi backend (Gửi bằng form value không được) */
  private prepareSave(): any {
    const filequyetdinh: FormData = new FormData();
    if (this.fileToUpload !== null) {
      filequyetdinh.append('filename_quyetdinh', this.fileToUpload, this.fileToUpload.name);
    }
    filequyetdinh.append('tgbatdau', this.formatDate_Picker(this.tgbatdauhieuluc.value));
    return filequyetdinh;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: 'Bạn thực sự muốn cập nhật thông tin quyết định thành lập các tiêu chí đánh giá ?', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onUpdate();
      }
    });
  }

  onUpdate() {
    const filequyetdinh = this.prepareSave();

    this.tcdanhgia_Service.putSuaQuyetDinh(filequyetdinh, this.id_quyetdinh).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          this.toastr.warning('Đã tồn tại quyết định thành lập tiêu chí trong cơ sở dữ liệu. Vui lòng kiểm tra lại...', 'CẢNH BÁO !!!');
        } else {
          this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
          this.getDanhSachChiMucTieuChi();
          this.getDanhSachTieuChiThem();
        }
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  getDanhSachChiMucTieuChi() {
    if (this.id_quyetdinh !== null) {
      this.tcdanhgia_Service.getDanhSachChiMuc(this.id_quyetdinh).subscribe(
        responseData => {
          this.DanhSachChiMuc = responseData.danhsach;
        }
      );
    }
  }

  getDanhSachTieuChiThem() {
    if (this.id_quyetdinh !== null) {
      this.tcdanhgia_Service.getDanhSachTieuChiThem(this.id_quyetdinh).subscribe(
        responseData => {
          this.DanhSachTieuChiThem = responseData.danhsach;
          this.total = this.DanhSachTieuChiThem.length;
        }
      );
    }
  }

  changeTieuChiCha(tieuchicha: any) {
    if (tieuchicha === '') {
      this.isdiemthuong = 0;
    } else {
      this.isdiemthuong = 1;
    }
  }

  themTieuChi() {

    if (this.infoTieuchi_Form.value.id_tieuchicha === '') {
      this.infoTieuchi_Form.get('id_tieuchicha').setValue('');
      this.infoTieuchi_Form.get('is_tieuchigoc').setValue('1');
    } else {
      this.infoTieuchi_Form.get('is_tieuchigoc').setValue('0');
    }

    if (this.infoTieuchi_Form.value.diemthuong === 0 || this.infoTieuchi_Form.value.diemthuong === '') {
      this.infoTieuchi_Form.get('diemthuong').setValue('0');
    } else {
      this.infoTieuchi_Form.get('diemthuong').setValue('1');
    }

    const info = {
      id_quyetdinhtieuchi: this.id_quyetdinh,
      khongdanhgia: this.infoTieuchi_Form.value.khongdanhgia,
      chimuc_tieuchi: this.infoTieuchi_Form.value.chimuc_tieuchi,
      tentieuchi: this.infoTieuchi_Form.value.tentieuchi,
      id_tieuchicha: this.infoTieuchi_Form.value.id_tieuchicha,
      id_loaidiem: this.id_loaidiem.value,
      is_tieuchigoc: this.infoTieuchi_Form.value.is_tieuchigoc,
      diemtoida: this.infoTieuchi_Form.value.diemtoida,
      can_minhchung: this.infoTieuchi_Form.value.can_minhchung,
      diemthuong: this.infoTieuchi_Form.value.diemthuong,
    };


    this.tcdanhgia_Service.postThemTieuChi(info).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
        this.iskhongdanhgia = 1;
        this.isdiemthuong = 0;
        this.resetForm();
        this.getDanhSachChiMucTieuChi();
        this.getDanhSachTieuChiThem();
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  openDialogDelete(id: string, tentieuchi: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: tentieuchi, message: 'Bạn thực sự muốn tiêu chí ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    this.tcdanhgia_Service.deleteXoaTieuChi(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa tiêu chí này', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA THÀNH CÔNG !!!');
          this.iskhongdanhgia = 1;
          this.isdiemthuong = 0;
          this.resetForm();
          this.getDanhSachChiMucTieuChi();
          this.getDanhSachTieuChiThem();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  selectedTieuChi(tc: any) {
    const dialogRef = this.dialog.open(SuatieuchidanhgiadialogComponent, {
      width: '500px',
      data: { tc: tc }
    });
  }
}
