import { Component, OnInit, Inject } from '@angular/core';
import * as _moment from 'moment';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ToastrService } from 'ngx-toastr';
import { FormControl, Validators } from '@angular/forms';
const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY/MM/DD'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-capnhatthoigianketthucdialog',
  templateUrl: './capnhatthoigianketthucdialog.component.html',
  // styleUrls: ['./capnhatthoigianketthucdialog.component.scss']
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ]
})
export class CapnhatthoigianketthucdialogComponent implements OnInit {

  tgketthuchieuluc = new FormControl('', Validators.required);
  constructor(
    private tcdanhgia_Service: TieuchidanhgiaService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<CapnhatthoigianketthucdialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  formatDate_Picker(value: any) {

    const days = moment(value).dates();
    const month = moment(value).months();
    const year = moment(value).years();

    const correctDate = year + '-' + (month + 1) + '-' + days;
    return correctDate;
  }

  capNhatTGkKetThuc() {
    this.tcdanhgia_Service.putCapNhatTGKetThucQuyetDinh(this.formatDate_Picker(this.tgketthuchieuluc.value), this.data.id).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
        this.dialogRef.close('Closed');
      }
    );
  }

  onCancel() {
    this.dialogRef.close();
  }
}
