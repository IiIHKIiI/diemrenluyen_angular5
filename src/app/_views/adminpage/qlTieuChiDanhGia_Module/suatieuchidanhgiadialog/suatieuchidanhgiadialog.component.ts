import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { MatDialogRef, MAT_DIALOG_DATA, MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-suatieuchidanhgiadialog',
  templateUrl: './suatieuchidanhgiadialog.component.html',
  // styleUrls: ['./suatieuchidanhgiadialog.component.scss']
  providers: [
    { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check' }
  ]
})
export class SuatieuchidanhgiadialogComponent implements OnInit {
  arrdiemtoida = [];

  infoTieuchi_Form: FormGroup;
  id_loaidiem = new FormControl('');
  DanhSachLoaiDiem = [];
  DanhSachChiMuc: any;

  selectedValue: any;
  tieuchicha: string;
  isdiemthuong: number;
  constructor(
    private tcdanhgia_Service: TieuchidanhgiaService,
    private toastr: ToastrService,
    public dialogRef: MatDialogRef<SuatieuchidanhgiadialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    if (this.data.tc.id_tieuchicha === null) {
      this.tieuchicha = 'TCG';
    } else {
      this.tieuchicha = this.data.tc.id_tieuchicha;
    }

    this.isdiemthuong = 1;
    this.mangDiemToiDa();
    this.setForm();
    this.getDanhSachLoaiDiem();
    this.getDanhSachChiMucTieuChi();

  }
  mangDiemToiDa() {
    for (let i = 1; i <= 25; i++) {
      this.arrdiemtoida.push(i);
    }
  }

  setForm() {
    this.infoTieuchi_Form = new FormGroup({
      id_tieuchicha: new FormControl(''),
      khongdanhgia: new FormControl('', Validators.required),
      chimuc_tieuchi: new FormControl('', Validators.required),
      tentieuchi: new FormControl('', Validators.required),
      is_tieuchigoc: new FormControl(''),
      diemtoida: new FormControl('', Validators.required),
      can_minhchung: new FormControl('', Validators.required),
      diemthuong: new FormControl(''),
    });
  }

  getDanhSachLoaiDiem() {
    this.tcdanhgia_Service.getDanhSachLoaiDiem().subscribe(
      responseData => {
        this.DanhSachLoaiDiem = responseData.danhsach;
      }
    );
  }

  getDanhSachChiMucTieuChi() {
    this.tcdanhgia_Service.getDanhSachChiMuc(this.data.tc.id_quyetdinhtieuchi).subscribe(
      responseData => {
        this.DanhSachChiMuc = responseData.danhsach;
      }
    );
  }

  getInfoTieuChi(id_tieuchi: string) {
    this.tcdanhgia_Service.getInfoTieuChi(id_tieuchi).subscribe(
      responseData => {
        this.selectedValue = responseData.thongtin;
      }
    );
  }
  changeTieuChiCha(tieuchicha: any) {
    if (tieuchicha === 'TCG') {
      this.isdiemthuong = 0;
    } else {
      this.isdiemthuong = 1;
    }
  }
  SuaTieuChi() {
    if (this.infoTieuchi_Form.value.id_tieuchicha === '') {
      this.infoTieuchi_Form.get('id_tieuchicha').setValue('');
      this.infoTieuchi_Form.get('is_tieuchigoc').setValue('1');
    } else {
      this.infoTieuchi_Form.get('is_tieuchigoc').setValue('0');
    }

    if (this.infoTieuchi_Form.value.diemthuong === false || this.infoTieuchi_Form.value.diemthuong === '') {
      this.infoTieuchi_Form.get('diemthuong').setValue('0');
    } else {
      this.infoTieuchi_Form.get('diemthuong').setValue('1');
    }

    const info = {
      id_quyetdinhtieuchi: this.data.tc.id_quyetdinhtieuchi,
      khongdanhgia: this.infoTieuchi_Form.value.khongdanhgia,
      chimuc_tieuchi: this.infoTieuchi_Form.value.chimuc_tieuchi,
      tentieuchi: this.infoTieuchi_Form.value.tentieuchi,
      id_tieuchicha: this.infoTieuchi_Form.value.id_tieuchicha,
      id_loaidiem: this.id_loaidiem.value,
      is_tieuchigoc: this.infoTieuchi_Form.value.is_tieuchigoc,
      diemtoida: this.infoTieuchi_Form.value.diemtoida,
      can_minhchung: this.infoTieuchi_Form.value.can_minhchung,
      diemthuong: this.infoTieuchi_Form.value.diemthuong,
    };

    this.tcdanhgia_Service.putSuaTieuChi(info, this.data.tc.id).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
        this.dialogRef.close('Closed');
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  onCancel() {
    this.dialogRef.close();
  }
}
