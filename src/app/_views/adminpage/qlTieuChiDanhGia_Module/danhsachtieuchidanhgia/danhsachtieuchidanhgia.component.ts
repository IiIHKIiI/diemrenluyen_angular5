import { Component, OnInit } from '@angular/core';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { CapnhatthoigianketthucdialogComponent } from '../capnhatthoigianketthucdialog/capnhatthoigianketthucdialog.component';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachtieuchidanhgia',
  templateUrl: './danhsachtieuchidanhgia.component.html',
  // styleUrls: ['./danhsachtieuchidanhgia.component.scss']
})
export class DanhsachtieuchidanhgiaComponent implements OnInit {
  p = 1; // Phân trang được load đầu tiên
  total = 0;

  DanhSachQuyetDinh = [];
  constructor(
    private tcdanhgia_Service: TieuchidanhgiaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getDanhSachQuyetDinh();
  }

  backBtn() {
    this.location.back();
  }

  getDanhSachQuyetDinh() {
    this.tcdanhgia_Service.getDanhSachQuyetDinh().subscribe(
      responseData => {
        this.DanhSachQuyetDinh = responseData.danhsach;
        this.total = this.DanhSachQuyetDinh.length;
      }
    );
  }

  openDialog(id: string, maquyetdinh: string) {
    const dialogRef = this.dialog.open(CapnhatthoigianketthucdialogComponent, {
      data: { id: id, maquyetdinh: maquyetdinh }
    });

    dialogRef.afterClosed().subscribe(selection => {
      this.getDanhSachQuyetDinh();
    });
  }

  openDialogDelete(id: string, maquyetdinh: string) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: maquyetdinh, message: 'Bạn thực sự muốn xóa quyết định và các tiêu chí liên quan đến quyết đinh ' }
    });

    dialogRef.afterClosed().subscribe(selection => {
      this.onDelete(id);
    });
  }

  onDelete(id: string) {
    this.tcdanhgia_Service.deleteXoaQuyetDinh(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa quyết định này vì quyết định này đã được sử dụng để đánh giá điểm rèn luyện, vui lòng kiểm tra rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA QUYẾT ĐỊNH THÀNH CÔNG !!!');
          this.getDanhSachQuyetDinh();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
