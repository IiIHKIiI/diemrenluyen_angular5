import { Component, OnInit } from '@angular/core';
import { NganhService } from '../../../../_services/nganh.service';
import { ToastrService } from 'ngx-toastr';
import { Nganh } from '../../../../_models/nganh';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachnganh',
  templateUrl: './danhsachnganh.component.html',
  // styleUrls: ['./danhsachnganh.component.scss']
})
export class DanhsachnganhComponent implements OnInit {
  DanhSachNganh = [];
  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;
  loaiLoc: number;
  id_khoa: number;
  DanhSachKhoa: any;
  dskhoa_control = new FormControl('', [Validators.required]);
  constructor(
    private nganh_Service: NganhService,
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.id_khoa = null;
    this.getDanhSachNganh();
    this.getDanhSachKhoa();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachNganh();
    } else if (this.loaiLoc === 2) {
      if (this.id_khoa !== null) {
        this.getDanhSachNganh_BoMon(this.id_khoa);
      }
    }
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachNganh() {
    return this.nganh_Service.getAllDanhSachNganh().subscribe(responseData => {
      this.DanhSachNganh = responseData.danhsach;
      this.total = this.DanhSachNganh.length;
    });
  }

  getDanhSachNganh_BoMon(id: number) {
    return this.nganh_Service.getDanhSachNganh(id).subscribe(responseData => {
      this.DanhSachNganh = responseData.danhsach;
      this.total = this.DanhSachNganh.length;
    });
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  /* Xóa bộ môn theo id*/
  onDelete(id: string) {
    return this.nganh_Service.deleteXoaNganh(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa ngành này vì ngành này đã có lớp trực thuộc, vui lòng xóa các lớp hoặc chuyển các lớp thuộc ngành này sang ngành khác rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA NGÀNH THÀNH CÔNG !!!');
          this.getDanhSachNganh();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
