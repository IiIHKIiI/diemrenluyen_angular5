import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NganhService } from '../../../../_services/nganh.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Nganh } from '../../../../_models/nganh';
import { Location } from '@angular/common';

@Component({
  selector: 'app-themnganh',
  templateUrl: './themnganh.component.html',
  // styleUrls: ['./themnganh.component.scss']
})
export class ThemnganhComponent implements OnInit {
  infoNganh_Form: FormGroup;
  DanhSachKhoa: any;
  DanhSachBoMon: any;
  nganh: Nganh;
  constructor(
    private nganh_Service: NganhService,
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.setForm();
    this.getDanhSachKhoa();
  }

  backBtn() {
    this.location.back();
  }

  setForm() {
    this.infoNganh_Form = new FormGroup({
      'manganh': new FormControl('', [Validators.required]),
      'tennganh': new FormControl('', [Validators.required]),
      'id_bomon_donvi': new FormControl('', [Validators.required])
    });
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  onSubmit() {
    this.nganh = this.infoNganh_Form.value;
    return this.nganh_Service.postThemNganh(this.nganh).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới ngành vì đã tồn tại ngành có cùng mã ngành được thêm.', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'THÊM NGÀNH MỚI THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlynganh');
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }

  onCancel() {
    this.location.back();
  }

}
