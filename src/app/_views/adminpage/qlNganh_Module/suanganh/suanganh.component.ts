import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { ToastrService } from 'ngx-toastr';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { Khoa } from '../../../../_models/khoa';
import { Nganh } from '../../../../_models/nganh';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';
@Component({
  selector: 'app-suanganh',
  templateUrl: './suanganh.component.html',
  // styleUrls: ['./suanganh.component.scss']
})
export class SuanganhComponent implements OnInit {

  id: string;
  infoNganh_Form: FormGroup;
  selectedValue: any;
  DanhSachKhoa: any;
  DanhSachBoMon: any;
  nganh: Nganh;
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private nganh_Service: NganhService,
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setDefaultData();
    this.setForm();
    this.getDanhSachKhoa();
    this.getInfoNganh();
    this.getDanhSachBoMon();
  }

  backBtn() {
    this.location.back();
  }

  /* Tạo giá trị mặc định trước khi api chưa load xong */
  setDefaultData(): void {
    this.selectedValue = {
      'manganh': '',
      'tennganh': '',
      'tructhuoc': '',
      'id_bomon_donvi': ''
    };
  }

  setForm() {
    this.infoNganh_Form = new FormGroup({
      'manganh': new FormControl('', [Validators.required]),
      'tennganh': new FormControl('', [Validators.required]),
      'id_bomon_donvi': new FormControl('', [Validators.required])
    });
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon() {
    return this.bm_Service.getDanhSachBoMon().subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  getDanhSachBoMon_tructhuoc(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  getInfoNganh() {
    return this.nganh_Service.getInfoNganh(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.nganh;
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: this.infoNganh_Form.value.tennganh, message: 'Bạn thực sự muốn chỉnh sửa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    this.nganh = this.infoNganh_Form.value;
    return this.nganh_Service.putSuaNganh(this.nganh, this.id).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể chỉnh sửa mã ngành vì đã tồn tại ngành với mã ngành được chỉnh sửa', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'CHỈNH SỬA THÔNG TIN NGÀNH THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlynganh');
      },
      err => {
        console.log(err);
        this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
      }
    );
  }

  onCancel() {
    this.location.back();
  }
}
