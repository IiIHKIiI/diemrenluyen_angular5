import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Khoa } from '../../../../_models/khoa';
import { Router } from '@angular/router';

@Component({
  selector: 'app-themkhoa',
  templateUrl: './themkhoa.component.html',
  // styleUrls: ['./themkhoa.component.scss']
})
export class ThemkhoaComponent implements OnInit {

  infoKhoa_Form: FormGroup;
  khoa: Khoa;
  constructor(
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.setForm();
  }

  backBtn() {
    this.location.back();
  }

  setForm() {
    this.infoKhoa_Form = new FormGroup({
      makhoa: new FormControl('', [Validators.required]),
      tenkhoa: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    this.khoa = this.infoKhoa_Form.value;
    return this.khoa_Service.postThemKhoa(this.khoa).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới khoa vì đã tồn tại khoa có cùng mã khoa được thêm.', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        } else {
          this.toastr.success(responseData.message, 'THÊM KHOA MỚI THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/quanly/quanlykhoa');
        }
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

  onCancel() {
    this.location.back();
  }
}
