import { Component, OnInit } from '@angular/core';
import { KhoaService } from '../../../../_services/khoa.service';
import { ToastrService } from 'ngx-toastr';
import { Khoa } from '../../../../_models/khoa';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachkhoa',
  templateUrl: './danhsachkhoa.component.html',
  // styleUrls: ['./danhsachkhoa.component.scss']
})
export class DanhsachkhoaComponent implements OnInit {

  DanhSachKhoa: Khoa[] = [];
  p = 1;
  total: number; // Tổng số record được load
  search: any;
  constructor(
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.getDanhSachKhoa();
  }

  backBtn() {
    this.location.back();
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
      this.total = this.DanhSachKhoa.length;
    });
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.khoa_Service.deleteXoaKhoa(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa khoa này vì đã có bộ môn trực thuộc khoa này, vui lòng xóa bộ môn hoặc chuyền bộ môn sang khoa khác để xóa khoa này.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA BỘ MÔN THÀNH CÔNG !!!');
          this.getDanhSachKhoa();
        }
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}


