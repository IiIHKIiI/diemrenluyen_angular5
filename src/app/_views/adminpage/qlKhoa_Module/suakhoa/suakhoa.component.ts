import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { Khoa } from '../../../../_models/khoa';
import { NgProgress } from 'ngx-progressbar';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { Location } from '@angular/common';

@Component({
  selector: 'app-suakhoa',
  templateUrl: './suakhoa.component.html',
  // styleUrls: ['./suakhoa.component.scss']
})
export class SuakhoaComponent implements OnInit {

  id: string;
  infoKhoa_Form: FormGroup;
  selectedValue: any;
  khoa: Khoa;
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private khoa_Service: KhoaService,
    private ngProgress: NgProgress,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setDefaultData();
    this.setForm();
    this.getInfoKhoa();
  }

  backBtn() {
    this.location.back();
  }

  setDefaultData(): void {
    this.selectedValue = {
      'makhoa': '',
      'tenkhoa': ''
    };
  }

  setForm() {
    this.infoKhoa_Form = new FormGroup({
      makhoa: new FormControl('', [Validators.required]),
      tenkhoa: new FormControl('', [Validators.required])
    });
  }

  getInfoKhoa() {
    this.khoa_Service.getInfoKhoa(this.id).subscribe(responseData => {
      this.selectedValue = responseData.khoa;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: this.infoKhoa_Form.value.tenkhoa, message: 'Bạn thực sự muốn chỉnh sửa thông tin của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    this.khoa = this.infoKhoa_Form.value;
    return this.khoa_Service.putSuaKhoa(this.id, this.khoa).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          this.toastr.error('Không thể chỉnh sửa mã khoa vì đã tồn tại khoa với mã khoa được chỉnh sửa', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'CHỈNH SỬA KHOA THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlykhoa');
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }

  onCancel() {
    this.location.back();
  }
}
