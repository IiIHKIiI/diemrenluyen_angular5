import { Component, OnInit } from '@angular/core';
import { StepperOptions } from 'ngx-stepper';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';

import * as _moment from 'moment';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { Location } from '@angular/common';
const moment = _moment;

@Component({
  selector: 'app-themthoigiandanhgia',
  templateUrl: './themthoigiandanhgia.component.html',
  // styleUrls: ['./themthoigiandanhgia.component.scss']
})
export class ThemthoigiandanhgiaComponent implements OnInit {
  public options: StepperOptions = {
    linear: false,
  };
  infoTGDanhGia_Form: FormGroup; // form group
  namhoc_control = new FormControl('', Validators.required);

  DanhSachNamHoc: any;
  DanhSachHocKy: any;
  DanhSachQD: any;

  chopheptre_default = 0; diemtru_default = 0;
  constructor(
    private tcdanhgia_Service: TieuchidanhgiaService,
    private dateTimeAdapter: DateTimeAdapter<any>,
    private tgdanhgia_Service: ThoigiandanhgiaService,
    private nhoc_hky_Service: NamhochockyService,
    private route: Router,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) {
    this.dateTimeAdapter.setLocale('vi-VN'); // change locale to Vietnamese
  }

  ngOnInit() {
    this.setForm();
    this.getDanhSachNamHoc();
    this.getDanhSachQD();
  }

  backBtn() {
    this.location.back();
  }


  setForm() {
    this.infoTGDanhGia_Form = new FormGroup({
      id_hocky: new FormControl('', Validators.required),
      id_tieuchidanhgia: new FormControl('', Validators.required),
      chopheptre: new FormControl('', Validators.required),
      diemtru: new FormControl('', Validators.required),
      tgbatdau_sv_tmp: new FormControl('', Validators.required),
      tgketthuc_sv_tmp: new FormControl('', Validators.required),
      tgbatdau_bcs_tmp: new FormControl('', Validators.required),
      tgketthuc_bcs_tmp: new FormControl('', Validators.required),
      tgbatdau_cvht_tmp: new FormControl('', Validators.required),
      tgketthuc_cvht_tmp: new FormControl('', Validators.required),
      tgbatdau_hoidongkhoa_tmp: new FormControl('', Validators.required),
      tgketthuc_hoidongkhoa_tmp: new FormControl('', Validators.required),
      tgbatdau_hoidongtruong_tmp: new FormControl('', Validators.required),
      tgketthuc_hoidongtruong_tmp: new FormControl('', Validators.required),
    });
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  getDanhSachNamHoc() {
    this.nhoc_hky_Service.getDanhSachNamHoc().subscribe(
      responseData => {
        this.DanhSachNamHoc = responseData.danhsach;
      }
    );
  }

  getDanhSachHocky_NamHoc(id_namhoc: string) {
    this.nhoc_hky_Service.getDanhSachHocKyByNamHoc(id_namhoc).subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }

  getDanhSachQD() {
    this.tcdanhgia_Service.getDanhSachQuyetDinh().subscribe(
      responseData => {
        this.DanhSachQD = responseData.danhsach;
      }
    );
  }

  formatDate_Picker(value: any) {

    const days = moment(value).dates();
    const month = moment(value).months();
    const year = moment(value).years();

    const hour = moment(value).hours();
    const minute = moment(value).minutes();

    const correctDate = year + '-' + (month + 1) + '-' + days + ' ' + hour + ':' + minute;
    return correctDate;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: 'Bạn thực sự muốn thêm thông tin thời gian đánh giá ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    const info = {
      id_hocky: this.infoTGDanhGia_Form.value.id_hocky,
      id_tieuchidanhgia: this.infoTGDanhGia_Form.value.id_tieuchidanhgia,
      chopheptre: this.infoTGDanhGia_Form.value.chopheptre,
      diemtru: this.infoTGDanhGia_Form.value.diemtru,
      tgbatdau_sv: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgbatdau_sv_tmp),
      tgketthuc_sv: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgketthuc_sv_tmp),
      tgbatdau_bancansu: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgbatdau_bcs_tmp),
      tgketthuc_bancansu: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgketthuc_bcs_tmp),
      tgbatdau_cvht: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgbatdau_cvht_tmp),
      tgketthuc_cvht: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgketthuc_cvht_tmp),
      tgbatdau_hoidongkhoa: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgbatdau_hoidongkhoa_tmp),
      tgketthuc_hoidongkhoa: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgketthuc_hoidongkhoa_tmp),
      tgbatdau_hoidongtruong: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgbatdau_hoidongtruong_tmp),
      tgketthuc_hoidongtruong: this.formatDate_Picker(this.infoTGDanhGia_Form.value.tgketthuc_hoidongtruong_tmp),

    };

    return this.tgdanhgia_Service.postThemThoiGianDanhGia(info).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          //  tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới thời gian đánh giá vì đã tồn tại học kỳ được thêm thời gian đánh giá', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        } else {
          this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/quanly/quanlythoigiandanhgia');
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error('LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }
}
