import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-lichsuthoigiandanhgia',
  templateUrl: './lichsuthoigiandanhgia.component.html',
  // styleUrls: ['./lichsuthoigiandanhgia.component.scss']
})
export class LichsuthoigiandanhgiaComponent implements OnInit {

  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;
  loaiLoc: number;
  id_hocy: string;
  DanhSachNamHoc: any;
  DanhSachHocKy: any;
  DanhSachThoiGianDanhGia = [];
  dsnamhoc_control = new FormControl('', [Validators.required]);
  dshk_control = new FormControl('', [Validators.required]);
  constructor(
    private nhoc_hky_Service: NamhochockyService,
    private tgdanhgia_Service: ThoigiandanhgiaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    // this.loaiLoc = 1;
    // this.id_hocy = null;
    // this.getDanhSachNamHoc();
    this.getDanhSachThoiGianDanhGia();
  }

  backBtn() {
    this.location.back();
  }


  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  getDanhSachThoiGianDanhGia() {
    this.tgdanhgia_Service.getDanhSachThoiGianDanhGia().subscribe(
      responseData => {
        this.DanhSachThoiGianDanhGia = responseData.danhsach;
        this.total = this.DanhSachThoiGianDanhGia.length;
      }
    );
  }

  openConfirmDialog(id: string, mahocky: string, manamhoc: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      // tslint:disable-next-line:max-line-length
      data: { name: mahocky + ' | ' + manamhoc, message: 'Bạn thực sự muốn xóa thông tin thời gian đánh giá của ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.tgdanhgia_Service.deleteXoaNamHocHocKy(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa thông tin này này vì thông tin này này đã có các dữ liệu đã liên kết đến, vui lòng xóa các dữ liệu liên quan rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA THỜI GIAN ĐÁNH GIÁ THÀNH CÔNG !!!');
          this.getDanhSachThoiGianDanhGia();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }
}
