import { Component, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { StepperOptions } from 'ngx-stepper';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { ThoigiandanhgiaService } from '../../../../_services/thoigiandanhgia.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';

import * as _moment from 'moment';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { TieuchidanhgiaService } from '../../../../_services/tieuchidanhgia.service';
import { Location } from '@angular/common';
const moment = _moment;

@Component({
  selector: 'app-suathoigiandanhgia',
  templateUrl: './suathoigiandanhgia.component.html',
  // styleUrls: ['./suathoigiandanhgia.component.scss']
})
export class SuathoigiandanhgiaComponent implements OnInit, AfterViewChecked {
  public options: StepperOptions = {
    linear: false,
  };
  infoTGDanhGia_Form: FormGroup; // form group
  namhoc_control = new FormControl('', Validators.required);

  id: string;
  DanhSachNamHoc: any;
  DanhSachHocKy: any;
  DanhSachQD: any;

  selectedValue: any;
  svbd = new FormControl(new Date());
  svkt = new FormControl(new Date());
  bcsbd = new FormControl(new Date());
  bcskt = new FormControl(new Date());
  cvhtbd = new FormControl(new Date());
  cvhtkt = new FormControl(new Date());
  hdkhoabd = new FormControl(new Date());
  hdkhoakt = new FormControl(new Date());
  hdtruongbd = new FormControl(new Date());
  hdtruongkt = new FormControl(new Date());
  constructor(
    private tcdanhgia_Service: TieuchidanhgiaService,
    private changeDetector: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private dateTimeAdapter: DateTimeAdapter<any>,
    private tgdanhgia_Service: ThoigiandanhgiaService,
    private nhoc_hky_Service: NamhochockyService,
    private route: Router,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) {
    this.dateTimeAdapter.setLocale('vi-VN'); // change locale to Vietnamese
  }


  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setForm();
    this.setDefaultData();
    this.getAllHocKy();
    this.getInfoThoiGianDanhGia();
    this.getDanhSachNamHoc();
    this.getDanhSachQD();
  }

  backBtn() {
    this.location.back();
  }


  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  setDefaultData() {
    this.selectedValue = {
      'id_hocky': '',
      'id_tieuchidanhgia': '',
      'chopheptre': '',
      'diemtru': '',
      'tgbatdau_sv': '',
      'tgketthuc_sv': '',
      'tgbatdau_bancansu': '',
      'tgketthuc_bancansu': '',
      'tgbatdau_cvht': '',
      'tgketthuc_cvht': '',
      'tgbatdau_hoidongkhoa': '',
      'tgketthuc_hoidongkhoa': '',
      'tgbatdau_hoidongtruong': '',
      'tgketthuc_hoidongtruong': ''
    };
  }

  setForm() {
    this.infoTGDanhGia_Form = new FormGroup({
      id_hocky: new FormControl('', Validators.required),
      id_tieuchidanhgia: new FormControl('', Validators.required),
      chopheptre: new FormControl('', Validators.required),
      diemtru: new FormControl('', Validators.required),
    });
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  getDanhSachNamHoc() {
    this.nhoc_hky_Service.getDanhSachNamHoc().subscribe(
      responseData => {
        this.DanhSachNamHoc = responseData.danhsach;
      }
    );
  }

  getAllHocKy() {
    this.nhoc_hky_Service.getDanhSachHocKy().subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }

  getDanhSachHocky_NamHoc(id_namhoc: string) {
    this.nhoc_hky_Service.getDanhSachHocKyByNamHoc(id_namhoc).subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }


  getDanhSachQD() {
    this.tcdanhgia_Service.getDanhSachQuyetDinh().subscribe(
      responseData => {
        this.DanhSachQD = responseData.danhsach;
      }
    );
  }


  getInfoThoiGianDanhGia() {
    this.tgdanhgia_Service.getInfoThoiGianDanhGiaById(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.thongtin;
        this.svbd = new FormControl(new Date(this.selectedValue.tgbatdau_sv));
        this.svkt = new FormControl(new Date(this.selectedValue.tgketthuc_sv));
        this.bcsbd = new FormControl(new Date(this.selectedValue.tgbatdau_bancansu));
        this.bcskt = new FormControl(new Date(this.selectedValue.tgketthuc_bancansu));
        this.cvhtbd = new FormControl(new Date(this.selectedValue.tgbatdau_cvht));
        this.cvhtkt = new FormControl(new Date(this.selectedValue.tgketthuc_cvht));
        this.hdkhoabd = new FormControl(new Date(this.selectedValue.tgbatdau_hoidongkhoa));
        this.hdkhoakt = new FormControl(new Date(this.selectedValue.tgketthuc_hoidongkhoa));
        this.hdtruongbd = new FormControl(new Date(this.selectedValue.tgbatdau_hoidongtruong));
        this.hdtruongkt = new FormControl(new Date(this.selectedValue.tgketthuc_hoidongtruong));
      }
    );
  }

  formatDate_Picker(value: any) {

    const days = moment(value).dates();
    const month = moment(value).months();
    const year = moment(value).years();

    const hour = moment(value).hours();
    const minute = moment(value).minutes();

    const correctDate = year + '-' + (month + 1) + '-' + days + ' ' + hour + ':' + minute;
    return correctDate;
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: 'Bạn thực sự muốn cập nhật thông tin thời gian đánh giá ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    const info = {
      id_hocky: this.infoTGDanhGia_Form.value.id_hocky,
      chopheptre: this.infoTGDanhGia_Form.value.chopheptre,
      diemtru: this.infoTGDanhGia_Form.value.diemtru,
      tgbatdau_sv: this.formatDate_Picker(this.svbd.value),
      tgketthuc_sv: this.formatDate_Picker(this.svkt.value),
      tgbatdau_bancansu: this.formatDate_Picker(this.bcsbd.value),
      tgketthuc_bancansu: this.formatDate_Picker(this.bcskt.value),
      tgbatdau_cvht: this.formatDate_Picker(this.cvhtbd.value),
      tgketthuc_cvht: this.formatDate_Picker(this.cvhtkt.value),
      tgbatdau_hoidongkhoa: this.formatDate_Picker(this.hdkhoabd.value),
      tgketthuc_hoidongkhoa: this.formatDate_Picker(this.hdkhoakt.value),
      tgbatdau_hoidongtruong: this.formatDate_Picker(this.hdtruongbd.value),
      tgketthuc_hoidongtruong: this.formatDate_Picker(this.hdtruongkt.value),
    };

    return this.tgdanhgia_Service.putSuaNamHocHocKy(info, this.id).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          //  tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể cập nhật thời gian đánh giá vì đã tồn tại học kỳ được thêm thời gian đánh giá', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        } else {
          this.toastr.success(responseData.message, 'THÀNH CÔNG !!!');
          this.route.navigateByUrl('/trangchu/quanly/quanlythoigiandanhgia');
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error('LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }
}
