import { Component, OnInit } from '@angular/core';
import { LopService } from '../../../../_services/lop.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { Khoa } from '../../../../_models/khoa';
import { Nganh } from '../../../../_models/nganh';
import { Lop } from '../../../../_models/lop';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-themlop',
  templateUrl: './themlop.component.html',
  // styleUrls: ['./themlop.component.scss']
})
export class ThemlopComponent implements OnInit {
  infoLop_Form: FormGroup;
  DanhSachKhoa: Khoa[] = [];
  DanhSachNganh: Nganh[] = [];
  lop: Lop;
  tiento_lop: number;
  tenlop: string;
  constructor(
    private lop_Service: LopService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location
  ) { }

  ngOnInit() {
    this.setForm();
    this.getDanhSachKhoa();
    this.tiento_lop = 1;

  }

  backBtn() {
    this.location.back();
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  setForm() {
    this.infoLop_Form = new FormGroup({
      'malop': new FormControl('', [Validators.required]),
      'tenlop': new FormControl(''),
      'nambatdau': new FormControl('', [Validators.required]),
      'namketthuc': new FormControl('', [Validators.required]),
      'id_nganh': new FormControl('', [Validators.required]),
      'hedaotao': new FormControl('', [Validators.required])
    });
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachNganh(id_khoa: number) {
    return this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(responseData => {
      this.DanhSachNganh = responseData.danhsach;
    });
  }

  onSubmit() {
    if (this.infoLop_Form.value.hedaotao === 1) {
      this.infoLop_Form.get('tenlop').setValue('DH' + this.infoLop_Form.value.malop);
    } else {
      this.infoLop_Form.get('tenlop').setValue('CD' + this.infoLop_Form.value.malop);
    }

    this.lop = this.infoLop_Form.value;
    return this.lop_Service.postThemLop(this.lop).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể thêm mới lớp học vì đã tồn tại lớp học có cùng tên lớp được thêm.', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'THÊM LỚP HỌC THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlylop');
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }

  onCancel() {
    this.location.back();
  }

}
