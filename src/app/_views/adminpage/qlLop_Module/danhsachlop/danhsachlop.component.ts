import { Component, OnInit } from '@angular/core';
import { LopService } from '../../../../_services/lop.service';
import { ToastrService } from 'ngx-toastr';
import { Lop } from '../../../../_models/lop';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachlop',
  templateUrl: './danhsachlop.component.html',
  // styleUrls: ['./danhsachlop.component.scss']
})
export class DanhsachlopComponent implements OnInit {

  DanhSachLop: any;
  DanhSachNganh: any;
  DanhSachKhoa: any;

  dskhoa_control = new FormControl('', [Validators.required]);
  dsnganh_control = new FormControl('', [Validators.required]);

  loaiLoc: number;
  id_nganh: number;
  id_khoa: number;

  search: string;
  total: number;
  p = 1; // Phân trang được load đầu tiên
  constructor(
    private lop_Service: LopService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.id_khoa = null;
    this.id_nganh = null;
    this.DanhSachLop = [];
    this.getDanhSachKhoa();
    this.getDanhSachLop();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachLop();
    } else if (this.loaiLoc === 2) {
      if (this.id_khoa !== null && this.id_nganh !== null) {
        this.getDanhSachLop_Nganh(this.id_nganh);
      }
    }
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  /* Lấy danh sách các ngành thuộc khoa đươc chọn */
  getDanhSachNganh(id_khoa: number): void {
    this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(
      responseData => {
        this.DanhSachNganh = responseData.danhsach;
      }
    );
  }

  getDanhSachLop() {
    return this.lop_Service.getDanhSachLop().subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
        this.total = this.DanhSachLop.length;
      }
    );
  }

  getDanhSachLop_Nganh(id: number) {
    return this.lop_Service.getDanhSachLop_Nganh(id).subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
        this.total = this.DanhSachLop.length;
      }
    );
  }

  openDialog(id: string, name: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: name, message: 'Bạn thực sự muốn xóa thông tin của lớp ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id);
      }
    });
  }

  onDelete(id: string) {
    return this.lop_Service.deleteXoaLop(id).subscribe(
      responseData => {
        if (responseData.maloi) {
          if (responseData.maloi === 1451) {
            // tslint:disable-next-line:max-line-length
            this.toastr.error('Không thể xóa lớp này vì đã được sử dụng ở nghiệp vụ khác hoặc lớp này đang chứa dữ liệu, vui lòng xóa các dữ liệu liên quan đến lớp này rồi thử lại.', 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
          }
        } else {
          this.toastr.success(responseData.message, 'XÓA LỚP HỌC THÀNH CÔNG !!!');
          this.getDanhSachLop();
        }
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

}
