import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LopService } from '../../../../_services/lop.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { NganhService } from '../../../../_services/nganh.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Lop } from '../../../../_models/lop';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-sualop',
  templateUrl: './sualop.component.html',
  // styleUrls: ['./sualop.component.scss']
})
export class SualopComponent implements OnInit {
  infoLop_Form: FormGroup;
  DanhSachKhoa: any;
  DanhSachNganh: any;
  id: string;
  selectedValue: any;
  lop: Lop;
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private lop_Service: LopService,
    private khoa_Service: KhoaService,
    private nganh_Service: NganhService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setDefaultData();
    this.setForm();
    this.getDanhSachKhoa();
    this.getDanhSachNganh();
    this.getInfoLop();
  }

  backBtn() {
    this.location.back();
  }

  /* Thiết lập các giá trị mặc định */
  setDefaultData(): void {
    this.selectedValue = {
      'tenlop': '',
      'nambatdau': '',
      'namketthuc': '',
      'tructhuoc': '',
      'id_nganh': '',
      'hedaotao': ''
    };
  }

  /* Ngăn người dùng nhập vào ký tự đối với các control chỉ cho nhập số */
  keyPress(event: any) {
    if (event.charCode !== 0) {
      const pattern = /[0-9\+\-\ ]/;
      const inputChar = String.fromCharCode(event.charCode);

      if (!pattern.test(inputChar)) {
        // invalid character, prevent input
        event.preventDefault();
      }
    }
  }

  setForm() {
    this.infoLop_Form = new FormGroup({
      'tenlop': new FormControl('', [Validators.required]),
      'nambatdau': new FormControl('', [Validators.required]),
      'namketthuc': new FormControl('', [Validators.required]),
      'id_nganh': new FormControl('', [Validators.required]),
      'hedaotao': new FormControl('', [Validators.required])
    });
  }

  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachNganh() {
    return this.nganh_Service.getAllDanhSachNganh().subscribe(responseData => {
      this.DanhSachNganh = responseData.danhsach;
    });
  }

  getDanhSachNganh_Khoa(id_khoa: number) {
    return this.nganh_Service.getDanhSachNganh(id_khoa).subscribe(responseData => {
      this.DanhSachNganh = responseData.danhsach;
    });
  }

  getInfoLop() {
    return this.lop_Service.getInfoLop(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.lop;
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: this.infoLop_Form.value.tenlop, message: 'Bạn thực sự muốn chỉnh sửa thông tin của lớp ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    this.lop = this.infoLop_Form.value;
    return this.lop_Service.putSuaLop(this.lop, this.id).subscribe(
      responseData => {
        if (responseData.maloi === 1062) {
          // tslint:disable-next-line:max-line-length
          this.toastr.error('Không thể chỉnh sửa tên lớp vì đã tồn tại lớp học với tên lớp được chỉnh sửa', 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
        this.toastr.success(responseData.message, 'CHỈNH SỬA LỚP HỌC THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlylop');
      },
      err => {
        if (err) {
          this.toastr.error(err.message, 'LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }

  onCancel() {
    this.location.back();
  }

}
