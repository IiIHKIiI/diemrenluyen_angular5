import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { DanhSachSinhVienComponent } from './qlSinhVien_Module/danh-sach-sinh-vien/danh-sach-sinh-vien.component';
import { ThemsinhvienComponent } from './qlSinhVien_Module/themsinhvien/themsinhvien.component';
import { ReactiveFormsModule, FormsModule, FormControl } from '@angular/forms';
import { DanhsachkhoaComponent } from './qlKhoa_Module/danhsachkhoa/danhsachkhoa.component';
import { ThemkhoaComponent } from './qlKhoa_Module/themkhoa/themkhoa.component';
import { SuakhoaComponent } from './qlKhoa_Module/suakhoa/suakhoa.component';
import { ThemnganhComponent } from './qlNganh_Module/themnganh/themnganh.component';
import { ThembomondonviComponent } from './qlBoMonDonVi_Module/thembomondonvi/thembomondonvi.component';
import { SuabomondonviComponent } from './qlBoMonDonVi_Module/suabomondonvi/suabomondonvi.component';
import { SuanganhComponent } from './qlNganh_Module/suanganh/suanganh.component';
import { DanhsachnganhComponent } from './qlNganh_Module/danhsachnganh/danhsachnganh.component';
import { DanhsachbomondonviComponent } from './qlBoMonDonVi_Module/danhsachbomondonvi/danhsachbomondonvi.component';
import { NgProgressModule } from 'ngx-progressbar';
import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';
import { DanhsachlopComponent } from './qlLop_Module/danhsachlop/danhsachlop.component';
import { ThemlopComponent } from './qlLop_Module/themlop/themlop.component';
import { SualopComponent } from './qlLop_Module/sualop/sualop.component';
import { SuasinhvienComponent } from './qlSinhVien_Module/suasinhvien/suasinhvien.component';
import { AdminRouters } from './admin.router';
import { DanhsachtinhhinhhoctapComponent } from './qlTHHocTap_Module/danhsachtinhhinhhoctap/danhsachtinhhinhhoctap.component';
import { SuathhoctapComponent } from './qlTHHocTap_Module/suathhoctap/suathhoctap.component';
import { DanhsachtinhhinhkyluatComponent } from './qlTHKyLuat_Module/danhsachtinhhinhkyluat/danhsachtinhhinhkyluat.component';
import { ThemkyluatComponent } from './qlTHKyLuat_Module/themkyluat/themkyluat.component';
import { SuakyluatComponent } from './qlTHKyLuat_Module/suakyluat/suakyluat.component';
import { ThongtinkyluatComponent } from './qlTHKyLuat_Module/thongtinkyluat/thongtinkyluat.component';
import { DanhsachtkcanboComponent } from './qlTaiKhoan_Module/tkcanbo_Module/danhsachtkcanbo/danhsachtkcanbo.component';
import { DanhsachtksinhvienComponent } from './qlTaiKhoan_Module/tkSinhVien_Module/danhsachtksinhvien/danhsachtksinhvien.component';
import { DanhsachcanboComponent } from './qlCanBo_Module/danhsachcanbo/danhsachcanbo.component';
import { ThemcanboComponent } from './qlCanBo_Module/themcanbo/themcanbo.component';
import { SuacanboComponent } from './qlCanBo_Module/suacanbo/suacanbo.component';
import { NgxStepperModule } from 'ngx-stepper';
import { DanhsachnamhochockyComponent } from './qlNamHocHocKy_Module/danhsachnamhochocky/danhsachnamhochocky.component';
import { DanhsachcvhtComponent } from './qlCoVanHocTap_Module/danhsachcvht/danhsachcvht.component';
import { ThemnamhocComponent } from './qlNamHocHocKy_Module/themnamhoc/themnamhoc.component';
import { SuanamhocComponent } from './qlNamHocHocKy_Module/suanamhoc/suanamhoc.component';
import { ThemcovanhoctapComponent } from './qlCoVanHocTap_Module/themcovanhoctap/themcovanhoctap.component';
import { SuacovanhoctapComponent } from './qlCoVanHocTap_Module/suacovanhoctap/suacovanhoctap.component';
import { UploadModule } from '@progress/kendo-angular-upload';
import { LichsuthoigiandanhgiaComponent } from './qlThoiGianDanhGia_Module/lichsuthoigiandanhgia/lichsuthoigiandanhgia.component';
import { ThemthoigiandanhgiaComponent } from './qlThoiGianDanhGia_Module/themthoigiandanhgia/themthoigiandanhgia.component';
import { SuathoigiandanhgiaComponent } from './qlThoiGianDanhGia_Module/suathoigiandanhgia/suathoigiandanhgia.component';
// Import Date tiem picker
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
import { ManamhocMahockyPipe } from '../../_pipes/manamhoc-mahocky-filter.pipe';
import { SharedComponentModule } from '../SharedComponent.module';
import { ThemtieuchidanhgiaComponent } from './qlTieuChiDanhGia_Module/themtieuchidanhgia/themtieuchidanhgia.component';
import { DanhsachtieuchidanhgiaComponent } from './qlTieuChiDanhGia_Module/danhsachtieuchidanhgia/danhsachtieuchidanhgia.component';
import { SuaquyetdinhComponent } from './qlTieuChiDanhGia_Module/suaquyetdinh/suaquyetdinh.component';
import { DanhsachthongbaoComponent } from './qlThongBao_Module/danhsachthongbao/danhsachthongbao.component';
import { ThemthongbaoComponent } from './qlThongBao_Module/themthongbao/themthongbao.component';
import { SuathongbaoComponent } from './qlThongBao_Module/suathongbao/suathongbao.component';
import { CKEditorModule } from 'ng2-ckeditor';
import { TrangchinhquanlyComponent } from './trangchinhquanly/trangchinhquanly.component';
import { FileUploadModule } from 'primeng/fileupload';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    SharedComponentModule,
    ReactiveFormsModule,
    NgProgressModule,
    NgxPaginationModule,
    NgxStepperModule, // Stepper
    AdminRouters,
    UploadModule,
    OwlMomentDateTimeModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CKEditorModule,
    FileUploadModule
  ],
  declarations: [
    DanhSachSinhVienComponent,
    // XemchitietsinhvienDialogComponent,
    ThemsinhvienComponent,
    SuasinhvienComponent,
    DanhsachkhoaComponent,
    ThemkhoaComponent,
    SuakhoaComponent,
    DanhsachnganhComponent,
    ThemnganhComponent,
    SuanganhComponent,
    DanhsachbomondonviComponent,
    ThembomondonviComponent,
    SuabomondonviComponent,
    DanhsachlopComponent,
    ThemlopComponent,
    SualopComponent,
    DanhsachtinhhinhhoctapComponent,
    SuathhoctapComponent,
    DanhsachtinhhinhkyluatComponent,
    ThemkyluatComponent,
    SuakyluatComponent,
    ThongtinkyluatComponent,
    DanhsachtkcanboComponent,
    DanhsachtksinhvienComponent,
    // ThemphanquyenDialogComponent,
    DanhsachcanboComponent,
    ThemcanboComponent,
    SuacanboComponent,
    DanhsachnamhochockyComponent,
    ThemnamhocComponent,
    SuanamhocComponent,
    DanhsachcvhtComponent,
    ThemcovanhoctapComponent,
    SuacovanhoctapComponent,
    LichsuthoigiandanhgiaComponent,
    ThemthoigiandanhgiaComponent,
    SuathoigiandanhgiaComponent,
    DanhsachtieuchidanhgiaComponent,
    ThemtieuchidanhgiaComponent,
    SuaquyetdinhComponent,
    DanhsachthongbaoComponent,
    ThemthongbaoComponent,
    SuathongbaoComponent,
    TrangchinhquanlyComponent,
  ],
  exports: [
    DanhSachSinhVienComponent,
    // XemchitietsinhvienDialogComponent,
    ThemsinhvienComponent,
    SuasinhvienComponent,
    DanhsachkhoaComponent,
    ThemkhoaComponent,
    SuakhoaComponent,
    DanhsachnganhComponent,
    ThemnganhComponent,
    SuanganhComponent,
    DanhsachbomondonviComponent,
    ThembomondonviComponent,
    SuabomondonviComponent,
    DanhsachlopComponent,
    ThemlopComponent,
    SualopComponent,
    DanhsachtinhhinhhoctapComponent,
    SuathhoctapComponent,
    DanhsachtinhhinhkyluatComponent,
    ThemkyluatComponent,
    SuakyluatComponent,
    ThongtinkyluatComponent,
    DanhsachtkcanboComponent,
    DanhsachtksinhvienComponent,
    // ThemphanquyenDialogComponent,
    DanhsachcanboComponent,
    ThemcanboComponent,
    SuacanboComponent,
    DanhsachnamhochockyComponent,
    ThemnamhocComponent,
    SuanamhocComponent,
    DanhsachcvhtComponent,
    ThemcovanhoctapComponent,
    SuacovanhoctapComponent,
    LichsuthoigiandanhgiaComponent,
    ThemthoigiandanhgiaComponent,
    SuathoigiandanhgiaComponent,
    DanhsachtieuchidanhgiaComponent,
    ThemtieuchidanhgiaComponent,
    SuaquyetdinhComponent,
    DanhsachthongbaoComponent,
    ThemthongbaoComponent,
    SuathongbaoComponent,
    TrangchinhquanlyComponent,
  ]
})
export class AdminPageComponentModule { }
