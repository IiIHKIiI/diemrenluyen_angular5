import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { CanboService } from '../../../../_services/canbo.service';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { ToastrService } from 'ngx-toastr';
import { KhoaService } from '../../../../_services/khoa.service';
import { MatDialog } from '@angular/material';
import { CovanhoctapService } from '../../../../_services/covanhoctap.service';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-danhsachcvht',
  templateUrl: './danhsachcvht.component.html',
  // styleUrls: ['./danhsachcvht.component.scss']
})
export class DanhsachcvhtComponent implements OnInit {
  DanhSachBoMon: any;
  p = 1; // Phân trang được load đầu tiên
  total: number;
  search: string;
  loaiLoc: number;
  DanhSachKhoa: any;
  DanhSachCoVanHocTap = [];
  dskhoa_control = new FormControl('', [Validators.required]);
  dsbomon_control = new FormControl('', [Validators.required]);
  constructor(
    private cvht_Service: CovanhoctapService,
    private cb_Service: CanboService,
    private bm_Service: BomondonviService,
    private khoa_Service: KhoaService,
    private toastr: ToastrService,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.loaiLoc = 1;
    this.getDanhSachKhoa();
    this.locDanhSach();
  }

  backBtn() {
    this.location.back();
  }

  reset() {
    this.dskhoa_control = new FormControl('', Validators.required);
    this.dsbomon_control = new FormControl('', Validators.required);
  }


  locDanhSach() {
    if (this.loaiLoc === 1) {
      this.getDanhSachCoVan('%');
    } else if (this.loaiLoc === 2) {
      if (this.dsbomon_control.value !== null) {
        this.getDanhSachCoVan(this.dsbomon_control.value);
      }
    }
  }

  applyFilter(value: string) {
    value = value.trim();
    this.search = value;
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  getDanhSachCoVan(id_bomon: string) {
    return this.cvht_Service.postDanhSachCoVanHocTap(id_bomon).subscribe(
      responseData => {
        this.DanhSachCoVanHocTap = responseData.danhsach;
        this.total = this.DanhSachCoVanHocTap.length;
      }
    );
  }

  openDialog(id: string, name: string, id_lop: string, tenlop: string): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        name: name, message: 'Bạn thực sự muốn xóa thông tin cố vấn học tập của lớp ' + tenlop + ' do cán bộ quản lý '
      }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành xóa
        this.onDelete(id, id_lop);
      }
    });
  }

  onDelete(id: string, id_lop: string) {
    return this.cvht_Service.deleteCoVanHocTap(id, id_lop).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'XÓA THÔNG TIN CỐ VẤN HỌC TẬP THÀNH CÔNG !!!');
        this.locDanhSach();
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error(err.message, 'XÓA THẤT BẠI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      }
    );
  }

}
