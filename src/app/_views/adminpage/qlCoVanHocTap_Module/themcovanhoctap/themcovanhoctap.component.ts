import { Component, OnInit } from '@angular/core';
import { StepperOptions } from 'ngx-stepper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { KhoaService } from '../../../../_services/khoa.service';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { CanboService } from '../../../../_services/canbo.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { CovanhoctapService } from '../../../../_services/covanhoctap.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-themcovanhoctap',
  templateUrl: './themcovanhoctap.component.html',
  // styleUrls: ['./themcovanhoctap.component.scss']
})
export class ThemcovanhoctapComponent implements OnInit {
  public options: StepperOptions = {
    linear: true,
  };
  infoCVHT_form: FormGroup; // form group

  dskhoa_control = new FormControl('', Validators.required);
  dsbomon_control = new FormControl('', Validators.required);

  DanhSachKhoa: any;
  DanhSachBoMon: any;
  DanhSachCanBoChuaCoVan: any;
  DanhSachLopChuaCoVan: any;
  DanhSachHocKy: any;
  constructor(
    private cvht_Service: CovanhoctapService,
    private nhoc_hky_Service: NamhochockyService,
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private cb_Service: CanboService,
    private toastr: ToastrService,
    private route: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    this.setForm();
    this.getDanhSachKhoa();
    this.getDanhSachHocKy();
  }

  backBtn() {
    this.location.back();
  }

  setForm() {
    this.infoCVHT_form = new FormGroup({
      id_canbo: new FormControl('', Validators.required),
      id_lop: new FormControl('', Validators.required),
      id_hockybatdau: new FormControl('', Validators.required),
      id_hockyketthuc: new FormControl('', Validators.required),
    });
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các cán bộ chưa cố vân học tập */
  getDanhSachCanBoChuaCoVan(id_bomon_donvi: string) {
    return this.cb_Service.getDanhSachCanBo_ChuaCoVan(id_bomon_donvi).subscribe(
      responseData => {
        this.DanhSachCanBoChuaCoVan = responseData.danhsachcb;
        this.DanhSachLopChuaCoVan = responseData.danhsachlop;
      }
    );
  }

  getDanhSachHocKy() {
    this.nhoc_hky_Service.getDanhSachHocKy().subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }

  onSubmit() {
    // console.log(this.infoCVHT_form.value);
    this.cvht_Service.postThemCoVanHocTap(this.infoCVHT_form.value).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'PHÂN CÔNG CỐ VẤN HỌC TẬP THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlycovanhoctap');
      }
    );
  }
}
