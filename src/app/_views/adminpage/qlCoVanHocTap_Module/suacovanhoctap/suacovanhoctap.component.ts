import { Component, OnInit } from '@angular/core';
import { StepperOptions } from 'ngx-stepper';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CovanhoctapService } from '../../../../_services/covanhoctap.service';
import { NamhochockyService } from '../../../../_services/namhochocky.service';
import { KhoaService } from '../../../../_services/khoa.service';
import { BomondonviService } from '../../../../_services/bomondonvi.service';
import { CanboService } from '../../../../_services/canbo.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LopService } from '../../../../_services/lop.service';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from '../../../_layouts/confirm-dialog/confirm-dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-suacovanhoctap',
  templateUrl: './suacovanhoctap.component.html',
  // styleUrls: ['./suacovanhoctap.component.scss']
})
export class SuacovanhoctapComponent implements OnInit {
  public options: StepperOptions = {
    linear: false,
  };
  infoCVHT_form: FormGroup; // form group

  dskhoa_control = new FormControl('', Validators.required);
  dsbomon_control = new FormControl('', Validators.required);

  DanhSachKhoa: any;
  DanhSachBoMon: any;
  DanhSachCanBo: any;
  DanhSachLop: any;
  DanhSachHocKy: any;

  selectedValue: any;

  id: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private lop_Service: LopService,
    private cvht_Service: CovanhoctapService,
    private nhoc_hky_Service: NamhochockyService,
    private khoa_Service: KhoaService,
    private bm_Service: BomondonviService,
    private cb_Service: CanboService,
    private toastr: ToastrService,
    private route: Router,
    private dialog: MatDialog,
    private location: Location,
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.setForm();
    this.getDanhSachKhoa();
    this.setDefaultData();
    this.getDanhSachBoMon();
    this.getDanhSachCanBo();
    this.getDanhSachLop();
    this.getDanhSachHocKy();
    this.getInfoCVHT();
  }

  backBtn() {
    this.location.back();
  }

  setDefaultData() {
    this.selectedValue = {
      'tructhuoc': '',
      'id_bomon_donvi': '',
      'id_hockybatdau': '',
      'id_hockyketthuc': '',
      'id_lop': '',
      'id_canbo': ''
    };
  }

  setForm() {
    this.infoCVHT_form = new FormGroup({
      id_canbo: new FormControl('', Validators.required),
      id_lop: new FormControl('', Validators.required),
      id_hockybatdau: new FormControl('', Validators.required),
      id_hockyketthuc: new FormControl('', Validators.required),
    });
  }

  /* Lấy tất cả danh sách khoa */
  getDanhSachKhoa() {
    return this.khoa_Service.getDanhSachKhoa().subscribe(responseData => {
      this.DanhSachKhoa = responseData.danhsach;
    });
  }

  getDanhSachBoMon() {
    return this.bm_Service.getDanhSachBoMon().subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  getDanhSachBoMon_khoa(id: number) {
    return this.bm_Service.getDanhSachBoMonByTrucThuoc(id).subscribe(
      responseData => {
        this.DanhSachBoMon = responseData.danhsach;
      }
    );
  }

  /* Lấy danh sách các cán bộ chưa cố vân học tập */
  getDanhSachCanBoChuaCoVan_BoMon(id_bomon_donvi: string) {
    return this.cb_Service.getDanhSachCanBo_ChuaCoVan(id_bomon_donvi).subscribe(
      responseData => {
        this.DanhSachCanBo = responseData.danhsachcb;
        this.DanhSachLop = responseData.danhsachlop;
      }
    );
  }
  /* Lấy tất cả danh sách lớp */
  getDanhSachLop() {
    return this.lop_Service.getDanhSachLop().subscribe(
      responseData => {
        this.DanhSachLop = responseData.danhsach;
      }
    );
  }

  getDanhSachHocKy() {
    this.nhoc_hky_Service.getDanhSachHocKy().subscribe(
      responseData => {
        this.DanhSachHocKy = responseData.danhsach;
      }
    );
  }

  getDanhSachCanBo() {
    return this.cb_Service.getDanhSachCanBo_BoMon('%').subscribe(
      responseData => {
        this.DanhSachCanBo = responseData.danhsach;
      }
    );
  }

  getInfoCVHT() {
    this.cvht_Service.getInfoCoVanHocTap(this.id).subscribe(
      responseData => {
        this.selectedValue = responseData.cvht;
      }
    );

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { name: null, message: 'Bạn thực sự muốn chỉnh sửa thông tin cố vấn học tập ', }
    });

    dialogRef.afterClosed().subscribe(selection => {
      if (selection) { // Nếu chọn xác nhận thì tiến hành chỉnh sửa
        this.onSubmit();
      }
    });
  }

  onSubmit() {
    return this.cvht_Service.putSuaCoVanHocTap(this.infoCVHT_form.value, this.id).subscribe(
      responseData => {
        this.toastr.success(responseData.message, 'CHỈNH SỬA THÔNG TIN CỐ VẤN HỌC TẬP THÀNH CÔNG !!!');
        this.route.navigateByUrl('/trangchu/quanly/quanlycovanhoctap');
      },
      err => {
        if (err) {
          console.log(err);
          this.toastr.error('LỖI. VUI LÒNG KIỂM TRA LẠI !!!');
        }
      });
  }
}
