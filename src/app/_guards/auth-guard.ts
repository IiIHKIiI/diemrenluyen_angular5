import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs//Observable';
import { UserService } from '../_services/user.service';
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: UserService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('token')) {
      return true;

    } else {

      // not logged in so redirect to login page with the return url
      this.router.navigateByUrl('/dangnhap');
      return false;
    }
  }

}
