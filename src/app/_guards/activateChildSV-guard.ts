import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanLoad } from '@angular/router';
import { Observable } from 'rxjs//Observable';
import { UserService } from '../_services/user.service';
import { ToastrService } from 'ngx-toastr';
import { CanActivateChild } from '@angular/router/src/interfaces';


@Injectable()
export class ActivateChildSVGuard implements CanActivate, CanActivateChild {
    check: boolean;
    constructor(
        private authService: UserService,
        private router: Router,
        private toastr: ToastrService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.checkUserRights().do(response => {
            if (response === false) {
                // tslint:disable-next-line:max-line-length
                this.toastr.warning('Bạn đang cố gắng truy cập vào đường dẫn không cho phép. Vui lòng không thực hiện các thao tác tương tự...', 'CẢNH BÁO');
                this.router.navigateByUrl('/dangnhap');
            }
        });
    }
    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.canActivate(route, state);
    }

    checkUserRights(): Observable<boolean> {
        return this.authService.getInfoUser().map(
            responseData => {
                if (responseData.user.loaiuser === 1) {
                    return true;
                } else {
                    return false;
                }
            }
        );
    }
}

