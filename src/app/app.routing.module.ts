import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { AuthGuard } from './_guards/auth-guard';
import { ActivateChildAdminGuard } from './_guards/activateChildAdmin-guard';
import { ActivateChildSVGuard } from './_guards/activateChildSV-guard';
import { ActivateChildCanBoGuard } from './_guards/activateChildCanBo-guard';
import { LoginComponent } from './_views/login/login.component';


const routes: Routes = [
    {
        path: '', pathMatch: 'full', redirectTo: 'dangnhap'
    },
    {
        path: 'dangnhap',
        component: LoginComponent
    },
    {
        path: 'trangchu',
        canActivate: [AuthGuard],
        children: [
            {
                path: 'quanly',
                loadChildren: 'app/_views/adminpage/admin-component.module#AdminPageComponentModule',
                canActivate: [ActivateChildAdminGuard]
            },
            {
                path: 'sinhvien',
                loadChildren: 'app/_views/mainpage/SVPage/SVPage-component.module#SVPageComponentModule',
                canActivate: [ActivateChildSVGuard]
            },
            {
                path: 'canbo',
                loadChildren: 'app/_views/mainpage/CanBoPage/CanBoPage-component.module#CanBoPageComponentModule',
                canActivate: [ActivateChildCanBoGuard]
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
