export class Sinhvien {
    mssv: string;
    hoten: string;
    ngaysinh: string;
    cmnd: number;
    sdt_canhan: number;
    sdt_giadinh: number;
    diachi: string;
    email: string;
    id_lop: number;
}
