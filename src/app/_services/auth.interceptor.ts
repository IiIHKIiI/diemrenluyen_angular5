import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UserService } from './user.service';
import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, private toastr: ToastrService) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem('token');
    if (!token) {
      return next.handle(req);
    } else {
      req = req.clone({
        headers: req.headers
          .set('Authorization', 'Bearer ' + token)
          .set('X-Request-With', 'XMLHttpRequest')
          .set('Accept', '*/*')
      });

      /* Nếu trên request chưa có Content-Type thì thêm vào (mặc định là application/json) */
      if (!req.headers.has('Content-Type')) {
        req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
      }

      /* Nếu đã có enctype (gửi bằng form data) thì bỏ Content-Type: application/json*/
      if (req.headers.has('enctype')) {
        req = req.clone({ headers: req.headers.delete('Content-Type') });
      }

      return next.handle(req).do((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do stuff with response if you want

        }
      }, (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) { // Lỗi không xác thực được người dùng
            // hiển thị thông báo
            this.toastr.warning('Lỗi xác thực người dùng. Vui lòng đăng nhập lại...', 'CẢNH BÁO !!!', {
              timeOut: 5000
            });
            this.router.navigateByUrl('/dangnhap');
          } else if (err.status === 502) { // Lỗi không kết nối được đến máy chủ backend
            this.toastr.warning('Không tìm thấy máy chủ hoặc máy chủ chưa được mở', 'CẢNH BÁO !!!', {
              timeOut: 5000
            });
            this.router.navigateByUrl('/dangnhap');
          }
        }
      });
    }
  }
}
