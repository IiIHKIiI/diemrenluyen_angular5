import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RequestOptions } from '@angular/http';

@Injectable()
export class TthoctapService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';



  constructor(
    private http: HttpClient
  ) { }

  getAllDanhSachTTHocTap(id_lop: string) {
    return this.http.post<any>(this.server + 'admin/quanlytinhtranghoctap/danhsachtinhtranghoctap', id_lop);
  }

  putSuaTTHT(id: string, info_ttht: FormData) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'admin/quanlytinhtranghoctap/suatinhtranghoctap/' + id, info_ttht, { headers: { 'enctype': 'multipart/form-data' } });
  }

  getInfoTTHT(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlytinhtranghoctap/thongtintthoctap/' + id);
  }

  deleteXoaTTHocTap(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlytinhtranghoctap/xoatinhtranghoctap/' + id);
  }

  getDemSoLuong(id_lop: string) {
    const id = { 'id_lop' : id_lop};
    return this.http.post<any>(this.server + 'admin/quanlytinhtranghoctap/demsoluongtthoctap', id);
  }

  getFileHocTap(id_sv: string) {
    return this.http.get<any>(this.server + 'admin/quanlytinhtranghoctap/getfilehoctap/' + id_sv);
  }
}
