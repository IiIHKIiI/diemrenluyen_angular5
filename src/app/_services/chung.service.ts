import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ChungService {
  id_thoigiandanhgia: string;
  infoUser: any;

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  setIDThoiGianDanhGia(id_thoigiandanhgia) {
    return this.id_thoigiandanhgia = id_thoigiandanhgia;
  }

  getIDThoiGianDanhGia() {
    return this.id_thoigiandanhgia;
  }

  setInfoUser(info: any) {
    return this.infoUser = info;
  }

  getInfoUser() {
    return this.infoUser;
  }

  postKiemTraHocKy(tghientai: any) {
    return this.http.post<any>(this.server + 'user/kiemtrahocky', { tg: tghientai });
  }

  getBangDiemDanhGiaByIDThoiGian_SV(id_thoigiandanhgia: string, id_sv: string) {
    return this.http.post<any>(this.server + 'user/thongtinbangdiem', { id_thoigiandanhgia: id_thoigiandanhgia, id_sv: id_sv });
  }
}
