import { Injectable } from '@angular/core';
import { Observable } from 'rxjs//Observable';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class KhoaService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  info: any;

  constructor(private http: HttpClient) { }
  /* Get danh sách tất cả khoa */
  getDanhSachKhoa() {
    return this.http.get<any>(this.server + 'admin/quanlykhoa/danhsachkhoa');
  }

  /* Thêm khoa mới */
  postThemKhoa(info: any) {
    const data = JSON.stringify(info);
    return this.http.post<any>(this.server + 'admin/quanlykhoa/themkhoa', data);
  }

  /* Sửa thông tin khoa */
  putSuaKhoa(id: string, info: any) {
    return this.http.put<any>(this.server + 'admin/quanlykhoa/suakhoa/' + id, info);
  }

  /* Xóa khoa */
  deleteXoaKhoa(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlykhoa/xoakhoa/' + id);
  }
  /* Get thông tin khoa bằng id */
  getInfoKhoa(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlykhoa/thongtinkhoa/' + id);
  }
}
