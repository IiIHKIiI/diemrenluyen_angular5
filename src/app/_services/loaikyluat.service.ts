import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoaikyluatService {
  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachLoaiKyLuat() {
    return this.http.get<any>(this.server + 'admin/quanlykyluat/danhsachloaikyluat');
  }
}
