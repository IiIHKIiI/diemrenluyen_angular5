import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ThongbaoService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getAllDanhSachThongBao() {
    return this.http.get<any>(this.server + 'admin/quanlythongbao/danhsachthongbao');
  }

  getDanhSachThongBaoHienThi() {
    return this.http.get<any>(this.server + 'admin/quanlythongbao/danhsachthongbaohienthi');
  }

  getInfoThongBao(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlythongbao/thongtinthongbao/' + id);
  }

  postThemThongBao(info_ThongBao: FormData) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'admin/quanlythongbao/themthongbao', info_ThongBao, { headers: { 'enctype': 'multipart/form-data' } });
  }

  putSuaThongBao(info_ThongBao: FormData, id: string) {
    // tslint:disable-next-line:max-line-length
    return this.http.put<any>(this.server + 'admin/quanlythongbao/suathongbao/' + id, info_ThongBao, { headers: { 'enctype': 'multipart/form-data' } });
  }

  deleteXoaThongBao(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlythongbao/xoathongbao/' + id);
  }

  putLockOrOpenTB(trangthai: any, id: string) {
    return this.http.put<any>(this.server + 'admin/quanlythongbao/khoahoackichhoatthongbao/' + id, trangthai);
  }
}
