import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CanboService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachCanBo_BoMon(id_bomon: string) {
    const id = { id_bomon: id_bomon };
    return this.http.post<any>(this.server + 'admin/quanlycanbo/danhsachcanbo_bomon', id);
  }

  postThemCanBo(info_canbo: any) {
    const data = JSON.stringify(info_canbo);
    return this.http.post<any>(this.server + 'admin/quanlycanbo/themcanbo', data);
  }

  getInfoCanBo(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlycanbo/thongtincanbo/' + id);
  }

  putSuaCanBo(info_canbo: any, id: string) {
    const data = JSON.stringify(info_canbo);
    return this.http.put<any>(this.server + 'admin/quanlycanbo/suacanbo/' + id, data);
  }

  deleteXoaCanBo(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlycanbo/xoacanbo/' + id);
  }

  getDanhSachCanBo_ChuaCoVan(id_bomon_donvi: string) {
    const id_bomon = { id_bomon_donvi: id_bomon_donvi };
    return this.http.post<any>(this.server + 'admin/quanlycanbo/danhsachcanbochuacovan', id_bomon);
  }

   importDSCanBo(info: FormData) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'admin/quanlycanbo/importdscanbo', info, { headers: { 'enctype': 'multipart/form-data' } });
  }
}
