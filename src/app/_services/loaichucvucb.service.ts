import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoaichucvucbService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getAllDSLoaiChucVuCB() {
    return this.http.get<any>(this.server + 'admin/quanlycanbo/danhsachchucvuquanly');
  }
}
