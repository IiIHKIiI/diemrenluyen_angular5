import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestOptions } from '@angular/http';

@Injectable()
export class KyluatService {
  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(
    private http: HttpClient
  ) { }

  getAllDanhSachKyLuat(id_lop: string) {
    const id = { 'id_lop': id_lop };
    return this.http.post<any>(this.server + 'admin/quanlykyluat/danhsachkyluat', id_lop);
  }

  postThemKyLuat(info: FormData) {
    return this.http.post<any>(this.server + 'admin/quanlykyluat/themkyluat', info, { headers: { 'enctype': 'multipart/form-data' } });
  }

  putSuaKyLuat(info: FormData, id: string) {
    return this.http.post<any>(this.server + 'admin/quanlykyluat/suakyluat/' + id, info, { headers: { 'enctype': 'multipart/form-data' } });
  }

  getInfoKyLuat(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlykyluat/thongtinkyluat/' + id);
  }

  deleteXoaKyLuat(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlykyluat/xoakyluat/' + id);
  }

  getDemSoLuong(id_lop: string) {
    const id = { 'id_lop': id_lop };
    return this.http.post<any>(this.server + 'admin/quanlykyluat/demsoluongkyluat', id);
  }

  getFileKyLuat(id_sv: string) {
    return this.http.get<any>(this.server + 'admin/quanlykyluat/getfilekyluat/' + id_sv);
  }
}
