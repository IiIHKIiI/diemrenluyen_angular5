import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoaichucvubcsService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(
    private http: HttpClient
  ) { }

  getAllDSLoaiChucVu() {
    return this.http.get<any>(this.server + 'admin/quanlysinhvien/danhsachchucvusinhvien');
  }
}
