import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NamhochockyService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachNamHocHocKy() {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/danhsachnamhochocky');
  }

  getDanhSachHocKy() {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/danhsachhocky');
  }

  postThemNamHocHocKy(info_namhoc: any) {
    return this.http.post<any>(this.server + 'admin/quanlynamhochocky/themnamhochocky', info_namhoc);
  }

  getInfo_NamHocHocKy(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/thongtinnamhoc/' + id);
  }

  putSuaNamHocHocKy(info_namhoc: any, id: string) {
    return this.http.put<any>(this.server + 'admin/quanlynamhochocky/suanamhochocky/' + id, info_namhoc);
  }

  deleteXoaNamHocHocKy(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlynamhochocky/xoanamhochocky/' + id);
  }

  getDanhSachNamHoc() {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/danhsachnamhoc');
  }

  getDanhSachHocKyByNamHoc(id_namhoc: string) {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/danhsachhocky/' + id_namhoc);
  }

  getDanhSachAllHocKyByNamHoc(id_namhoc: string) {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/danhsachtatcahocky/' + id_namhoc);
  }

  getHocKy_TGDG() {
    return this.http.get<any>(this.server + 'admin/quanlynamhochocky/danhsachhocky_tgdanhgia');
  }
}
