import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestOptions, ResponseContentType, ResponseType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class ChitietdanhgiaService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  checkDanhGia(id_hocky: string, id_sv: string) {
    return this.http.post<any>(this.server + 'user/kiemtradanhgia', { id_hocky: id_hocky, id_sv: id_sv });
  }

  postTaoBangDiemDanhGia(id_thoigiandanhgia: string, id_sv: string) {
    return this.http.post<any>(this.server + 'user/taobangdiemdanhgia', { id_thoigiandanhgia: id_thoigiandanhgia, id_sv: id_sv });
  }

  getLichSuDanhGiaByIdSV(id_sv) {
    return this.http.get<any>(this.server + 'user/lichsudanhgia/' + id_sv);
  }

  postChiTietDanhGia(infoDanhGia: any) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'user/chitietdanhgiasinhvien', infoDanhGia, { headers: { 'enctype': 'multipart/form-data' } });
  }

  getInfoDanhGia(id_thoigiandanhgia: string, id_sv: string, loaiuserdanhgia: string) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'user/thongtindanhgia', { id_thoigiandanhgia: id_thoigiandanhgia, id_sv: id_sv, loaiuserdanhgia: loaiuserdanhgia });
  }

  getInfoDanhGiaByIdBangDiemDanhGia(id_bangdiemdanhgia: string) {
    return this.http.get<any>(this.server + 'user/thongtindanhgia/' + id_bangdiemdanhgia);
  }

  postDuyetNhanh(infoDuyetNhanh: any) {
    return this.http.post<any>(this.server + 'user/duyetnhanhdanhgia', infoDuyetNhanh);
  }

  exportKetQuaDanhGia(info: any): Observable<Blob> {
    return this.http.post(this.server + 'user/exportketquadanhgia', info, { responseType: 'blob' });
  }

  exportBangDiemDanhGia(info: any): Observable<Blob> {
    return this.http.post(this.server + 'user/exportbangdiemdanhgia', info, { responseType: 'blob' });
  }

  postThongKeDiemDanhGiaTheoHocKy(info: any) {
    return this.http.post<any>(this.server + 'user/xemthongkediemtheohocky', info);
  }

  postThongKeDiemDanhGiaTheoLop_HocKy(info: any) {
    return this.http.post<any>(this.server + 'user/xemthongkediemtheolop_hocky', info);
  }

  putSuaDanhGia(infoDanhGia: any) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'user/suadanhgia', infoDanhGia, { headers: { 'enctype': 'multipart/form-data' } });
  }

  getFileMinhChung(info: any) {
    return this.http.post<any>(this.server + 'user/getFileMinhChung', info);
  }

}
