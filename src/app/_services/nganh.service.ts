import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NganhService {
  server = 'http://danhgiadrl.agu.edu.vn/api/';
  constructor(private uService: UserService, private http: HttpClient) {

  }

  /* Lấy tất cả danh sách ngành theo khoa */
  getDanhSachNganh(id_khoa: number) {

    return this.http.post<any>(this.server + 'admin/quanlynganh/danhsachnganh_khoa', { id_khoa });
  }

  /* Lấy tất cả danh sách  */
  getAllDanhSachNganh() {
    return this.http.get<any>(this.server + 'admin/quanlynganh/danhsachnganh');
  }

  /* Thêm ngành mới */
  postThemNganh(info_nganh: any) {
    const info = JSON.stringify(info_nganh);
    return this.http.post<any>(this.server + 'admin/quanlynganh/themnganh', info);
  }
  /* Sửa thông tin ngành */
  putSuaNganh(info_nganh: any, id: string) {
    const info = JSON.stringify(info_nganh);
    return this.http.put<any>(this.server + 'admin/quanlynganh/suanganh/' + id, info);
  }
  /* Xóa ngành */
  deleteXoaNganh(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlynganh/xoanganh/' + id);
  }
  /* Xem thông tin ngành theo id*/
  getInfoNganh(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlynganh/thongtinnganh/' + id);
  }
}
