import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LoaitthoctapService {
  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachLoaiTTHT() {
    return this.http.get<any>(this.server + 'admin/quanlytinhtranghoctap/danhsachloaitthoctap');
  }

}
