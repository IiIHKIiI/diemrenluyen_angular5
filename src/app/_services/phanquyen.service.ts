import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class PhanquyenService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDSPhanQuyenByIDUser(id_user: string) {
    // const id = { id_user: id_user };
    return this.http.get<any>(this.server + 'admin/quanlytaikhoan/danhsachphanquyen/' + id_user);
  }

  postPhanQuyen(id_nhomquyen: string, id_user: string) {
    const id = { id_nhomquyen: id_nhomquyen };
    return this.http.post<any>(this.server + 'admin/quanlytaikhoan/themphanquyen/' + id_user, id);
  }

}
