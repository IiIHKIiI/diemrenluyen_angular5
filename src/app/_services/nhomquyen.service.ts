import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NhomquyenService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachLoaiQuyenSV() {
    return this.http.get<any>(this.server + 'admin/quanlytaikhoan/danhsachnhomquyensinhvien');
  }

  getDanhSachLoaiQuyenCB() {
    return this.http.get<any>(this.server + 'admin/quanlytaikhoan/danhsachnhomquyencanbo');
  }

}
