import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CovanhoctapService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  postDanhSachCoVanHocTap(id_bomon_donvi: string) {
    const id_bomon = { id_bomon_donvi: id_bomon_donvi };
    return this.http.post<any>(this.server + 'admin/quanlycanbo/danhsachcovanhoctap', id_bomon);
  }

  getInfoCoVanHocTap(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlycanbo/thongtincovanhoctap/' + id);
  }

  postThemCoVanHocTap(infocvht: any) {
    const info = JSON.stringify(infocvht);
    return this.http.post<any>(this.server + 'admin/quanlycanbo/themcovanhoctap', info);
  }

  putSuaCoVanHocTap(infocvht: any, id: string) {
    const info = JSON.stringify(infocvht);
    return this.http.put<any>(this.server + 'admin/quanlycanbo/suacovanhoctap/' + id, info);
  }

  deleteCoVanHocTap(id_canbo: string, id_lop: string) {
    const info = { id_canbo: id_canbo, id_lop: id_lop };
    return this.http.post<any>(this.server + 'admin/quanlycanbo/xoacovanhoctap', info);
  }
}
