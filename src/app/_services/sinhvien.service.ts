import { Injectable } from '@angular/core';
import { UserService } from './user.service';

import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs//Observable';


@Injectable()
export class SinhvienService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  infoKhoa_Nganh_Lop: any;
  id_lop: number;
  hedaotao: number;
  constructor(private http: HttpClient) {

  }

  getDanhSachSinhVien_TheoLop(id_lop: string) {
    return this.http.post<any>(this.server + 'admin/quanlysinhvien/danhsachsinhvien_lop', { id_lop: id_lop });
  }

  postDanhSachSinhVien_DanhGia(id_lop: string, id_thoigiandanhgia: string) {
    return this.http.post<any>(this.server + 'user/danhsachsinhvien_danhgia', { id_lop: id_lop, id_thoigiandanhgia: id_thoigiandanhgia });
  }

  /* Xem thông tin sinh viên */
  getThongTinSinhVien(id_sv: string) {
    return this.http.get<any>(this.server + 'admin/quanlysinhvien/thongtinsinhvien/' + id_sv);
  }

  /* Xem thông tin chi tiết sinh viên */
  getChiTietSinhVien(id_sv: string) {
    return this.http.get<any>(this.server + 'admin/quanlysinhvien/chitietthongtinsinhvien/' + id_sv);
  }


  /* Lấy tên khoa, ngành ,lớp */
  setinfoKhoa_Nganh_Lop(info: any, id_lop: string, hedaotao: number) {
    this.infoKhoa_Nganh_Lop = { info: info, id_lop: id_lop, hedaotao: hedaotao };
  }

  /*  */
  getinfoKhoa_Nganh_Lop(): any {
    return this.infoKhoa_Nganh_Lop;
  }

  /* Thêm sinh viên */
  postThemSinhVien(info_sv: any) {
    const data = JSON.stringify(info_sv);
    return this.http.post<any>(this.server + 'admin/quanlysinhvien/themsinhvien', data);
  }

  /* Sửa thông tin sinh viên */
  putSuaSinhVien(id: string, info_sv: any) {
    const data = JSON.stringify(info_sv);
    return this.http.put<any>(this.server + 'admin/quanlysinhvien/suasinhvien/' + id, data);
  }

  /* Xóa thông tin sinh viên */
  deleteXoaSinhVien(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlysinhvien/xoasinhvien/' + id);
  }

  getIDLop(id_sv: string) {
    return this.http.get<any>(this.server + 'admin/quanlysinhvien/getidlop/' + id_sv);
  }

  importDSSinhVien(info: FormData) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'admin/quanlysinhvien/importdssinhvien', info, { headers: { 'enctype': 'multipart/form-data' } });
  }

}
