import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ThoigiandanhgiaService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachThoiGianDanhGia() {
    return this.http.get<any>(this.server + 'admin/quanlythoigiandanhgia/danhsachthoigiandanhgia');
  }

  getInfoThoiGianDanhGiaById(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlythoigiandanhgia/thongtinthoigiandanhgia/' + id);
  }

  getThoiGianDanhGiaByIdHocKy(id_hocky: string) {
    return this.http.get<any>(this.server + 'admin/quanlythoigiandanhgia/thoigiandanhgia/' + id_hocky);
  }

  postThemThoiGianDanhGia(info_tgdanhgia: any) {
    // const info = JSON.stringify(info_tgdanhgia);
    return this.http.post<any>(this.server + 'admin/quanlythoigiandanhgia/themthoigiandanhgia', info_tgdanhgia);
  }

  putSuaNamHocHocKy(info_tgdanhgia: any, id: string) {
    // const info = JSON.stringify(info_tgdanhgia);
    return this.http.put<any>(this.server + 'admin/quanlythoigiandanhgia/suathoigiandanhgia/' + id, info_tgdanhgia);
  }

  deleteXoaNamHocHocKy(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlythoigiandanhgia/xoathoigiandanhgia/' + id);
  }

  checkTGDanhGiaTheoPhanQuyen(phanquyen: string, id_thoigiandanhgia: string) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'user/checkthoigiandanhgiatheophanquyen', { phanquyen: phanquyen, id_thoigiandanhgia: id_thoigiandanhgia });
  }
}
