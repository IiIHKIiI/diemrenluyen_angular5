import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BomondonviService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachBoMon() {
    return this.http.get<any>(this.server + 'admin/quanlybomon/danhsachbomon');
  }

  /* Lấy danh sách bộ môn theo điều kiện */
  getDanhSachBoMonByTrucThuoc(tructhuoc: number) {
    return this.http.get<any>(this.server + 'admin/quanlybomon/danhsachbomon_tructhuoc/' + tructhuoc);
  }

  /* Lấy thông tin bộ môn để sửa */
  getInfoBoMon(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlybomon/thongtinbomon/' + id);
  }

  /* Thêm bộ môn mới */
  postBoMon(info_bomon: any) {
    const info = JSON.stringify(info_bomon);
    return this.http.post<any>(this.server + 'admin/quanlybomon/thembomon', info);
  }

  /* Sửa thông tin bộ môn */
  putSuaBoMon(info_bomon: any, id: string) {
    const info = JSON.stringify(info_bomon);
    return this.http.put<any>(this.server + 'admin/quanlybomon/suabomon/' + id, info);
  }

  /* Xóa bộ môn theo id */
  deleteXoaBoMon(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlybomon/xoabomon/' + id);
  }

}
