import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LopService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private uService: UserService, private http: HttpClient) { }

  /* Lấy tất cả lớp theo ngành */
  getDanhSachLop_Nganh(id_nganh: number) {
    return this.http.post<any>(this.server + 'admin/quanlylop/danhsachlop_nganh', { id_nganh });
  }


  getLopCoVan(id_canbo: string, id_thoigiandanhgia: string) {
    return this.http.post<any>(this.server + 'user/lopcovan', { id_canbo: id_canbo, id_thoigiandanhgia: id_thoigiandanhgia });
  }

  getDanhSachLopHoiDongDanhGia(id_canbo: string) {
    return this.http.post<any>(this.server + 'user/danhsachlophoidongdanhgia', { id_canbo: id_canbo });
  }


  /* Lấy tất cả các lớp */
  getDanhSachLop() {
    return this.http.get<any>(this.server + 'admin/quanlylop/danhsachlop');
  }

  /* Thêm lớp mới */
  postThemLop(info_lop: any) {
    const info = JSON.stringify(info_lop);
    return this.http.post<any>(this.server + 'admin/quanlylop/themlop', info);
  }

  /* Sửa thông tin lớp */
  putSuaLop(info_lop: any, id: string) {
    const info = JSON.stringify(info_lop);
    return this.http.put<any>(this.server + 'admin/quanlylop/sualop/' + id, info);
  }

  /* Xóa lớp */
  deleteXoaLop(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlylop/xoalop/' + id);
  }

  /* Xem thông tin của lớp theo ngành */
  getInfoLop(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlylop/thongtinlop/' + id);
  }
}
