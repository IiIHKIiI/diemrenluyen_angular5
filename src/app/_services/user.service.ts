import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import * as JWT from 'jwt-decode';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs//Observable';

@Injectable()
export class UserService {
  id_user: string;
  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient, private route: Router) { }

  /* Đăng nhập */
  login(username: string, password: string) {
    return this.http.post<any>(this.server + 'dangnhap', { username, password })
      .map(response => {
        const token = response.token;
        const decodedToken = JWT(token);
        return { token: token, decodedToken: decodedToken };
      })
      .do(data => {
        const token = data.token;
        localStorage.setItem('token', token);
      });
  }

  /* Đăng xuất */
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
  }

  /* Lấy token */
  getToken() {
    return localStorage.getItem('token');
  }

  /* lấy thông tin User bằng token */
  getInfoUser() {
    const token = JSON.stringify(this.getToken());
    return this.http.post<any>(this.server + 'admin/thongtinnguoidunghienhanh', token);
  }

  /* Lấy danh sách tài khoản người dùng */
  getAllDanhSachTaiKhoanSV(id: string) {
    const id_lop = { id_lop: id };
    return this.http.post<any>(this.server + 'admin/quanlytaikhoan/danhsachtaikhoansinhvien', id_lop);
  }

  /* Lấy danh sách tài khoản người dùng */
  getAllDanhSachTaiKhoanCB(id: string) {
    const id_bomon = { id_bomon_donvi: id };
    return this.http.post<any>(this.server + 'admin/quanlytaikhoan/danhsachtaikhoancanbo', id_bomon);
  }

  /* Tạo tài khoản cho sinh viên */
  postCreateAccount(info_Account: any) {
    const info = JSON.stringify(info_Account);
    return this.http.post<any>(this.server + 'admin/quanlytaikhoan/taotaikhoansv', info);
  }

  /* Tạo tài khoản cho cán bộ */
  postCreateAccountCB(info_Account: any) {
    const info = JSON.stringify(info_Account);
    return this.http.post<any>(this.server + 'admin/quanlytaikhoan/taotaikhoancb', info);
  }

  /* Xóa tài khoản bằng ID user */
  deleteAccount(id_user: string) {
    return this.http.delete<any>(this.server + 'admin/quanlytaikhoan/xoataikhoansv/' + id_user);
  }

  /* Khóa hoặc mở khóa tài khoản */
  putLockOrOpenAccount(trangthai: any, id_user: string) {
    return this.http.put<any>(this.server + 'admin/quanlytaikhoan/khoahoackichhoattaikhoan/' + id_user, trangthai);
  }

  /* Khôi phục mật khẩu */
  resetPass(id_user: string) {
    return this.http.get<any>(this.server + 'admin/quanlytaikhoan/khoiphucmatkhau/' + id_user);
  }

  doiMatKhau(matkhaumoi: string, id_user: string) {
    return this.http.put<any>(this.server + 'user/doimatkhau/' + id_user, { matkhaumoi: matkhaumoi });
  }
}
