import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TieuchidanhgiaService {

  server = 'http://danhgiadrl.agu.edu.vn/api/';

  constructor(private http: HttpClient) { }

  getDanhSachLoaiDiem() {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/danhsachloaidiem');
  }

  getDanhSachQuyetDinh() {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/danhsachquyetdinhtieuchi');
  }

  postThemQuyetDinhTieuChi(info: FormData) {
    // tslint:disable-next-line:max-line-length
    return this.http.post<any>(this.server + 'admin/quanlytieuchidanhgia/themquyetdinhtieuchidanhgia', info, { headers: { 'enctype': 'multipart/form-data' } });
  }

  getDanhSachTieuChiThem(id_quyetdinhtieuchi: string) {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/danhsachtieuchivuathem/' + id_quyetdinhtieuchi);
  }

  getTieuChiDanhGia(id_tieuchidanhgia: string) {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/danhsachtieuchi/' + id_tieuchidanhgia);
  }

  getDanhSachChiMuc(id_quyetdinhtieuchi: string) {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/danhsachchimucvuathem/' + id_quyetdinhtieuchi);
  }

  getInfoTieuChi(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/thongtintieuchi/' + id);
  }

  postThemTieuChi(info_tieuchi: any) {
    return this.http.post<any>(this.server + 'admin/quanlytieuchidanhgia/themtieuchidanhgia', info_tieuchi);
  }

  putSuaTieuChi(info_tieuchi: any, id: string) {
    return this.http.put<any>(this.server + 'admin/quanlytieuchidanhgia/suatieuchidanhgia/' + id, info_tieuchi);
  }

  deleteXoaTieuChi(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlytieuchidanhgia/xoatieuchi/' + id);
  }

  putCapNhatTGKetThucQuyetDinh(tgketthuc: string, id: string) {
    return this.http.put<any>(this.server + 'admin/quanlytieuchidanhgia/capnhatthoigianketthucquyetdinh/' + id, { tgketthuc: tgketthuc });
  }

  deleteXoaQuyetDinh(id: string) {
    return this.http.delete<any>(this.server + 'admin/quanlytieuchidanhgia/xoaquyetdinh/' + id);
  }

  getInfoQD_TC(id: string) {
    return this.http.get<any>(this.server + 'admin/quanlytieuchidanhgia/thongtinquyetdinh/' + id);
  }

  putSuaQuyetDinh(info: FormData, id: string) {
    // tslint:disable-next-line:max-line-length
    return this.http.put<any>(this.server + 'admin/quanlytieuchidanhgia/suaquyetdinh/' + id, info, { headers: { 'enctype': 'multipart/form-data' }});
  }
}
