import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';

// Import Lib Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Import Lib Module Angular Material
import { MaterialModule } from './material.module';

// Import MDBootstrap
import { MDBBootstrapModule, MDBRootModule } from 'angular-bootstrap-md';

// Import Toast Notifycation
import { ToastrModule } from 'ngx-toastr';

// Import Progress Bar loading
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';

// Import Pagination
import { NgxPaginationModule } from 'ngx-pagination';

// Import Stepper
import { NgxStepperModule } from 'ngx-stepper';

// Import Back To Top Button
import { GoTopButtonModule } from 'ng2-go-top-button';

// Import Upload
import { UploadModule } from '@progress/kendo-angular-upload';

// Import Date tiem picker
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

// Import CKeditor
import { CKEditorModule } from 'ng2-ckeditor';

// Import Upload
import { FileUploadModule } from 'primeng/fileupload';

// Import chart
import * as FusionCharts from 'fusioncharts';
import * as Charts from 'fusioncharts/fusioncharts.charts';
import * as FintTheme from 'fusioncharts/themes/fusioncharts.theme.fint';
import { FusionChartsModule } from 'angular4-fusioncharts';

// Import HttpClient để truyền dữ liệu thông qua API
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';

// Import Forms
import {
  ReactiveFormsModule,
  FormsModule,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';

// Import Routing
import { AppRoutingModule } from './app.routing.module';

// Import các component
import { AdminPageComponentModule } from './_views/adminpage/admin-component.module';
import { SVPageComponentModule } from './_views/mainpage/SVPage/SVPage-component.module';
import { CanBoPageComponentModule } from './_views/mainpage/CanBoPage/CanBoPage-component.module';

// Guard
import { AuthGuard } from './_guards/auth-guard'; // Auth Guard
import { ActivateChildSVGuard } from './_guards/activateChildSV-guard';
import { ActivateChildCanBoGuard } from './_guards/activateChildCanBo-guard';
import { ActivateChildAdminGuard } from './_guards/activateChildAdmin-guard';

// Service
import { UserService } from './_services/user.service';
import { AlertService } from './_services/alert.service';
import { KhoaService } from './_services/khoa.service';
import { NganhService } from './_services/nganh.service';
import { LopService } from './_services/lop.service';
import { SinhvienService } from './_services/sinhvien.service';

import { BomondonviService } from './_services/bomondonvi.service';
import { AuthInterceptor } from './_services/auth.interceptor'; // Custom headers HttpRequest để đính kèm token xác thực

// tslint:disable-next-line:max-line-length
import { XemchitietsinhvienDialogComponent } from './_views/adminpage/qlSinhVien_Module/xemchitietsinhvien-dialog/xemchitietsinhvien-dialog.component';
import { ConfirmDialogComponent } from './_views/_layouts/confirm-dialog/confirm-dialog.component';
import { TthoctapService } from './_services/tthoctap.service';
import { LoaitthoctapService } from './_services/loaitthoctap.service';
import { LoaikyluatService } from './_services/loaikyluat.service';
import { KyluatService } from './_services/kyluat.service';
import { LoginComponent } from './_views/login/login.component';
import { ThemphanquyenDialogComponent } from './_views/adminpage/qlTaiKhoan_Module/themphanquyen-dialog/themphanquyen-dialog.component';
import { NhomquyenService } from './_services/nhomquyen.service';
import { PhanquyenService } from './_services/phanquyen.service';
import { LoaichucvubcsService } from './_services/loaichucvubcs.service';
import { CanboService } from './_services/canbo.service';
import { LoaichucvucbService } from './_services/loaichucvucb.service';
import { CovanhoctapService } from './_services/covanhoctap.service';
import { NamhochockyService } from './_services/namhochocky.service';
import { ThoigiandanhgiaService } from './_services/thoigiandanhgia.service';
import { SharedComponentModule } from './_views/SharedComponent.module';
import { DashboardComponent } from './_views/_layouts/dashboard/dashboard.component';
import { TieuchidanhgiaService } from './_services/tieuchidanhgia.service';
// tslint:disable-next-line:max-line-length
import { SuatieuchidanhgiadialogComponent } from './_views/adminpage/qlTieuChiDanhGia_Module/suatieuchidanhgiadialog/suatieuchidanhgiadialog.component';
import { CapnhatthoigianketthucdialogComponent } from './_views/adminpage/qlTieuChiDanhGia_Module/capnhatthoigianketthucdialog/capnhatthoigianketthucdialog.component';
import { ThongbaoService } from './_services/thongbao.service';
import { ChungService } from './_services/chung.service';
import { ChitietdanhgiaService } from './_services/chitietdanhgia.service';
import { ThemminhchungDialogComponent } from './_views/mainpage/main_shared_component/themminhchung-dialog/themminhchung-dialog.component';
// tslint:disable-next-line:max-line-length
import { XemlichsudanhgiadialogComponent } from './_views/mainpage/main_shared_component/xemlichsudanhgiadialog/xemlichsudanhgiadialog.component';
import { DoimatkhaudialogComponent } from './_views/mainpage/main_shared_component/doimatkhaudialog/doimatkhaudialog.component';
// tslint:disable-next-line:max-line-length
import { ImportDssinhVienDialogComponent } from './_views/adminpage/qlSinhVien_Module/import-dssinh-vien-dialog/import-dssinh-vien-dialog.component';
import { XemthongkediemdanhgiadialogComponent } from './_views/mainpage/CanBoPage/xemthongkediemdanhgiadialog/xemthongkediemdanhgiadialog.component';
import { ImportDscanBoDialogComponent } from './_views/adminpage/qlCanBo_Module/import-dscan-bo-dialog/import-dscan-bo-dialog.component';

FusionChartsModule.fcRoot(FusionCharts, Charts, FintTheme);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(), // MDbootstrap
    ReactiveFormsModule,
    AppRoutingModule, // Routes

    SharedComponentModule.forRoot(),

    ToastrModule.forRoot({
      positionClass: 'toast-top-full-width', // Vị trí hiển thị Toastr Notifycation
    }), // Toastr Notifycation
    NgProgressModule, // Loading Bar
    NgxPaginationModule, // Pagination - Phân trang,
    NgxStepperModule, // Stepper
    GoTopButtonModule,
    UploadModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CKEditorModule,
    FileUploadModule,
    FusionChartsModule
  ],
  providers: [
    UserService,
    AuthGuard,
    ActivateChildAdminGuard,
    ActivateChildCanBoGuard,
    ActivateChildSVGuard,
    AlertService,
    KhoaService,
    NganhService,
    LopService,
    SinhvienService,
    BomondonviService,
    TthoctapService,
    LoaitthoctapService,
    LoaikyluatService,
    KyluatService,
    NhomquyenService,
    PhanquyenService,
    LoaichucvubcsService,
    CanboService,
    LoaichucvucbService,
    CovanhoctapService,
    NamhochockyService,
    ThoigiandanhgiaService,
    TieuchidanhgiaService,
    ThongbaoService,
    ChungService,
    ChitietdanhgiaService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, // AuthGuard dùng để xác thực user
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }, // Loading Bar every request
  ],
  entryComponents: [
    XemchitietsinhvienDialogComponent,
    ConfirmDialogComponent,
    ThemphanquyenDialogComponent,
    SuatieuchidanhgiadialogComponent,
    CapnhatthoigianketthucdialogComponent,
    ThemminhchungDialogComponent,
    XemlichsudanhgiadialogComponent,
    DoimatkhaudialogComponent,
    ImportDssinhVienDialogComponent,
    XemthongkediemdanhgiadialogComponent,
    ImportDscanBoDialogComponent,
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
